-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Máy chủ: localhost
-- Thời gian đã tạo: Th7 28, 2020 lúc 06:42 PM
-- Phiên bản máy phục vụ: 10.4.13-MariaDB
-- Phiên bản PHP: 7.2.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `batdongsan`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `about`
--

CREATE TABLE `about` (
  `id` int(11) NOT NULL,
  `title` text COLLATE utf8_unicode_ci NOT NULL,
  `slogan` text COLLATE utf8_unicode_ci NOT NULL,
  `short_des` text COLLATE utf8_unicode_ci NOT NULL,
  `detail_des` text COLLATE utf8_unicode_ci NOT NULL,
  `slug` text COLLATE utf8_unicode_ci NOT NULL,
  `img` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `about`
--

INSERT INTO `about` (`id`, `title`, `slogan`, `short_des`, `detail_des`, `slug`, `img`) VALUES
(1, 'about', 'Vì sự phát triển tương lại', 'Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore\r\nDuis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore\r\n', '<p>Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore&nbsp;Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore<br />\r\n&nbsp;Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore</p>\r\n\r\n<p>Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore<br />\r\n&nbsp;</p>\r\n', 'about', '1595634057_shop-title-img.jpg');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `category`
--

CREATE TABLE `category` (
  `id` int(11) NOT NULL,
  `name` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `name_en` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `img` text CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `slug` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `active` int(11) NOT NULL DEFAULT 1,
  `deleted` int(11) NOT NULL DEFAULT 0,
  `level` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Đang đổ dữ liệu cho bảng `category`
--

INSERT INTO `category` (`id`, `name`, `name_en`, `description`, `img`, `slug`, `active`, `deleted`, `level`) VALUES
(1, 'Căn hộ', 'Apartment', NULL, '1592173177_apartment.png', 'apartment-1', 1, 0, 0),
(2, 'Toà nhà', 'Building', NULL, '1592173202_building.png', 'building-2', 1, 0, 0),
(3, 'Nhà đất', 'House', NULL, '1592173213_house.png', 'house-3', 1, 0, 0),
(4, 'Văn phòng', 'Office', NULL, '1592173232_office.png', 'office-4', 1, 0, 0),
(5, 'Cửa hàng', 'Shop', NULL, '1592173241_shop.png', 'shop-5', 1, 0, 0);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `comment`
--

CREATE TABLE `comment` (
  `id` int(11) NOT NULL,
  `productId` int(11) NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `deleted` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `company_info`
--

CREATE TABLE `company_info` (
  `id` int(11) NOT NULL,
  `info` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `value` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_updated` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `company_info`
--

INSERT INTO `company_info` (`id`, `info`, `value`, `last_updated`) VALUES
(1, 'address', ' 774 Le Duan, Quan 1, HCM', '2020-06-21 00:00:00'),
(2, 'phone', '0909000123', '2020-06-21 00:00:00'),
(3, 'facebook', 'https://www.facebook.com/', '2020-06-21 00:00:00'),
(4, 'email', 'olivia@gmail.com', '2020-06-21 00:00:00'),
(5, 'description', 'Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore.', '2020-06-21 00:00:00'),
(6, 'hot_product_home', '<p style=\"text-align:center\"><span style=\"font-size:26px\"><strong>Best Offers This Week</strong></span></p>\r\n\r\n<p style=\"text-align:center\">Lorem ipsum dolor sit amet, atqui sanctus delectus in duo. Purto fuisset sed et.</p>\r\n', '2020-07-11 00:00:00'),
(7, 'list_area_home', '<p style=\"text-align:center\"><span style=\"font-size:26px\"><strong>Select a city to begin exploring its neighborhoods</strong></span></p>\r\n\r\n<p style=\"text-align:center\">Lorem ipsum dolor sit amet, atqui sanctus delectus in duo. Purto fuisset sed et.</p>\r\n', '2020-07-11 00:00:00'),
(8, 'hot_product_home_English', '<p>sbc e</p>\r\n', '2020-07-11 00:00:00'),
(9, 'list_area_home_English', '<p>123 e</p>\r\n', '2020-07-11 00:00:00');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `detaildistribution`
--

CREATE TABLE `detaildistribution` (
  `id` int(11) NOT NULL,
  `distributionId` int(11) NOT NULL,
  `productId` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `paymentId` int(11) NOT NULL,
  `paymentProduct` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `deleted` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `distribution`
--

CREATE TABLE `distribution` (
  `id` int(11) NOT NULL,
  `distributorId` int(11) NOT NULL,
  `date` datetime NOT NULL,
  `totalPayment` int(11) NOT NULL DEFAULT 0,
  `userId` int(11) NOT NULL,
  `note` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `deleted` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `distributor`
--

CREATE TABLE `distributor` (
  `id` int(11) NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `address` text COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `note` text COLLATE utf8_unicode_ci NOT NULL,
  `deleted` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `event`
--

CREATE TABLE `event` (
  `id` int(11) NOT NULL,
  `name` text COLLATE utf8_unicode_ci NOT NULL,
  `startDate` datetime DEFAULT NULL,
  `endDate` datetime DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `deleted` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `gallery_detail`
--

CREATE TABLE `gallery_detail` (
  `id` int(11) NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `img` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `gallery_id` int(11) NOT NULL,
  `active` int(11) NOT NULL DEFAULT 1,
  `slug` text COLLATE utf8_unicode_ci NOT NULL,
  `deleted` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `history`
--

CREATE TABLE `history` (
  `id` int(11) NOT NULL,
  `title` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `time` text COLLATE utf8_unicode_ci NOT NULL,
  `des` text COLLATE utf8_unicode_ci NOT NULL,
  `img` text COLLATE utf8_unicode_ci NOT NULL,
  `deleted` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `history`
--

INSERT INTO `history` (`id`, `title`, `time`, `des`, `img`, `deleted`) VALUES
(1, 'title so 01', '2020-07-22', 'Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore\r\n', '1595634981_tin_tuc_05.png', 0);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `home_banner`
--

CREATE TABLE `home_banner` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `promotion_id` int(11) DEFAULT NULL,
  `img` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `active` int(11) DEFAULT 1,
  `deleted` int(11) NOT NULL DEFAULT 0,
  `link` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `home_hot_product`
--

CREATE TABLE `home_hot_product` (
  `id` int(11) NOT NULL,
  `img` text COLLATE utf8_unicode_ci NOT NULL,
  `product_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `home_hot_product`
--

INSERT INTO `home_hot_product` (`id`, `img`, `product_id`) VALUES
(1, '1592327721_hinh.jpg', 1),
(2, '1592327749_hinh1.jpg', 1),
(3, '1592327785_property-15-featured-img-550x800.jpg', 1),
(4, '1592327814_property-15-featured-img-1-550x550.jpg', 1),
(5, '1592327835_hinh2.jpg', 1);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `home_info_compa`
--

CREATE TABLE `home_info_compa` (
  `id` int(11) NOT NULL,
  `area` int(11) NOT NULL,
  `district` int(11) NOT NULL,
  `agency` int(11) NOT NULL,
  `product` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `location`
--

CREATE TABLE `location` (
  `id` int(11) NOT NULL,
  `district` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `province` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `img` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `active` int(11) NOT NULL DEFAULT 1,
  `slug` text COLLATE utf8_unicode_ci NOT NULL,
  `deleted` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `location`
--

INSERT INTO `location` (`id`, `district`, `province`, `img`, `active`, `slug`, `deleted`) VALUES
(1, 'Quận 1', 'Hồ Chí Minh', '1592174895_chicago.jpg', 1, 'quan-1', 0),
(2, 'Quận 9', 'Hồ Chí Minh', '1592174976_miami.jpg', 1, 'quan-9', 0),
(3, 'Quận 2', 'Hồ Chí Minh', '1592175047_newyork.jpg', 1, 'quan-2', 0),
(4, 'Long Thành', 'Đồng Nai', '1592175103_san_diego.jpg', 1, 'long-thanh', 0),
(5, 'Quận 5', 'Hồ Chí Minh', '1592175205_san_francisco.jpg', 1, 'quan-5', 0),
(6, 'Quận 6', 'Hồ Chí Minh', '1592175620_miami.jpg', 1, 'quan-6', 0),
(7, 'Bình Thạnh', 'Hồ Chí Minh', '1592175620_miami.jpg', 1, 'ho-chi-minh-binh-thanh', 0);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `news`
--

CREATE TABLE `news` (
  `id` int(11) NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `img` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `active` int(11) NOT NULL DEFAULT 1,
  `parent` int(11) NOT NULL DEFAULT 0,
  `slug` text COLLATE utf8_unicode_ci NOT NULL,
  `deleted` int(11) NOT NULL DEFAULT 0,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `created_by` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `news`
--

INSERT INTO `news` (`id`, `name`, `description`, `content`, `img`, `active`, `parent`, `slug`, `deleted`, `created_at`, `created_by`) VALUES
(1, 'tin tuc so 1', 'Ngày 30/8/2004 Bộ trưởng Bộ Nông nghiệp và PTNT đã ký quyết định số 2616/QĐ/BNN-TCCB về việc chuyển doanh nghiệp Nhà nước Công ty Bảo vệ Thực vật II thành Công ty Cổ phần Nông dược HAI và công ty chính thức di vào hoạt động với giấy phép kinh doanh số 4103003108 cấp ngày 7/2/2005 của Sở Kế Hoạch Đầu tư TP HCM.', '<p>Ng&agrave;y 30/8/2004 Bộ trưởng Bộ N&ocirc;ng nghiệp v&agrave; PTNT đ&atilde; k&yacute; quyết định số 2616/QĐ/BNN-TCCB về việc chuyển doanh nghiệp Nh&agrave; nước C&ocirc;ng ty Bảo vệ Thực vật II th&agrave;nh C&ocirc;ng ty Cổ phần N&ocirc;ng dược HAI v&agrave; c&ocirc;ng ty ch&iacute;nh thức di v&agrave;o hoạt động với giấy ph&eacute;p kinh doanh số 4103003108 cấp ng&agrave;y 7/2/2005 của Sở Kế Hoạch Đầu tư TP HCM.</p>\r\n', '1595630588_tin_tuc_01.', 1, 0, 'tin-tuc-so-1', 0, '2020-07-22 21:22:37', NULL),
(2, 'tin tuc so 1', 'Ngày 30/8/2004 Bộ trưởng Bộ Nông nghiệp và PTNT đã ký quyết định số 2616/QĐ/BNN-TCCB về việc chuyển doanh nghiệp Nhà nước Công ty Bảo vệ Thực vật II thành Công ty Cổ phần Nông dược HAI và công ty chính thức di vào hoạt động với giấy phép kinh doanh số 4103003108 cấp ngày 7/2/2005 của Sở Kế Hoạch Đầu tư TP HCM.', '<p>Ng&agrave;y 30/8/2004 Bộ trưởng Bộ N&ocirc;ng nghiệp v&agrave; PTNT đ&atilde; k&yacute; quyết định số 2616/QĐ/BNN-TCCB về việc chuyển doanh nghiệp Nh&agrave; nước C&ocirc;ng ty Bảo vệ Thực vật II th&agrave;nh C&ocirc;ng ty Cổ phần N&ocirc;ng dược HAI v&agrave; c&ocirc;ng ty ch&iacute;nh thức di v&agrave;o hoạt động với giấy ph&eacute;p kinh doanh số 4103003108 cấp ng&agrave;y 7/2/2005 của Sở Kế Hoạch Đầu tư TP HCM.</p>\r\n', '1595630661_tintuc02.', 1, 0, 'tin-tuc-so-1', 0, '2020-07-22 21:22:37', NULL),
(3, 'tin tuc so 1', 'Ngày 30/8/2004 Bộ trưởng Bộ Nông nghiệp và PTNT đã ký quyết định số 2616/QĐ/BNN-TCCB về việc chuyển doanh nghiệp Nhà nước Công ty Bảo vệ Thực vật II thành Công ty Cổ phần Nông dược HAI và công ty chính thức di vào hoạt động với giấy phép kinh doanh số 4103003108 cấp ngày 7/2/2005 của Sở Kế Hoạch Đầu tư TP HCM.', '<p>Ng&agrave;y 30/8/2004 Bộ trưởng Bộ N&ocirc;ng nghiệp v&agrave; PTNT đ&atilde; k&yacute; quyết định số 2616/QĐ/BNN-TCCB về việc chuyển doanh nghiệp Nh&agrave; nước C&ocirc;ng ty Bảo vệ Thực vật II th&agrave;nh C&ocirc;ng ty Cổ phần N&ocirc;ng dược HAI v&agrave; c&ocirc;ng ty ch&iacute;nh thức di v&agrave;o hoạt động với giấy ph&eacute;p kinh doanh số 4103003108 cấp ng&agrave;y 7/2/2005 của Sở Kế Hoạch Đầu tư TP HCM.</p>\r\n', '1595632157_tin_tuc_04.png', 1, 0, 'tin-tuc-so-1', 0, '2020-07-22 21:22:37', NULL),
(4, 'tin tuc so 1', 'Ngày 30/8/2004 Bộ trưởng Bộ Nông nghiệp và PTNT đã ký quyết định số 2616/QĐ/BNN-TCCB về việc chuyển doanh nghiệp Nhà nước Công ty Bảo vệ Thực vật II thành Công ty Cổ phần Nông dược HAI và công ty chính thức di vào hoạt động với giấy phép kinh doanh số 4103003108 cấp ngày 7/2/2005 của Sở Kế Hoạch Đầu tư TP HCM.', '<p>Ng&agrave;y 30/8/2004 Bộ trưởng Bộ N&ocirc;ng nghiệp v&agrave; PTNT đ&atilde; k&yacute; quyết định số 2616/QĐ/BNN-TCCB về việc chuyển doanh nghiệp Nh&agrave; nước C&ocirc;ng ty Bảo vệ Thực vật II th&agrave;nh C&ocirc;ng ty Cổ phần N&ocirc;ng dược HAI v&agrave; c&ocirc;ng ty ch&iacute;nh thức di v&agrave;o hoạt động với giấy ph&eacute;p kinh doanh số 4103003108 cấp ng&agrave;y 7/2/2005 của Sở Kế Hoạch Đầu tư TP HCM.</p>\r\n', '1595630661_tintuc02.', 1, 0, 'tin-tuc-so-1', 0, '2020-07-22 21:22:37', NULL),
(5, 'tin tuc so 1', 'Ngày 30/8/2004 Bộ trưởng Bộ Nông nghiệp và PTNT đã ký quyết định số 2616/QĐ/BNN-TCCB về việc chuyển doanh nghiệp Nhà nước Công ty Bảo vệ Thực vật II thành Công ty Cổ phần Nông dược HAI và công ty chính thức di vào hoạt động với giấy phép kinh doanh số 4103003108 cấp ngày 7/2/2005 của Sở Kế Hoạch Đầu tư TP HCM.', '<p>Ng&agrave;y 30/8/2004 Bộ trưởng Bộ N&ocirc;ng nghiệp v&agrave; PTNT đ&atilde; k&yacute; quyết định số 2616/QĐ/BNN-TCCB về việc chuyển doanh nghiệp Nh&agrave; nước C&ocirc;ng ty Bảo vệ Thực vật II th&agrave;nh C&ocirc;ng ty Cổ phần N&ocirc;ng dược HAI v&agrave; c&ocirc;ng ty ch&iacute;nh thức di v&agrave;o hoạt động với giấy ph&eacute;p kinh doanh số 4103003108 cấp ng&agrave;y 7/2/2005 của Sở Kế Hoạch Đầu tư TP HCM.</p>\r\n', '1595630661_tintuc02.', 1, 0, 'tin-tuc-so-1', 0, '2020-07-22 21:22:37', NULL);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `partner`
--

CREATE TABLE `partner` (
  `id` int(11) NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `logo` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `active` int(11) NOT NULL DEFAULT 1,
  `slug` text COLLATE utf8_unicode_ci NOT NULL,
  `deleted` int(11) NOT NULL DEFAULT 0,
  `img1` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `img2` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `img3` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `img4` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `img5` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `activelisting` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `roomtype` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `rentalprice` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `propertytype` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `saleprice` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `location` int(11) NOT NULL,
  `lating` text COLLATE utf8_unicode_ci NOT NULL,
  `longing` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `partner`
--

INSERT INTO `partner` (`id`, `name`, `logo`, `active`, `slug`, `deleted`, `img1`, `img2`, `img3`, `img4`, `img5`, `activelisting`, `roomtype`, `rentalprice`, `propertytype`, `saleprice`, `address`, `location`, `lating`, `longing`) VALUES
(1, 'Vinhomes Central Park - Quận 2', '1592822215_vihomelogo.jpg', 1, 'vinhomes-central-park-quan-2', 0, '1593038362_vinhomebanner1.jpg', '1592822712_vinhome01.jpg', '1592822712_vinhome2.jpg', '1592822712_vinhome4.jpeg', '1592822712_vinhome5.jpeg', '942 units', '1-5', '15.2 triệu - 140.7 triệu / month', 'Binh Thanh District', '137.6 triệu - 168.2 triệu / m2', '208 Nguyễn Hữu Cảnh, Quận Bình Thạnh, Tp.HCM.', 7, '10.796420', '106.719704'),
(2, 'Nova Land - Quận 5', '1592838978_novalogo.jpg', 1, 'nova-land-quan-5', 0, '1592839151_novadetail1.jpg', '1592839151_novadetai3.jpg', '1592839151_novadetail5.jpg', '1592839151_novadetail2.jpg', '1592839151_novadetail4.jpg', '942 units', '1-7', '10.2 triệu - 140.7 triệu / month', 'Tân Bình District', '137.6 triệu - 168.2 triệu / m2', 'Hồ Chí Minh', 5, '10.750670', '106.662690'),
(3, 'FLC Group', '1592839828_flclogo.png', 1, 'flc_group', 0, '1592839828_flcdetail1.jpg', '1592839828_flcdetail2.jpg', '1592839828_ftcdetail6.jpg', '1592839828_flcdetail3.jpg', '1592839828_flcdetail5.jpg', '942 units', '1-5', '15.2 triệu - 140.7 triệu / month', 'Quận 9', '137.6 triệu - 168.2 triệu / m2', 'Bình Sơn, Long Thành', 4, '10.795010', '107.009780'),
(4, 'Vinhomes Central Park - Quận 9', '1592822215_vihomelogo.jpg', 1, 'vinhomes-central-park---quan-9', 0, '1593038362_vinhomebanner1.jpg', '1592822712_vinhome01.jpg', '1592822712_vinhome2.jpg', '1592822712_vinhome4.jpeg', '1592822712_vinhome5.jpeg', '942 units', '1-5', '15.2 triệu - 140.7 triệu / month', 'Binh Thanh District', '137.6 triệu - 168.2 triệu / m2', '208 Nguyễn Hữu Cảnh, Quận Bình Thạnh, Tp.HCM.', 2, '10.847959', '106.835740'),
(5, 'Nova Land - Quận 9', '1592838978_novalogo.jpg', 1, 'nova-land', 0, '1592839151_novadetail1.jpg', '1592839151_novadetai3.jpg', '1592839151_novadetail5.jpg', '1592839151_novadetail2.jpg', '1592839151_novadetail4.jpg', '942 units', '1-7', '10.2 triệu - 140.7 triệu / month', 'Tân Bình District', '137.6 triệu - 168.2 triệu / m2', '86 Đường Cầu Đình, phường Long Phước, Quận 9, TP HCM.', 2, '10.812710', '106.866977');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `partner_registry`
--

CREATE TABLE `partner_registry` (
  `id` int(11) NOT NULL,
  `representator` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `register_date` datetime NOT NULL DEFAULT current_timestamp(),
  `product` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `partner_registry`
--

INSERT INTO `partner_registry` (`id`, `representator`, `phone`, `email`, `register_date`, `product`) VALUES
(1, 'tuan', '0347366361', 'hongoctuan.it@gmail.com', '2020-06-21 15:24:40', NULL),
(2, 'tuan', '0347366361', 'hongoctuan.it@gmail.com', '2020-06-21 15:24:40', NULL),
(3, '1', '1647366361', '1@gmail.com', '2020-06-22 08:51:34', 'vinhomes-central-park-suoi-tien-ben-thanh'),
(4, '2', '0347366361', '2@gmail.com', '2020-06-22 08:51:53', 'vinhomes-central-park-suoi-tien-ben-thanh'),
(5, 'registernew', '0', 'tuanhn@sendo.vn', '2020-07-23 21:17:52', '0'),
(6, 'registernew', '0', 'tuanhn@sendo.vn', '2020-07-23 21:17:56', '0'),
(7, 'registernew', '0', 'tuanhn@sendo.vn', '2020-07-23 21:20:05', '0');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `product`
--

CREATE TABLE `product` (
  `id` int(11) NOT NULL,
  `name` text COLLATE utf8_unicode_ci NOT NULL,
  `short_des` text COLLATE utf8_unicode_ci NOT NULL,
  `detail_des` text COLLATE utf8_unicode_ci NOT NULL,
  `category_id` int(11) NOT NULL,
  `location` int(11) DEFAULT NULL,
  `img1` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `img2` text COLLATE utf8_unicode_ci NOT NULL,
  `img3` text COLLATE utf8_unicode_ci NOT NULL,
  `img4` text COLLATE utf8_unicode_ci NOT NULL,
  `img5` text COLLATE utf8_unicode_ci NOT NULL,
  `img6` text COLLATE utf8_unicode_ci NOT NULL,
  `img7` text COLLATE utf8_unicode_ci NOT NULL,
  `img8` text COLLATE utf8_unicode_ci NOT NULL,
  `price` int(20) NOT NULL DEFAULT 0,
  `status_product` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `hot` int(11) NOT NULL DEFAULT 0,
  `active` int(11) NOT NULL DEFAULT 1,
  `slug` text COLLATE utf8_unicode_ci NOT NULL,
  `youtube` text COLLATE utf8_unicode_ci NOT NULL,
  `deleted` int(11) NOT NULL DEFAULT 0,
  `partner_id` int(11) NOT NULL DEFAULT 0,
  `room` int(11) NOT NULL DEFAULT 1,
  `tolet` int(11) NOT NULL DEFAULT 1,
  `area` int(11) NOT NULL DEFAULT 0,
  `address` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `features` text COLLATE utf8_unicode_ci NOT NULL,
  `electricity` text COLLATE utf8_unicode_ci NOT NULL,
  `cable_tv` text COLLATE utf8_unicode_ci NOT NULL,
  `utilities` text COLLATE utf8_unicode_ci NOT NULL,
  `minimum_lease_term` text COLLATE utf8_unicode_ci NOT NULL,
  `habitable` text COLLATE utf8_unicode_ci NOT NULL,
  `payment_period` text COLLATE utf8_unicode_ci NOT NULL,
  `allowed_pets` text COLLATE utf8_unicode_ci NOT NULL,
  `deposit` text COLLATE utf8_unicode_ci NOT NULL,
  `additional_space` text COLLATE utf8_unicode_ci NOT NULL,
  `garages_size` text COLLATE utf8_unicode_ci NOT NULL,
  `garages` text COLLATE utf8_unicode_ci NOT NULL,
  `area_size` text COLLATE utf8_unicode_ci NOT NULL,
  `publication_date` text COLLATE utf8_unicode_ci NOT NULL,
  `from_center` text COLLATE utf8_unicode_ci NOT NULL,
  `parking` text COLLATE utf8_unicode_ci NOT NULL,
  `ceiling_height` text COLLATE utf8_unicode_ci NOT NULL,
  `accommodation` text COLLATE utf8_unicode_ci NOT NULL,
  `heating` text COLLATE utf8_unicode_ci NOT NULL,
  `year_built` text COLLATE utf8_unicode_ci NOT NULL,
  `total_floor` text COLLATE utf8_unicode_ci NOT NULL,
  `floor` text COLLATE utf8_unicode_ci NOT NULL,
  `property_size` text COLLATE utf8_unicode_ci NOT NULL,
  `ac_heating` int(11) NOT NULL,
  `balcony` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `clubhouse` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `dishwasher` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `elevator` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `fitness_center` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `granite_countertops` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `laundry_facilities` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `modern_kitchen` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `pet_friendly` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `pool` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `spa` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `storage_units` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `tennis_court` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `valet_parking` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `walk_in_closets` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `washer_dryer` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `wifi_in_common_areas` varchar(10) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `product`
--

INSERT INTO `product` (`id`, `name`, `short_des`, `detail_des`, `category_id`, `location`, `img1`, `img2`, `img3`, `img4`, `img5`, `img6`, `img7`, `img8`, `price`, `status_product`, `hot`, `active`, `slug`, `youtube`, `deleted`, `partner_id`, `room`, `tolet`, `area`, `address`, `features`, `electricity`, `cable_tv`, `utilities`, `minimum_lease_term`, `habitable`, `payment_period`, `allowed_pets`, `deposit`, `additional_space`, `garages_size`, `garages`, `area_size`, `publication_date`, `from_center`, `parking`, `ceiling_height`, `accommodation`, `heating`, `year_built`, `total_floor`, `floor`, `property_size`, `ac_heating`, `balcony`, `clubhouse`, `dishwasher`, `elevator`, `fitness_center`, `granite_countertops`, `laundry_facilities`, `modern_kitchen`, `pet_friendly`, `pool`, `spa`, `storage_units`, `tennis_court`, `valet_parking`, `walk_in_closets`, `washer_dryer`, `wifi_in_common_areas`) VALUES
(1, 'Vinhomes Central Park Suôi Tien - Ben Thanh', 'Vinhomes Central Park project, invested by Vingroup, is situated in extremely favorable location in the heart of Ho Chi Minh City, located within Tan Cang area, frontage stretching for more than 1km on the bank of Saigon River and a stop on the Ben Thanh – Suoi Tien metro line.', '<h1 dir=\"ltr\"><strong>Apartments For Rent &amp; Sale In Vinhomes Central Park, Ho Chi Minh City</strong></h1>\r\n\r\n<p dir=\"ltr\">Vinhomes Central Park project, invested by Vingroup, is situated in extremely favorable location in the heart of Ho Chi Minh City, located within Tan Cang area, frontage stretching for more than 1km on the bank of Saigon River and a stop on the Ben Thanh &ndash; Suoi Tien metro line.</p>\r\n\r\n<p dir=\"ltr\" style=\"text-align:center\"><img alt=\"\" src=\"/upload/images/vindetail1.jpg\" style=\"height:475px; width:1200px\" /><img alt=\"\" src=\"/upload/images/vindetail1(1).jpg\" style=\"height:475px; width:1200px\" /></p>\r\n\r\n<p dir=\"ltr\">Vinhomes Central Park urban has convenient transportation by roadway, railway, waterway, easy connection to the important area of the city:&nbsp;</p>\r\n\r\n<p dir=\"ltr\">- Frontage stretching over 1km along the bank of Sai Gon river</p>\r\n\r\n<p dir=\"ltr\">- 2 minutes to Ben Thanh - Suoi Tien Metro Line No. 1</p>\r\n\r\n<p dir=\"ltr\">- 3 minutes to the Thu Thiem new urban area</p>\r\n\r\n<p dir=\"ltr\">- 4 minutes to District 1</p>\r\n\r\n<h2 dir=\"ltr\"><strong>1. Details of apartments in Vinhomes Central Park</strong></h2>\r\n\r\n<p style=\"text-align:center\"><img alt=\"\" src=\"/upload/images/vindetail10.jpg\" style=\"height:667px; width:1200px\" /><img alt=\"\" src=\"/upload/images/vindetail2.jpg\" style=\"height:696px; width:1200px\" /></p>\r\n\r\n<p dir=\"ltr\">Vinhomes Central Park urban includes 19 apartment buildings which are Landmark (L1 to L6), The Central (C1-C3), The Park (P1 to P7), Landmark 81 and Landmark Plus with 38 - 50 floors providing more than 10,000 apartments, and penthouses with areas from 45m2 - 400m2, designed from 1- 4 bedrooms.</p>\r\n\r\n<p dir=\"ltr\">All apartments are designed to optimize the living space, get full of natural liight and cool air.</p>\r\n\r\n<h2 dir=\"ltr\"><strong>2. Sample pictures</strong></h2>\r\n\r\n<p dir=\"ltr\" style=\"text-align:center\">&nbsp;</p>\r\n\r\n<p dir=\"ltr\" style=\"text-align:center\"><strong><img alt=\"\" src=\"/upload/images/vindetail3.jpg\" style=\"height:727px; width:1200px\" /></strong></p>\r\n\r\n<p dir=\"ltr\" style=\"text-align:center\"><strong><img alt=\"\" src=\"/upload/images/vindetail4.jpg\" style=\"height:794px; width:1200px\" /></strong></p>\r\n\r\n<p dir=\"ltr\" style=\"text-align:center\"><strong><img alt=\"\" src=\"/upload/images/vindetail5.jpg\" style=\"height:790px; width:1200px\" /></strong></p>\r\n\r\n<p dir=\"ltr\" style=\"text-align:center\"><strong><img alt=\"\" src=\"/upload/images/vindetail6.jpg\" style=\"height:799px; width:1200px\" /></strong></p>\r\n\r\n<p dir=\"ltr\" style=\"text-align:center\"><strong><img alt=\"\" src=\"/upload/images/vindetail2.jpg\" style=\"height:696px; width:1200px\" /><img alt=\"\" src=\"/upload/images/vindetail3.jpg\" style=\"height:727px; width:1200px\" /><img alt=\"\" src=\"/upload/images/vindetail4.jpg\" style=\"height:794px; width:1200px\" /></strong><img alt=\"\" src=\"/upload/images/vindetail5.jpg\" style=\"height:790px; width:1200px\" /><strong><img alt=\"\" src=\"/upload/images/vindetail8.jpg\" style=\"height:610px; width:1200px\" /></strong><em>Sample Apartments in Vinhomes Central Park</em></p>\r\n\r\n<h2 dir=\"ltr\"><strong>3. Utilities</strong></h2>\r\n\r\n<p dir=\"ltr\">- Interlevel Vinschool Central Park</p>\r\n\r\n<p dir=\"ltr\">- International Hospital Vinmec Central Park</p>\r\n\r\n<p dir=\"ltr\">- Luxury Marinas</p>\r\n\r\n<p dir=\"ltr\">- Children&#39;s outdoors play area</p>\r\n\r\n<p dir=\"ltr\">- Business Center: indoor ice rink, Vinpearland Game, modern cinema.</p>\r\n\r\n<p dir=\"ltr\">- Green Park</p>\r\n\r\n<p dir=\"ltr\">- Swimming pool, Gym, system training ground outdoor sports (football, tennis courts, and basketball courts)</p>\r\n\r\n<p dir=\"ltr\">- Security and 24/7 camera system</p>\r\n\r\n<p style=\"text-align:center\"><img alt=\"\" src=\"/upload/images/vindetail7.jpg\" style=\"height:789px; width:1200px\" /></p>\r\n\r\n<p dir=\"ltr\">And there are many other extra advantages only in Vinhomes Central Park</p>\r\n\r\n<p dir=\"ltr\" style=\"text-align:center\"><img alt=\"\" src=\"/upload/images/vindetail10.jpg\" style=\"float:left; height:267px; width:480px\" /><img alt=\"\" src=\"/upload/images/vindetail8.jpg\" style=\"float:left; height:265px; margin-left:10px; margin-right:10px; width:400px\" /></p>\r\n\r\n<h2 dir=\"ltr\"><strong>4. AREA</strong></h2>\r\n\r\n<p dir=\"ltr\">- Interlevel Vinschool Central Park</p>\r\n\r\n<p dir=\"ltr\">- International Hospital Vinmec Central Park</p>\r\n\r\n<p dir=\"ltr\">- Luxury Marinas</p>\r\n\r\n<p dir=\"ltr\">- Children&#39;s outdoors play area</p>\r\n\r\n<p dir=\"ltr\">- Business Center: indoor ice rink, Vinpearland Game, modern cinema.</p>\r\n\r\n<p dir=\"ltr\">- Green Park</p>\r\n\r\n<p dir=\"ltr\">- Swimming pool, Gym, system training ground outdoor sports (football, tennis courts, and basketball courts)</p>\r\n\r\n<p dir=\"ltr\">- Security and 24/7 camera system</p>\r\n', 1, 1, '1592823589_hot1.jpg', '', '', '', '', '', '', '', 1500000000, 'chothue.png', 1, 1, 'vinhomes-central-park-suoi-tien-ben-thanh', '', 0, 1, 2, 2, 60, 'ven sông (nối giữa đường Tôn Đức Thắng – Q.1 và đường Ung Văn Khiêm)', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0'),
(2, 'Nhà dự án Precia', 'Precia là tên thương mại của Block 2 thuộc Dự án D’Lusso tại Mặt tiền Đường Nguyễn Thị Định, Quận 2, TP.HCM. Căn hộ Precia cao 21 tầng (2 tầng hầm, 3 tầng TTTM) với hơn 230 căn hộ có diện tích từ 46 – 100 m² do Công ty TNHH Xây dựng và Kinh doanh nhà Điền Phúc Thành làm chủ đầu tư. Dự án căn hộ Precia Quận 2 nằm trên diện tích khu đất: 11.513,1 m2 ~1,15 ha cách khu trung tâm Khu đô thị Thủ Thiêm chỉ 5 phút đi xe máy.', '<p>Precia tọa lạc tr&ecirc;n tuyến đường Nguyễn Thị Định, thuộc phường An Ph&uacute;, Quận 2, Th&agrave;nh phố Hồ Ch&iacute; Minh, dự &aacute;n nằm liền kề với khu căn hộ D&rsquo;Lusso, c&aacute;ch Đại lộ Mai Ch&iacute; Thọ khoảng 400m.</p>\r\n\r\n<h2>Quy m&ocirc; dự &aacute;n căn hộ Precia</h2>\r\n\r\n<p style=\"text-align:center\"><img alt=\"\" src=\"/upload/images/preciadetail1.jpg\" style=\"height:800px; width:1200px\" /></p>\r\n\r\n<p>Precia nằm tr&ecirc;n khu đất c&oacute; tổng diện t&iacute;ch 7.156,4 m2, với mật độ x&acirc;y dựng khoảng 28,7%. B&ecirc;n cạnh đ&oacute;, dự &aacute;n c&ograve;n d&agrave;nh 3.475 m2 đất cho c&aacute;c tiện &iacute;ch dịch vụ v&agrave; mảng c&acirc;y xanh.</p>\r\n\r\n<p>Quy m&ocirc; dự &aacute;n gồm 1 khối th&aacute;p, cao 23 tầng, với 1 tầng hầm. Theo thiết kế dự &aacute;n sẽ c&oacute; 1 tầng khối đế thương mại v&agrave; 22 tầng căn hộ.</p>\r\n\r\n<p style=\"text-align:center\"><img alt=\"\" src=\"/upload/images/Preciadetail2.jpg\" style=\"height:259px; width:460px\" /><img alt=\"\" src=\"/upload/images/Preciadetail3.jpg\" style=\"height:259px; width:460px\" /></p>\r\n\r\n<p>Dự &aacute;n Precia cung ứng với tổng số 333 căn hộ v&agrave; 8 căn shophouse. Cụ thể, c&oacute; 72 căn hộ 1&nbsp;<a href=\"https://cafeland.vn/chu-de-nong/phong-ngu-dep-114/\" target=\"_bank\"><strong>ph&ograve;ng ngủ</strong></a>&nbsp;với diện t&iacute;ch 48,25 m2; 209 căn hộ 2 ph&ograve;ng ngủ với diện t&iacute;ch 66,24 m2; 40 căn hộ 3 ph&ograve;ng ngủ với diện t&iacute;ch 93,9 m2 v&agrave; 12 căn Penthouse với diện t&iacute;ch 116,2 m2.</p>\r\n\r\n<p style=\"text-align:center\"><img alt=\"\" src=\"/upload/images/Preciadetail3.jpg\" style=\"width:1200px\" /></p>\r\n\r\n<p style=\"text-align:center\">&nbsp;</p>\r\n\r\n<p>Theo đ&oacute;, c&aacute;c tiện &iacute;ch b&ecirc;n trong dự &aacute;n gồm c&oacute;: hồ bơi tr&ecirc;n cao, ph&ograve;ng tập gym, yoga, sky garden, cabana, th&aacute;c nước v&agrave; hồ cảnh quan,&hellip;</p>\r\n\r\n<h2>Th&ocirc;ng tin từ&nbsp;<a href=\"https://cafeland.vn/\" target=\"_bank\"><strong>CafeLand</strong></a></h2>\r\n\r\n<p>Dự &aacute;n căn hộ Precia do C&ocirc;ng ty TNHH Kinh doanh Địa ốc Minh Th&ocirc;ng l&agrave;m chủ đầu tư. C&ocirc;ng ty Cổ phần Đầu tư Bất động sản Rio Land l&agrave; đơn vị ph&aacute;t triển dự &aacute;n.</p>\r\n\r\n<p>C&ocirc;ng ty TNHH Kinh doanh Địa ốc Minh Th&ocirc;ng (thuộc C&ocirc;ng ty TNHH X&acirc;y dựng v&agrave; Kinh doanh nh&agrave; Điền Ph&uacute;c Th&agrave;nh), được th&agrave;nh lập v&agrave;o ng&agrave;y 10/07/2007. Theo th&ocirc;ng tin đăng k&yacute; thay đổi doanh nghiệp ng&agrave;y 19/09/2019, Địa ốc Minh Th&ocirc;ng đang hoạt động với vốn điều lệ 400 tỷ đồng.</p>\r\n\r\n<p>Gi&aacute; b&aacute;n căn hộ được tham khảo tr&ecirc;n thị trường của dự &aacute;n Precia từ 49 triệu/m2.</p>\r\n\r\n<h2>Th&ocirc;ng tin nhanh dự &aacute;n căn hộ Precia</h2>\r\n\r\n<p style=\"text-align:center\"><img alt=\"\" src=\"/upload/images/Preciadetail4.jpg\" style=\"height:840px; width:1200px\" /></p>\r\n\r\n<p style=\"text-align:center\"><img alt=\"\" src=\"/upload/images/Preciadetail6.jpg\" style=\"height:506px; width:1200px\" /></p>\r\n\r\n<table align=\"center\" border=\"1\" cellpadding=\"1\" cellspacing=\"1\">\r\n	<tbody>\r\n		<tr>\r\n			<td>\r\n			<p><strong>Dự &aacute;n:</strong>&nbsp;Precia</p>\r\n			</td>\r\n			<td><strong>Tổng diện t&iacute;ch:</strong>&nbsp;7.156,4 m2</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p><strong>Vị tr&iacute;:</strong>&nbsp;Đường Nguyễn Thị Định, phường An Ph&uacute;, Quận 2, Th&agrave;nh phố Hồ Ch&iacute; Minh</p>\r\n			</td>\r\n			<td><strong>Tổng vốn đầu tư:</strong></td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p><strong>Loại h&igrave;nh:&nbsp;</strong>Căn hộ</p>\r\n			</td>\r\n			<td><strong>Ng&agrave;y khởi c&ocirc;ng:</strong></td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p><strong>Chủ đầu tư:</strong>&nbsp;C&ocirc;ng ty TNHH Kinh doanh Địa ốc Minh Th&ocirc;ng</p>\r\n\r\n			<p><strong>Đơn vị ph&aacute;t triển:</strong>&nbsp;C&ocirc;ng ty Cổ phần Đầu tư Bất động sản Rio Land</p>\r\n			</td>\r\n			<td><strong>Năm ho&agrave;n th&agrave;nh:</strong></td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n\r\n<p>&nbsp;</p>\r\n', 1, 3, '1592824124_hot2.jpg', '', '', '', '', '', '', '', 2000000000, 'ban.png', 2, 1, 'nha-du-an-precia', '', 0, 2, 3, 2, 80, 'Mặt tiền Đường Nguyễn Thị Định, Quận 2, TP.HCM', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0'),
(12, 'Dự án nhà nova land quận 9', 'Precia là tên thương mại của Block 2 thuộc Dự án D’Lusso tại Mặt tiền Đường Nguyễn Thị Định, Quận 2, TP.HCM. Căn hộ Precia cao 21 tầng (2 tầng hầm, 3 tầng TTTM) với hơn 230 căn hộ có diện tích từ 46 – 100 m² do Công ty TNHH Xây dựng và Kinh doanh nhà Điền Phúc Thành làm chủ đầu tư. Dự án căn hộ Precia Quận 2 nằm trên diện tích khu đất: 11.513,1 m2 ~1,15 ha cách khu trung tâm Khu đô thị Thủ Thiêm chỉ 5 phút đi xe máy.', '<p>Precia tọa lạc tr&ecirc;n tuyến đường Nguyễn Thị Định, thuộc phường An Ph&uacute;, Quận 2, Th&agrave;nh phố Hồ Ch&iacute; Minh, dự &aacute;n nằm liền kề với khu căn hộ D&rsquo;Lusso, c&aacute;ch Đại lộ Mai Ch&iacute; Thọ khoảng 400m.</p>\r\n\r\n<h2>Quy m&ocirc; dự &aacute;n căn hộ Precia</h2>\r\n\r\n<p style=\"text-align:center\"><img alt=\"\" src=\"/upload/images/novadetail1.jpg\" style=\"height:619px; width:1200px\" /></p>\r\n\r\n<p>Precia nằm tr&ecirc;n khu đất c&oacute; tổng diện t&iacute;ch 7.156,4 m2, với mật độ x&acirc;y dựng khoảng 28,7%. B&ecirc;n cạnh đ&oacute;, dự &aacute;n c&ograve;n d&agrave;nh 3.475 m2 đất cho c&aacute;c tiện &iacute;ch dịch vụ v&agrave; mảng c&acirc;y xanh.</p>\r\n\r\n<p>Quy m&ocirc; dự &aacute;n gồm 1 khối th&aacute;p, cao 23 tầng, với 1 tầng hầm. Theo thiết kế dự &aacute;n sẽ c&oacute; 1 tầng khối đế thương mại v&agrave; 22 tầng căn hộ.</p>\r\n\r\n<p><img alt=\"\" src=\"/upload/images/Preciadetail3.jpg\" style=\"height:259px; width:460px\" /><img alt=\"\" src=\"/upload/images/novadetail2.jpg\" style=\"float:left; height:338px; margin-left:10px; margin-right:10px; width:500px\" />Dự &aacute;n Precia cung ứng với tổng số 333 căn hộ v&agrave; 8 căn shophouse.</p>\r\n\r\n<p>Cụ thể, c&oacute; 72 căn hộ 1&nbsp;<a href=\"https://cafeland.vn/chu-de-nong/phong-ngu-dep-114/\" target=\"_bank\"><strong>ph&ograve;ng ngủ</strong></a>&nbsp;với diện t&iacute;ch 48,25 m2; 209 căn hộ 2 ph&ograve;ng ngủ với diện t&iacute;ch 66,24 m2; 40 căn hộ 3 ph&ograve;ng ngủ với diện t&iacute;ch 93,9 m2 v&agrave; 12 căn Penthouse với diện t&iacute;ch 116,2 m2.</p>\r\n\r\n<p>Theo đ&oacute;, c&aacute;c tiện &iacute;ch b&ecirc;n trong dự &aacute;n gồm c&oacute;: hồ bơi tr&ecirc;n cao, ph&ograve;ng tập gym, yoga, sky garden, cabana, th&aacute;c nước v&agrave; hồ cảnh quan,&hellip;</p>\r\n\r\n<h2>Th&ocirc;ng tin từ&nbsp;<a href=\"https://cafeland.vn/\" target=\"_bank\"><strong>CafeLand</strong></a></h2>\r\n\r\n<p>Dự &aacute;n căn hộ Precia do C&ocirc;ng ty TNHH Kinh doanh Địa ốc Minh Th&ocirc;ng l&agrave;m chủ đầu tư. C&ocirc;ng ty Cổ phần Đầu tư Bất động sản Rio Land l&agrave; đơn vị ph&aacute;t triển dự &aacute;n.</p>\r\n\r\n<p>C&ocirc;ng ty TNHH Kinh doanh Địa ốc Minh Th&ocirc;ng (thuộc C&ocirc;ng ty TNHH X&acirc;y dựng v&agrave; Kinh doanh nh&agrave; Điền Ph&uacute;c Th&agrave;nh), được th&agrave;nh lập v&agrave;o ng&agrave;y 10/07/2007. Theo th&ocirc;ng tin đăng k&yacute; thay đổi doanh nghiệp ng&agrave;y 19/09/2019, Địa ốc Minh Th&ocirc;ng đang hoạt động với vốn điều lệ 400 tỷ đồng.</p>\r\n\r\n<p>Gi&aacute; b&aacute;n căn hộ được tham khảo tr&ecirc;n thị trường của dự &aacute;n Precia từ 49 triệu/m2.</p>\r\n\r\n<h2>&nbsp;</h2>\r\n\r\n<h2>&nbsp;</h2>\r\n\r\n<h2>Th&ocirc;ng tin nhanh dự &aacute;n căn hộ Precia</h2>\r\n\r\n<p style=\"text-align:center\"><img alt=\"\" src=\"/upload/images/novadetail4.jpg\" style=\"height:675px; width:1200px\" /></p>\r\n\r\n<p style=\"text-align:center\"><img alt=\"\" src=\"/upload/images/flcdetail1.jpg\" style=\"height:675px; width:1200px\" /></p>\r\n\r\n<table align=\"center\" border=\"1\" cellpadding=\"1\" cellspacing=\"1\">\r\n	<tbody>\r\n		<tr>\r\n			<td>\r\n			<p><strong>Dự &aacute;n:</strong>&nbsp;Precia</p>\r\n			</td>\r\n			<td><strong>Tổng diện t&iacute;ch:</strong>&nbsp;7.156,4 m2</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p><strong>Vị tr&iacute;:</strong>&nbsp;Đường Nguyễn Thị Định, phường An Ph&uacute;, Quận 2, Th&agrave;nh phố Hồ Ch&iacute; Minh</p>\r\n			</td>\r\n			<td><strong>Tổng vốn đầu tư:</strong></td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p><strong>Loại h&igrave;nh:&nbsp;</strong>Căn hộ</p>\r\n			</td>\r\n			<td><strong>Ng&agrave;y khởi c&ocirc;ng:</strong></td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p><strong>Chủ đầu tư:</strong>&nbsp;C&ocirc;ng ty TNHH Kinh doanh Địa ốc Minh Th&ocirc;ng</p>\r\n\r\n			<p><strong>Đơn vị ph&aacute;t triển:</strong>&nbsp;C&ocirc;ng ty Cổ phần Đầu tư Bất động sản Rio Land</p>\r\n			</td>\r\n			<td><strong>Năm ho&agrave;n th&agrave;nh:</strong></td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n\r\n<p>&nbsp;</p>\r\n', 1, 2, '1592824158_hot3.jpg', '', '', '', '', '', '', '', 1200000000, 'chothue.png', 3, 1, 'du-an-nha-nova-land-quan-9', '', 0, 5, 2, 2, 60, '86 Đường Cầu Đình, phường Long Phước, Quận 9, TP HCM.', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0'),
(13, 'Dự án nhà demo số 002', 'Precia là tên thương mại của Block 2 thuộc Dự án D’Lusso tại Mặt tiền Đường Nguyễn Thị Định, Quận 2, TP.HCM. Căn hộ Precia cao 21 tầng (2 tầng hầm, 3 tầng TTTM) với hơn 230 căn hộ có diện tích từ 46 – 100 m² do Công ty TNHH Xây dựng và Kinh doanh nhà Điền Phúc Thành làm chủ đầu tư. Dự án căn hộ Precia Quận 2 nằm trên diện tích khu đất: 11.513,1 m2 ~1,15 ha cách khu trung tâm Khu đô thị Thủ Thiêm chỉ 5 phút đi xe máy.', '<h2><strong>TỔNG Q</strong><img alt=\"\" src=\"/upload/images/flcdetail1.jpg\" style=\"float:right; height:506px; width:500px\" />UAN</h2>\r\n\r\n<ul>\r\n	<li>Century City l&agrave; dự &aacute;n đất nền c&oacute; quy m&ocirc; l&ecirc;n đến 49,8 ha tọa lạc ngay trung t&acirc;m đ&ocirc; thị s&acirc;n bay Long Th&agrave;nh, cảng trung chuyển quốc tế tầm cỡ khu vực ch&acirc;u &Aacute; với c&ocirc;ng suất 100 triệu lượt kh&aacute;ch v&agrave; 5 triệu tấn h&agrave;ng h&oacute;a mỗi năm. Hứa hẹn trở th&agrave;nh một khu đ&ocirc; thị thương mại &ndash; dịch vụ đẳng cấp v&agrave; g&oacute;p phần v&agrave;o bước chuyển m&igrave;nh mạnh mẽ của Long Th&agrave;nh tr&ecirc;n chặng đường ph&aacute;t triển th&agrave;nh &ldquo;th&agrave;nh phố s&acirc;n bay&rdquo; tầm cỡ quốc tế.</li>\r\n	<li><strong>Vị tr&iacute;:&nbsp;</strong>Đường ĐT 769 v&agrave; đường Cầu M&ecirc;n, B&igrave;nh Sơn, Long Th&agrave;nh</li>\r\n	<li><strong>Quy m&ocirc;:</strong>&nbsp;49,8 ha gồm 3 ph&acirc;n khu</li>\r\n	<li><strong>Mật độ x&acirc;y dựng:</strong>&nbsp;50%</li>\r\n	<li><strong>Diện t&iacute;ch mảng xanh:</strong>&nbsp;30.000 m2</li>\r\n	<li><strong>Ph&aacute;t triển v&agrave; ph&acirc;n phối:&nbsp;</strong>Kim Oanh Group</li>\r\n	<li><strong>Ph&aacute;p l&yacute;:&nbsp;</strong>Đầy đủ, ra sổ đỏ từng sản phẩm v&agrave;o cuối năm 2020</li>\r\n</ul>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><img alt=\"\" src=\"/upload/images/flcdetail2.jpg\" style=\"float:left; height:281px; margin-left:10px; margin-right:10px; width:500px\" /></p>\r\n\r\n<h2><strong>T&Acirc;M ĐIỂM KẾT NỐI<br />\r\nTRUNG T&Acirc;M &ldquo;TH&Agrave;NH PHỐ S&Acirc;N BAY&rdquo;</strong></h2>\r\n\r\n<p>Tọa lạc&nbsp;tr&ecirc;n hai mặt tiền đường ĐT 769 v&agrave; đường Cầu M&ecirc;n,&nbsp;kết nối trực tiếp bốn tuyến giao th&ocirc;ng trọng điểm: đường V&agrave;nh đai 4, ĐT 769 mới, quốc lộ 51, cao tốc TPHCM &ndash; Long Th&agrave;nh &ndash; Dầu Gi&acirc;y. L&agrave; t&acirc;m điểm kết nối đến c&aacute;c th&agrave;nh phố của tứ gi&aacute;c ph&aacute;t triển: TP. HCM, Bi&ecirc;n H&ograve;a, Long Kh&aacute;nh v&agrave; Vũng T&agrave;u chỉ trong 20-30 ph&uacute;t di chuyển.&nbsp;</p>\r\n\r\n<p>Century City nằm ngay t&acirc;m điểm kết nối đại lộ Bắc Sơn &ndash; Long Th&agrave;nh, c&aacute;ch s&acirc;n bay quốc tế Long Th&agrave;nh chỉ 2km. L&agrave; biểu tượng thịnh vượng của &ldquo;th&agrave;nh phố s&acirc;n bay&rdquo;.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n', 1, 7, '1592824177_hot4.jpg', '', '', '', '', '', '', '', 1200000000, 'chothue.png', 4, 1, 'nha-01', '', 0, 1, 2, 2, 60, '726 Pine Streets', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0'),
(14, 'flc Đồng Nai', 'Tập đoàn FLC vừa có buổi làm việc với UBND tỉnh Đồng Nai, báo cáo ý tưởng dự án khu dân cư nông thôn mới, du lịch, nghỉ dưỡng sinh thái, thể thao và vui chơi giải trí hồ Đa Tôn, huyện Long Thành', '<h2><strong>TỔNG QUAN</strong></h2>\r\n\r\n<ul>\r\n	<li>Century City l&agrave; dự &aacute;n đất nền c&oacute; quy m&ocirc; l&ecirc;n đến 49,8 ha tọa lạc ngay trung t&acirc;m đ&ocirc; thị s&acirc;n bay <img alt=\"\" src=\"/upload/images/flcdetail1.jpg\" style=\"float:right; height:225px; width:400px\" />Long Th&agrave;nh, cảng trung chuyển quốc tế tầm cỡ khu vực ch&acirc;u &Aacute; với c&ocirc;ng suất 100 triệu lượt kh&aacute;ch v&agrave; 5 triệu tấn h&agrave;ng h&oacute;a mỗi năm. Hứa hẹn trở th&agrave;nh một khu đ&ocirc; thị thương mại &ndash; dịch vụ đẳng cấp v&agrave; g&oacute;p phần v&agrave;o bước chuyển m&igrave;nh mạnh mẽ của Long Th&agrave;nh tr&ecirc;n chặng đường ph&aacute;t triển th&agrave;nh &ldquo;th&agrave;nh phố s&acirc;n bay&rdquo; tầm cỡ quốc tế.</li>\r\n	<li><strong>Vị tr&iacute;:&nbsp;</strong>Đường ĐT 769 v&agrave; đường Cầu M&ecirc;n, B&igrave;nh Sơn, Long Th&agrave;nh</li>\r\n	<li><strong>Quy m&ocirc;:</strong>&nbsp;49,8 ha gồm 3 ph&acirc;n khu</li>\r\n	<li><strong>Mật độ x&acirc;y dựng:</strong>&nbsp;50%</li>\r\n	<li><strong>Diện t&iacute;ch mảng xanh:</strong>&nbsp;30.000 m2</li>\r\n	<li><strong>Ph&aacute;t triển v&agrave; ph&acirc;n phối:&nbsp;</strong>Kim Oanh Group</li>\r\n	<li><strong>Ph&aacute;p l&yacute;:&nbsp;</strong>Đầy đủ, ra sổ đỏ từng sản phẩm v&agrave;o cuối năm 2020</li>\r\n</ul>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong><img alt=\"\" src=\"/upload/images/flcdetail2.jpg\" style=\"float:left; height:225px; margin-left:10px; margin-right:10px; width:400px\" />T&Acirc;M ĐIỂM KẾT NỐI</strong><br />\r\n<strong>TRUNG T&Acirc;M &ldquo;TH&Agrave;NH PHỐ S&Acirc;N BAY&rdquo;</strong></p>\r\n\r\n<p>Tọa lạc&nbsp;tr&ecirc;n hai mặt tiền đường ĐT 769 v&agrave; đường Cầu M&ecirc;n,&nbsp;kết nối trực tiếp bốn tuyến giao th&ocirc;ng trọng điểm: đường V&agrave;nh đai 4, ĐT 769 mới, quốc lộ 51, cao tốc TPHCM &ndash; Long Th&agrave;nh &ndash; Dầu Gi&acirc;y. L&agrave; t&acirc;m điểm kết nối đến c&aacute;c th&agrave;nh phố của tứ gi&aacute;c ph&aacute;t triển: TP. HCM, Bi&ecirc;n H&ograve;a, Long Kh&aacute;nh v&agrave; Vũng T&agrave;u chỉ trong 20-30 ph&uacute;t di chuyển.&nbsp;</p>\r\n\r\n<p>Century City nằm ngay t&acirc;m điểm kết nối đại lộ Bắc Sơn &ndash; Long Th&agrave;nh, c&aacute;ch s&acirc;n bay quốc tế Long Th&agrave;nh chỉ 2km. L&agrave; biểu tượng thịnh vượng của &ldquo;th&agrave;nh phố s&acirc;n bay&rdquo;.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n', 1, 4, '1592824191_hot5.jpg', '', '', '', '', '', '', '', 1200000000, 'moidang.png', 5, 1, 'flc-quan-9', '', 0, 3, 2, 2, 70, 'Đường ĐT 769 và đường Cầu Mên, Bình Sơn, Long Thành', '', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', 0, '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0'),
(15, 'Vinhomes Central Park Quận 9', 'Vinhomes Central Park project, invested by Vingroup, is situated in extremely favorable location in the heart of Ho Chi Minh City, located within Tan Cang area, frontage stretching for more than 1km on the bank of Saigon River and a stop on the Ben Thanh – Suoi Tien metro line.', '<h1 dir=\"ltr\"><strong>Apartments For Rent &amp; Sale In Vinhomes Central Park, Ho Chi Minh City</strong></h1>\r\n\r\n<p dir=\"ltr\">Vinhomes Central Park project, invested by Vingroup, is situated in extremely favorable location in the heart of Ho Chi Minh City, located within Tan Cang area, frontage stretching for more than 1km on the bank of Saigon River and a stop on the Ben Thanh &ndash; Suoi Tien metro line.</p>\r\n\r\n<p dir=\"ltr\" style=\"text-align:center\"><img alt=\"\" src=\"/upload/images/vindetail1.jpg\" style=\"height:475px; width:1200px\" /><img alt=\"\" src=\"/upload/images/vindetail1(1).jpg\" style=\"height:475px; width:1200px\" /></p>\r\n\r\n<p dir=\"ltr\">Vinhomes Central Park urban has convenient transportation by roadway, railway, waterway, easy connection to the important area of the city:&nbsp;</p>\r\n\r\n<p dir=\"ltr\">- Frontage stretching over 1km along the bank of Sai Gon river</p>\r\n\r\n<p dir=\"ltr\">- 2 minutes to Ben Thanh - Suoi Tien Metro Line No. 1</p>\r\n\r\n<p dir=\"ltr\">- 3 minutes to the Thu Thiem new urban area</p>\r\n\r\n<p dir=\"ltr\">- 4 minutes to District 1</p>\r\n\r\n<h2 dir=\"ltr\"><strong>1. Details of apartments in Vinhomes Central Park</strong></h2>\r\n\r\n<p style=\"text-align:center\"><img alt=\"\" src=\"/upload/images/vindetail10.jpg\" style=\"height:667px; width:1200px\" /><img alt=\"\" src=\"/upload/images/vindetail2.jpg\" style=\"height:696px; width:1200px\" /></p>\r\n\r\n<p dir=\"ltr\">Vinhomes Central Park urban includes 19 apartment buildings which are Landmark (L1 to L6), The Central (C1-C3), The Park (P1 to P7), Landmark 81 and Landmark Plus with 38 - 50 floors providing more than 10,000 apartments, and penthouses with areas from 45m2 - 400m2, designed from 1- 4 bedrooms.</p>\r\n\r\n<p dir=\"ltr\">All apartments are designed to optimize the living space, get full of natural liight and cool air.</p>\r\n\r\n<h2 dir=\"ltr\"><strong>2. Sample pictures</strong></h2>\r\n\r\n<p dir=\"ltr\" style=\"text-align:center\">&nbsp;</p>\r\n\r\n<p dir=\"ltr\" style=\"text-align:center\"><strong><img alt=\"\" src=\"/upload/images/vindetail3.jpg\" style=\"height:727px; width:1200px\" /></strong></p>\r\n\r\n<p dir=\"ltr\" style=\"text-align:center\"><strong><img alt=\"\" src=\"/upload/images/vindetail4.jpg\" style=\"height:794px; width:1200px\" /></strong></p>\r\n\r\n<p dir=\"ltr\" style=\"text-align:center\"><strong><img alt=\"\" src=\"/upload/images/vindetail5.jpg\" style=\"height:790px; width:1200px\" /></strong></p>\r\n\r\n<p dir=\"ltr\" style=\"text-align:center\"><strong><img alt=\"\" src=\"/upload/images/vindetail6.jpg\" style=\"height:799px; width:1200px\" /></strong></p>\r\n\r\n<p dir=\"ltr\" style=\"text-align:center\"><strong><img alt=\"\" src=\"/upload/images/vindetail2.jpg\" style=\"height:696px; width:1200px\" /><img alt=\"\" src=\"/upload/images/vindetail3.jpg\" style=\"height:727px; width:1200px\" /><img alt=\"\" src=\"/upload/images/vindetail4.jpg\" style=\"height:794px; width:1200px\" /></strong><img alt=\"\" src=\"/upload/images/vindetail5.jpg\" style=\"height:790px; width:1200px\" /><strong><img alt=\"\" src=\"/upload/images/vindetail8.jpg\" style=\"height:610px; width:1200px\" /></strong><em>Sample Apartments in Vinhomes Central Park</em></p>\r\n\r\n<h2 dir=\"ltr\"><strong>3. Utilities</strong></h2>\r\n\r\n<p dir=\"ltr\">- Interlevel Vinschool Central Park</p>\r\n\r\n<p dir=\"ltr\">- International Hospital Vinmec Central Park</p>\r\n\r\n<p dir=\"ltr\">- Luxury Marinas</p>\r\n\r\n<p dir=\"ltr\">- Children&#39;s outdoors play area</p>\r\n\r\n<p dir=\"ltr\">- Business Center: indoor ice rink, Vinpearland Game, modern cinema.</p>\r\n\r\n<p dir=\"ltr\">- Green Park</p>\r\n\r\n<p dir=\"ltr\">- Swimming pool, Gym, system training ground outdoor sports (football, tennis courts, and basketball courts)</p>\r\n\r\n<p dir=\"ltr\">- Security and 24/7 camera system</p>\r\n\r\n<p style=\"text-align:center\"><img alt=\"\" src=\"/upload/images/vindetail7.jpg\" style=\"height:789px; width:1200px\" /></p>\r\n\r\n<p dir=\"ltr\">And there are many other extra advantages only in Vinhomes Central Park</p>\r\n\r\n<p dir=\"ltr\" style=\"text-align:center\"><img alt=\"\" src=\"/upload/images/vindetail10.jpg\" style=\"float:left; height:267px; width:480px\" /><img alt=\"\" src=\"/upload/images/vindetail8.jpg\" style=\"float:left; height:265px; margin-left:10px; margin-right:10px; width:400px\" /></p>\r\n\r\n<h2 dir=\"ltr\"><strong>4. AREA</strong></h2>\r\n\r\n<p dir=\"ltr\">- Interlevel Vinschool Central Park</p>\r\n\r\n<p dir=\"ltr\">- International Hospital Vinmec Central Park</p>\r\n\r\n<p dir=\"ltr\">- Luxury Marinas</p>\r\n\r\n<p dir=\"ltr\">- Children&#39;s outdoors play area</p>\r\n\r\n<p dir=\"ltr\">- Business Center: indoor ice rink, Vinpearland Game, modern cinema.</p>\r\n\r\n<p dir=\"ltr\">- Green Park</p>\r\n\r\n<p dir=\"ltr\">- Swimming pool, Gym, system training ground outdoor sports (football, tennis courts, and basketball courts)</p>\r\n\r\n<p dir=\"ltr\">- Security and 24/7 camera system</p>\r\n', 1, 2, '1592823589_hot1.jpg', '', '', '', '', '', '', '', 1500000000, 'chothue.png', 1, 1, 'vinhomes-central-park-quan-9', '', 0, 4, 2, 2, 60, 'ven sông (nối giữa đường Tôn Đức Thắng – Q.1 và đường Ung Văn Khiêm)', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0'),
(16, 'Dự án nhà demo số 001', 'Precia là tên thương mại của Block 2 thuộc Dự án D’Lusso tại Mặt tiền Đường Nguyễn Thị Định, Quận 2, TP.HCM. Căn hộ Precia cao 21 tầng (2 tầng hầm, 3 tầng TTTM) với hơn 230 căn hộ có diện tích từ 46 – 100 m² do Công ty TNHH Xây dựng và Kinh doanh nhà Điền Phúc Thành làm chủ đầu tư. Dự án căn hộ Precia Quận 2 nằm trên diện tích khu đất: 11.513,1 m2 ~1,15 ha cách khu trung tâm Khu đô thị Thủ Thiêm chỉ 5 phút đi xe máy.', '<p>Precia tọa lạc tr&ecirc;n tuyến đường Nguyễn Thị Định, thuộc phường An Ph&uacute;, Quận 2, Th&agrave;nh phố Hồ Ch&iacute; Minh, dự &aacute;n nằm liền kề với khu căn hộ D&rsquo;Lusso, c&aacute;ch Đại lộ Mai Ch&iacute; Thọ khoảng 400m.</p>\r\n\r\n<h2>Quy m&ocirc; dự &aacute;n căn hộ Precia</h2>\r\n\r\n<p style=\"text-align:center\"><img alt=\"\" src=\"/upload/images/novadetail1.jpg\" style=\"height:619px; width:1200px\" /></p>\r\n\r\n<p>Precia nằm tr&ecirc;n khu đất c&oacute; tổng diện t&iacute;ch 7.156,4 m2, với mật độ x&acirc;y dựng khoảng 28,7%. B&ecirc;n cạnh đ&oacute;, dự &aacute;n c&ograve;n d&agrave;nh 3.475 m2 đất cho c&aacute;c tiện &iacute;ch dịch vụ v&agrave; mảng c&acirc;y xanh.</p>\r\n\r\n<p>Quy m&ocirc; dự &aacute;n gồm 1 khối th&aacute;p, cao 23 tầng, với 1 tầng hầm. Theo thiết kế dự &aacute;n sẽ c&oacute; 1 tầng khối đế thương mại v&agrave; 22 tầng căn hộ.</p>\r\n\r\n<p><img alt=\"\" src=\"/upload/images/Preciadetail3.jpg\" style=\"height:259px; width:460px\" /><img alt=\"\" src=\"/upload/images/novadetail2.jpg\" style=\"float:left; height:338px; margin-left:10px; margin-right:10px; width:500px\" />Dự &aacute;n Precia cung ứng với tổng số 333 căn hộ v&agrave; 8 căn shophouse.</p>\r\n\r\n<p>Cụ thể, c&oacute; 72 căn hộ 1&nbsp;<a href=\"https://cafeland.vn/chu-de-nong/phong-ngu-dep-114/\" target=\"_bank\"><strong>ph&ograve;ng ngủ</strong></a>&nbsp;với diện t&iacute;ch 48,25 m2; 209 căn hộ 2 ph&ograve;ng ngủ với diện t&iacute;ch 66,24 m2; 40 căn hộ 3 ph&ograve;ng ngủ với diện t&iacute;ch 93,9 m2 v&agrave; 12 căn Penthouse với diện t&iacute;ch 116,2 m2.</p>\r\n\r\n<p>Theo đ&oacute;, c&aacute;c tiện &iacute;ch b&ecirc;n trong dự &aacute;n gồm c&oacute;: hồ bơi tr&ecirc;n cao, ph&ograve;ng tập gym, yoga, sky garden, cabana, th&aacute;c nước v&agrave; hồ cảnh quan,&hellip;</p>\r\n\r\n<h2>Th&ocirc;ng tin từ&nbsp;<a href=\"https://cafeland.vn/\" target=\"_bank\"><strong>CafeLand</strong></a></h2>\r\n\r\n<p>Dự &aacute;n căn hộ Precia do C&ocirc;ng ty TNHH Kinh doanh Địa ốc Minh Th&ocirc;ng l&agrave;m chủ đầu tư. C&ocirc;ng ty Cổ phần Đầu tư Bất động sản Rio Land l&agrave; đơn vị ph&aacute;t triển dự &aacute;n.</p>\r\n\r\n<p>C&ocirc;ng ty TNHH Kinh doanh Địa ốc Minh Th&ocirc;ng (thuộc C&ocirc;ng ty TNHH X&acirc;y dựng v&agrave; Kinh doanh nh&agrave; Điền Ph&uacute;c Th&agrave;nh), được th&agrave;nh lập v&agrave;o ng&agrave;y 10/07/2007. Theo th&ocirc;ng tin đăng k&yacute; thay đổi doanh nghiệp ng&agrave;y 19/09/2019, Địa ốc Minh Th&ocirc;ng đang hoạt động với vốn điều lệ 400 tỷ đồng.</p>\r\n\r\n<p>Gi&aacute; b&aacute;n căn hộ được tham khảo tr&ecirc;n thị trường của dự &aacute;n Precia từ 49 triệu/m2.</p>\r\n\r\n<h2>&nbsp;</h2>\r\n\r\n<h2>&nbsp;</h2>\r\n\r\n<h2>Th&ocirc;ng tin nhanh dự &aacute;n căn hộ Precia</h2>\r\n\r\n<p style=\"text-align:center\"><img alt=\"\" src=\"/upload/images/novadetail4.jpg\" style=\"height:675px; width:1200px\" /></p>\r\n\r\n<p style=\"text-align:center\"><img alt=\"\" src=\"/upload/images/flcdetail1.jpg\" style=\"height:675px; width:1200px\" /></p>\r\n\r\n<table align=\"center\" border=\"1\" cellpadding=\"1\" cellspacing=\"1\">\r\n	<tbody>\r\n		<tr>\r\n			<td>\r\n			<p><strong>Dự &aacute;n:</strong>&nbsp;Precia</p>\r\n			</td>\r\n			<td><strong>Tổng diện t&iacute;ch:</strong>&nbsp;7.156,4 m2</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p><strong>Vị tr&iacute;:</strong>&nbsp;Đường Nguyễn Thị Định, phường An Ph&uacute;, Quận 2, Th&agrave;nh phố Hồ Ch&iacute; Minh</p>\r\n			</td>\r\n			<td><strong>Tổng vốn đầu tư:</strong></td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p><strong>Loại h&igrave;nh:&nbsp;</strong>Căn hộ</p>\r\n			</td>\r\n			<td><strong>Ng&agrave;y khởi c&ocirc;ng:</strong></td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p><strong>Chủ đầu tư:</strong>&nbsp;C&ocirc;ng ty TNHH Kinh doanh Địa ốc Minh Th&ocirc;ng</p>\r\n\r\n			<p><strong>Đơn vị ph&aacute;t triển:</strong>&nbsp;C&ocirc;ng ty Cổ phần Đầu tư Bất động sản Rio Land</p>\r\n			</td>\r\n			<td><strong>Năm ho&agrave;n th&agrave;nh:</strong></td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n\r\n<p>&nbsp;</p>\r\n', 1, 5, '1592824158_hot3.jpg', '1596144485_2.jpg', '1596144485_3.jpg', '1596144485_4.jpg', '1596154039_5.jpg', '1596144485_6.jpg', '1596144485_4.jpg', '1596144485_8.jpg', 1200000000, 'chothue.png', 3, 1, 'nha-02', '', 0, 2, 2, 2, 60, '726 Pine Streets', '', '22', '21', '20', '19', '18', '17', '16', '15', '14', '13', '12', '11', '10', '9', '8', '7', '6', '5', '4', '3', '2', '1', 0, 'on', '0', '0', '0', '0', '0', 'on', '0', '0', '0', '0', '0', 'on', '0', '0', '0', '0');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `product_partner`
--

CREATE TABLE `product_partner` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `partner_id` int(11) NOT NULL,
  `order` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `promotion`
--

CREATE TABLE `promotion` (
  `id` int(11) NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `starDate` datetime NOT NULL,
  `endDate` datetime NOT NULL,
  `discount` float NOT NULL,
  `type` int(11) NOT NULL DEFAULT 0,
  `gift_productId` int(11) NOT NULL,
  `deleted` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `SEO`
--

CREATE TABLE `SEO` (
  `id` int(11) NOT NULL,
  `title` text COLLATE utf8_unicode_ci NOT NULL,
  `page` text COLLATE utf8_unicode_ci NOT NULL,
  `meta` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `SEO`
--

INSERT INTO `SEO` (`id`, `title`, `page`, `meta`) VALUES
(1, 'Olivia home', 'Home', 'mota 1'),
(2, 'Olivia', 'Category', 'mota');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `full_name` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `role` int(11) NOT NULL DEFAULT 0,
  `deleted` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `user`
--

INSERT INTO `user` (`id`, `username`, `password`, `full_name`, `email`, `phone`, `role`, `deleted`) VALUES
(1, 'root', 'ed629c55309af42cce378122e20852a6b667b09d', 'root', 'root@gmail.com', '0909090909', 0, 0);

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `about`
--
ALTER TABLE `about`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `comment`
--
ALTER TABLE `comment`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_commentProduct` (`productId`);

--
-- Chỉ mục cho bảng `company_info`
--
ALTER TABLE `company_info`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `detaildistribution`
--
ALTER TABLE `detaildistribution`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_detaildistributionDitributon` (`distributionId`),
  ADD KEY `fk_detaildistributionProduct` (`productId`);

--
-- Chỉ mục cho bảng `distribution`
--
ALTER TABLE `distribution`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_distributionDistributor` (`distributorId`),
  ADD KEY `fk_distributionUser` (`userId`);

--
-- Chỉ mục cho bảng `distributor`
--
ALTER TABLE `distributor`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `event`
--
ALTER TABLE `event`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `gallery_detail`
--
ALTER TABLE `gallery_detail`
  ADD PRIMARY KEY (`id`),
  ADD KEY `gallery_id` (`gallery_id`);
ALTER TABLE `gallery_detail` ADD FULLTEXT KEY `name` (`name`,`description`);

--
-- Chỉ mục cho bảng `history`
--
ALTER TABLE `history`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `home_banner`
--
ALTER TABLE `home_banner`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `home_hot_product`
--
ALTER TABLE `home_hot_product`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `location`
--
ALTER TABLE `location`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `news`
--
ALTER TABLE `news`
  ADD PRIMARY KEY (`id`);
ALTER TABLE `news` ADD FULLTEXT KEY `name` (`name`,`description`);

--
-- Chỉ mục cho bảng `partner`
--
ALTER TABLE `partner`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `partner_registry`
--
ALTER TABLE `partner_registry`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_producer` (`location`),
  ADD KEY `fk_category` (`category_id`);

--
-- Chỉ mục cho bảng `product_partner`
--
ALTER TABLE `product_partner`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `promotion`
--
ALTER TABLE `promotion`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `SEO`
--
ALTER TABLE `SEO`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `about`
--
ALTER TABLE `about`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT cho bảng `comment`
--
ALTER TABLE `comment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `company_info`
--
ALTER TABLE `company_info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT cho bảng `detaildistribution`
--
ALTER TABLE `detaildistribution`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `distribution`
--
ALTER TABLE `distribution`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `distributor`
--
ALTER TABLE `distributor`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `event`
--
ALTER TABLE `event`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `gallery_detail`
--
ALTER TABLE `gallery_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `history`
--
ALTER TABLE `history`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT cho bảng `home_banner`
--
ALTER TABLE `home_banner`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `home_hot_product`
--
ALTER TABLE `home_hot_product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT cho bảng `location`
--
ALTER TABLE `location`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT cho bảng `news`
--
ALTER TABLE `news`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT cho bảng `partner`
--
ALTER TABLE `partner`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT cho bảng `partner_registry`
--
ALTER TABLE `partner_registry`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT cho bảng `product`
--
ALTER TABLE `product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT cho bảng `product_partner`
--
ALTER TABLE `product_partner`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT cho bảng `promotion`
--
ALTER TABLE `promotion`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

<?php 
class M_userpostlist_en extends CI_model
{
	//lay danh sach tat ca product_en
	// public function get_current_page_records($limit, $start)
	// {
	// 	$arr=array();
	// 	$this->db->where('p.deleted',0);
	// 	// $this->db->where('p.active',1);
	// 	$this->db->select('p.id,p.name as name,c.name as category, p.hot, p.active as lock');
	// 	$this->db->from('product_en p');
	// 	$this->db->join('category c', 'p.category_id = c.id');
	// 	$this->db->order_by("p.name", "asc");
	// 	$this->db->limit($limit, $start);
	// 	$query = $this->db->get();
	// 	foreach($query->result() as $row)
	// 	{
	// 		$arr[]=$row;
	// 	}
	// 	return $arr;
	// }

	public function get_current_page_records()
	{
		$arr=array();
		$this->db->where('p.deleted',0);
		$this->db->where('p.type',1);
		$this->db->select('p.id,p.name as name,c.name as category, p.hot, p.price,p.img1, p.active as lock');
		$this->db->from('product_en p');
		$this->db->join('category c', 'p.category_id = c.id');
		$this->db->order_by("p.name", "asc");
		
		$query = $this->db->get();
		foreach($query->result() as $row)
		{
			$arr[]=$row;
		}
		return $arr;
	}

	public function get_total() 
    {
		$this->db->where('deleted',0);
        return $this->db->count_all_results("product_en");
	}
	public function deleteRow($where,$table)
	{
		$this->db->where($where);
		$this->db->delete($table);
		if($this->db->affected_rows()<1)
		{
			return false;
		}else{
			return true;
		}

	}

	public function getProduct($id)
	{
		$this->db->select("	product_en.id,
							product_en.name,
							product_en.detail_des,
							product_en.img1,
							product_en.img2,
							product_en.img3,
							product_en.img4,
							product_en.img5,
							product_en.img6,
							product_en.img7,
							product_en.img8,
							product_en.slug,
							product_en.price,
							product_en.status_product,
							product_en.slug,
							product_en.address,
							category.name_en as `category`,
							product.lating,
							product.longing,
							product_en.payment_period,
							product_en.room,
							product_en.tolet,
							product_en.garages,
							product_en.property_size");
		$this->db->where('product_en.deleted',0);
		$this->db->where('product_en.id',$id);
		$this->db->from('product_en');
		$this->db->join('category','category.id = product_en.category_id');
		return $this->db->get()->row();
	}

	public function getAllProduct()
	{
		$arr=array();
		$this->db->select("	product_en.id,
							product_en.name,
							product_en.detail_des,
							product_en.img1,
							product_en.img2,
							product_en.img3,
							product_en.img4,
							product_en.img5,
							product_en.img6,
							product_en.img7,
							product_en.img8,
							product_en.slug,
							product_en.price,
							product_en.status_product,
							product_en.slug,
							partner.name as partner,
							product_en.address,
							category.name as `category`,
							partner.lating,
							partner.longing,
							product_en.payment_period,
							product_en.room,
							product_en.tolet,
							product_en.garages,
							product_en.property_size");
		$this->db->where('product_en.deleted',0);
		$this->db->where('product_en.type',1);
		$this->db->where('product_en.active',1);
		$this->db->from('product_en');
		$this->db->join('category','category.id = product_en.category_id');
		$this->db->join('partner','partner.id = product_en.partner_id');
		$query = $this->db->get();
		foreach($query->result() as $row)
		{
			$arr[]=$row;
		}
		return $arr;
	}
}
?>
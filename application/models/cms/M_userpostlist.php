<?php 
class M_userpostlist extends CI_model
{
	//lay danh sach tat ca product
	// public function get_current_page_records($limit, $start)
	// {
	// 	$arr=array();
	// 	$this->db->where('p.deleted',0);
	// 	// $this->db->where('p.active',1);
	// 	$this->db->select('p.id,p.name as name,c.name as category, p.hot, p.active as lock');
	// 	$this->db->from('product p');
	// 	$this->db->join('category c', 'p.category_id = c.id');
	// 	$this->db->order_by("p.name", "asc");
	// 	$this->db->limit($limit, $start);
	// 	$query = $this->db->get();
	// 	foreach($query->result() as $row)
	// 	{
	// 		$arr[]=$row;
	// 	}
	// 	return $arr;
	// }

	public function get_current_page_records()
	{
		$arr=array();
		$this->db->where('p.deleted',0);
		$this->db->where('p.type',1);
		$this->db->select('p.id,p.name as name,c.name as category, p.hot, p.price,p.img1, p.active as lock');
		$this->db->from('product p');
		$this->db->join('category c', 'p.category_id = c.id');
		$this->db->order_by("p.name", "asc");
		
		$query = $this->db->get();
		foreach($query->result() as $row)
		{
			$arr[]=$row;
		}
		return $arr;
	}

	public function get_total() 
    {
		$this->db->where('deleted',0);
        return $this->db->count_all_results("product");
	}
	public function deleteRow($where,$table)
	{
		$this->db->where($where);
		$this->db->delete($table);
		if($this->db->affected_rows()<1)
		{
			return false;
		}else{
			return true;
		}

	}
	public function getProduct($id)
	{
		$this->db->select("	product.id,
							product.name,
							product.detail_des,
							product.img1,
							product.img2,
							product.img3,
							product.img4,
							product.img5,
							product.img6,
							product.img7,
							product.img8,
							product.slug,
							product.price,
							product.status_product,
							product.slug,
							product.address,
							category.name as `category`,
							product.lating,
							product.longing,
							product.payment_period,
							product.room,
							product.tolet,
							product.garages,
							product.property_size");
		$this->db->where('product.deleted',0);
		$this->db->where('product.id',$id);
		$this->db->from('product');
		$this->db->join('category','category.id = product.category_id');
		return $this->db->get()->row();
	}

	public function getAllProduct()
	{
		$arr=array();
		$this->db->select("	product.id,
							product.name,
							product.detail_des,
							product.img1,
							product.img2,
							product.img3,
							product.img4,
							product.img5,
							product.img6,
							product.img7,
							product.img8,
							product.slug,
							product.price,
							product.status_product,
							product.slug,
							partner.name as partner,
							product.address,
							category.name as `category`,
							partner.lating,
							partner.longing,
							product.payment_period,
							product.room,
							product.tolet,
							product.garages,
							product.property_size");
		$this->db->where('product.deleted',0);
		$this->db->where('product.type',1);
		$this->db->where('product.active',1);
		$this->db->from('product');
		$this->db->join('category','category.id = product.category_id');
		$this->db->join('partner','partner.id = product.partner_id');
		$query = $this->db->get();
		foreach($query->result() as $row)
		{
			$arr[]=$row;
		}
		return $arr;
	}
}
?>
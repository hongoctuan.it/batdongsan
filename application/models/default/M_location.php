<?php 
class M_location extends M_myweb
{
	private $where;
	private $order_by;
	private $page;
	//lay danh sach tat ca product
	public function __construct() {
		parent::__construct();
		//$this->table = 'member';
		if (!isset($this->where )) 
		   $this->where = array();
		   
		if(!isset($order_by)){
			$this->order_by=array();
		}
	}
	function loadProductPage($locationId=false){
        $arr=array();
		$this->db->select("	product.id,
							product.name as productname,
							product.area,
							product.img1,
							product.room,
							product.address as address,
							product.slug,
							product.tolet,
							category.id as `category_id`,
							category.name as `category_name`,
							category.slug as `category_slug`");
		$this->db->where('product.deleted',0);
		$this->db->where('product.location',$locationId);
		$this->db->from('product');
		$this->db->join('category','category.id = product.category_id');
		$query=$this->db->get();
		foreach($query->result() as $row)
        {
            $arr[]=$row;
		}
		return $arr;
    }
	
	function getlocationId($slug){
        $arr= array();
        $this->db->where('active',1);
        $this->db->where('deleted',0);
        $this->db->where('slug',$slug);
        $this->db->select('id');
        $query = $this->db->get('location');
        return $query->first_row();
    }
	
    function loadLocation($locationId){
        $arr= array();
        $this->db->where('active',1);
        $this->db->where('deleted',0);
        $this->db->where('location',$locationId);
        $this->db->select('name,id,logo,address,lating,longing');
        $query = $this->db->get('partner');
		foreach($query->result() as $row)
        {
            $arr[]=$row;
		}
		return $arr;    
	}
	function filter($properties,$cate,$bedrooms,$bathrooms,$minPrice,$maxPrice,$locationId){
        $arr= array();
        if($this->page<=1){
            $this->offset=0;
        }else{
            $this->offset = ($this->page-1) * $this->limit;
        }
        $query="SELECT  distinct product.*,
						product.name as productname,
                        partner.id as partner_id,
                        partner.logo as partner_logo,
                        location.province as location_province,
                        location.district as location_district,
						category.name as category_name
                        FROM product, partner,location, category
                        WHERE   product.active = 1 
                        AND product.deleted = 0
                        AND product.partner_id=partner.id
                        AND product.location=location.id
                        And product.category_id = category.id";
        if($cate != 'all'){
            $query=$query." AND product.category_id = ".$cate;
        }
        if($properties != 'all'){
            $query=$query." AND product.status_product = "."'".$properties."'";
        }
        if($minPrice != '0'){
            $query=$query." AND  product.price >= ".$minPrice;
        }
        if($maxPrice != '0'){
            $query=$query." AND  product.price <= ".$maxPrice;
        }
        if($bedrooms > '0'){
            $query=$query." AND  product.room = ".$bedrooms;
		}
		if($bathrooms != '0'){
            $query=$query." AND  product.tolet = ".$bathrooms;
		}
		if($locationId != '0'){
            $query=$query." AND  product.location = ".$locationId;
        }
        foreach($this->db->query($query)->result() as $row)
        {
            $arr[]=$row;
        }
        return $arr;
	}
	
	function getcatid($cate=false){
        $arr= array();
        $this->db->where('active',1);
        $this->db->where('deleted',0);
        $this->db->select('id');
        $query = $this->db->get('category');
        return $query->first_row();
    }
}
?>
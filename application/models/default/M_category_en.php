<?php 
class M_category_en extends CI_model
{
    private $page;
    private $limit;
    private $offset;
    private $order;
    private $by;

    public function __construct() {
		parent::__construct();
        if (!isset($this->page)) 
            $this->page = 1;
        if (!isset($this->limit)) 
            $this->limit = 100;
        
        $this->order=false;
        $this->by=false;
	}
	public function getProductList()
	{
        $arr= array();
        $this->db->where('active',1);
        $this->db->where('deleted',0);
        $query = $this->db->get('category');
        foreach($query->result() as $row)
		{   
            $row->product=$this->loadProduct($row->id);
            $arr[$row->id]=$row;
		}
		return $arr;
    }

    function loadProduct($id){
        $arr= array();
        $this->db->where('active',1);
        $this->db->where('deleted',0);
        $this->db->where('category_id',$id);
        $query = $this->db->get('product');

        foreach($query->result() as $row)
        {
            $arr[]=$row;
        }
        return $arr;
    }

    function loadSearchLocation(){
        $arr= array();
        $this->db->where('active',1);
        $this->db->where('deleted',0);
        $this->db->distinct();
        $this->db->select('province,province_en');
        $query = $this->db->get('location');

        foreach($query->result() as $row)
        {
            $arr[]=$row;
        }
        return $arr;
    }

    function getcatslug($cate=false){
        $arr= array();
        $this->db->where('active',1);
        $this->db->where('deleted',0);
        $this->db->select('slug');
        $query = $this->db->get('category');
        return $query->first_row();
    }
    

    function loadProductPage($category_slug=false,$location_id=false,$sort=false,$properties=false){
        $arr= array();
        if($this->page<=1){
            $this->offset=0;
        }else{
            $this->offset = ($this->page-1) * $this->limit;
        }
        $query="SELECT  product.*,
                        partner.id as partner_id,
                        partner.logo as partner_logo,
                        location.province as location_province,
                        location.district as location_district
                        FROM product, partner,location, category
                        WHERE   product.active = 1 
                        AND product.deleted = 0
                        AND product.partner_id=partner.id
                        AND partner.location=location.id
                        And product.category_id = category.id";
        if($category_slug){
            $query = $query." AND category.slug = "."'".$category_slug."'";
        }else if($location_id){
            $query = $query." AND product.location = "."'".$location_id."'";
        }
        if($properties){
            $query = $query." AND product.status_product = "."'".$properties."'";
        }
        
        if($sort){
            if($sort=='newest')
                $query=$query." ORDER BY product.id desc ".$this->by;
            elseif($sort=='hight')
                $query=$query." ORDER BY product.price desc ".$this->by;
            elseif($sort=='low')
                $query=$query." ORDER BY product.price asc ".$this->by;
        }
        $query=$query." LIMIT ".$this->limit." OFFSET ".$this->offset;
        foreach($this->db->query($query)->result() as $row)
        {
            $arr[]=$row;
        }
        return $arr;
    }

    function loadHotProduct($position=false){
        $arr= array();
        if($this->page<=1){
            $this->offset=0;
        }else{
            $this->offset = ($this->page-1) * $this->limit;
        }
        $query="SELECT  product.*,
                        partner.id as partner_id,
                        partner.name as partner_name,
                        location.province as location_province,
                        location.district as location_district
                        FROM product, partner,location
                        WHERE   product.active = 1 
                        AND product.deleted = 0
                        AND product.partner_id=partner.id
                        AND product.location=location.id
                        AND product.hot = ".$position;
        if($this->order){
            $query=$query." ORDER BY a.".$this->order." ".$this->by;
        }
        $query=$query." LIMIT ".$this->limit." OFFSET ".$this->offset;
        foreach($this->db->query($query)->result() as $row)
        {
            $arr[]=$row;
        }
        return $arr;
    }


    function loadLocation(){
        $arr= array();
        $this->db->where('active',1);
        $this->db->where('deleted',0);
        $query=$this->db->get('location');
        foreach($query->result() as $row)
        {
            $this->db->where('location',$row->id);
            $row->count=$this->db->count_all_results('product');
            $arr[]=$row;
        }
        return $arr;
    }

    function loadSearchProduct($properties=false,$province=false,$sort){
        $arr= array();
        if($this->page<=1){
            $this->offset=0;
        }else{
            $this->offset = ($this->page-1) * $this->limit;
        }
        $query="SELECT  product.*,
                        partner.id as partner_id,
                        partner.logo as partner_logo,
                        partner.address,
                        location.province as location_province,
                        location.district as location_district
                        FROM product, partner,location, category
                        WHERE   product.active = 1 
                        AND product.deleted = 0
                        AND product.partner_id=partner.id
                        AND partner.location=location.id
                        And product.category_id = category.id";
        if($properties != 'all'){
            $query=$query." AND product.status_product = "."'".$properties."'";
        }
        if($province != 'all'){
            $query=$query." AND  location.province = "."'".$province."'";
        }
        if($sort){
            if($sort=='newest')
                $query=$query." ORDER BY product.id desc ".$this->by;
            elseif($sort=='hight')
                $query=$query." ORDER BY product.price desc ".$this->by;
            elseif($sort=='low')
                $query=$query." ORDER BY product.price asc ".$this->by;
        }
        $query=$query." LIMIT ".$this->limit." OFFSET ".$this->offset;
        foreach($this->db->query($query)->result() as $row)
        {
            $arr[]=$row;
        }
        return $arr;
    }
    function loadSearchProject($project=false){
        $arr= array();
        if($this->page<=1){
            $this->offset=0;
        }else{
            $this->offset = ($this->page-1) * $this->limit;
        }
        $query="SELECT  product.*,
                        partner.id as partner_id,
                        partner.logo as partner_logo,
                        partner.address,
                        location.province as location_province,
                        location.district as location_district
                        FROM product, partner,location, category
                        WHERE   product.active = 1 
                        AND product.deleted = 0
                        AND product.partner_id=partner.id
                        AND partner.location=location.id
                        And product.category_id = category.id";
        if($project != ''){
            $query=$query." AND partner.slug = "."'".$project."'";
        }
        $query=$query." LIMIT ".$this->limit." OFFSET ".$this->offset;
        foreach($this->db->query($query)->result() as $row)
        {
            $arr[]=$row;
        }
        return $arr;
    }
    function loadCatSearchProduct($keyword,$location,$properties,$minprice,$maxprice,$beds,$sort=false){
        $arr= array();
        if($this->page<=1){
            $this->offset=0;
        }else{
            $this->offset = ($this->page-1) * $this->limit;
        }
        $query="SELECT  distinct product.*,
                        partner.id as partner_id,
                        partner.logo as partner_logo,
                        partner.address,
                        location.province as location_province,
                        location.district as location_district
                        FROM product, partner,location, category
                        WHERE   product.active = 1 
                        AND product.deleted = 0
                        AND product.partner_id=partner.id
                        AND partner.location=location.id
                        And product.category_id = category.id";
        if($keyword != ''){
            $query=$query." AND product.short_des like "."'%".$keyword."%'";
        }
        if($location != 'all'){
            $query=$query." AND location.province = "."'".$location."'";
        }
        if($properties != 'all'){
            $query=$query." AND product.status_product = "."'".$properties."'";
        }
        if($minprice != '0'){
            $query=$query." AND  product.price >= ".$minprice;
        }
        if($maxprice != '0'){
            $query=$query." AND  product.price <= ".$maxprice;
        }
        if($beds != '0'){
            $query=$query." AND  product.room = ".$beds;
        }
        if($sort){
            if($sort=='newest')
                $query=$query." ORDER BY product.id desc ".$this->by;
            elseif($sort=='hight')
                $query=$query." ORDER BY product.price desc ".$this->by;
            elseif($sort=='low')
                $query=$query." ORDER BY product.price asc ".$this->by;
        }
        $query=$query." LIMIT ".$this->limit." OFFSET ".$this->offset;
        foreach($this->db->query($query)->result() as $row)
        {
            $arr[]=$row;
        }
        return $arr;
    }




    function loadLocationProduct($province=false,$district=false){
        $arr= array();
        if($this->page<=1){
            $this->offset=0;
        }else{
            $this->offset = ($this->page-1) * $this->limit;
        }
        $query="SELECT  product.*,
                        partner.id as partner_id,
                        partner.logo as partner_logo,
                        partner.address,
                        location.province as location_province,
                        location.district as location_district
                        FROM product, partner,location, category
                        WHERE   product.active = 1 
                        AND product.deleted = 0
                        AND product.partner_id=partner.id
                        AND product.location=location.id
                        And product.category_id = category.id
                        AND location.province = "."'".$province."'".
                        "AND location.district = "."'".$district."'";
        if($this->order){
            $query=$query." ORDER BY a.".$this->order." ".$this->by;
        }
        $query=$query." LIMIT ".$this->limit." OFFSET ".$this->offset;
        foreach($this->db->query($query)->result() as $row)
        {
            $arr[]=$row;
        }
        return $arr;
    }

    
    public function getCategoryWhere($where)
    {
        $arr= array();
        $this->db->where('active',1);
        $this->db->where('deleted',0);
        $this->db->where($where);
        $query=$this->db->get('category');
        foreach($query->result() as $row)
        {
            $arr[]=$row;
        }
        if(!empty($arr)){
            return $arr;
        }
        return NULL;
    }
    function getPartner($project){
        $arr= array();
        $this->db->where('deleted',0);
        $this->db->where('active',1);
        $this->db->where('slug',$project);
        $query = $this->db->get('partner');
		return $query->row();
    }
    function getExchangerates(){
        $this->db->where('id',14);
        $query = $this->db->get('company_info');
		return $query->row();
    }
    public function setPage($page)
	{
		$this->page     =   $page;
    }
    public function setLimit($limit)
	{
		$this->limit    =   $limit;
    }
    public function setOrderBy($order,$by)
	{
        $this->order    =   $order;
        $this->by       =   $by;
    }

}
?>
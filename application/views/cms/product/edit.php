<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script>
	function toggle(source) {
    var checkboxes = document.querySelectorAll('input[id="expend"]');
    for (var i = 0; i < checkboxes.length; i++) {
        if (checkboxes[i] != source)
            checkboxes[i].checked = source.checked;
    }
}
	$('#add_more').click(function() {
		"use strict";
		$(this).before($("<div/>", {
		id: 'filediv'
		}).fadeIn('slow').append(
		$("<input/>", {
			name: 'file[]',
			type: 'file',
			id: 'file',
			multiple: 'multiple',
			accept: 'image/*'
		})
		));
	});

	$('#upload').click(function(e) {
		"use strict";
		e.preventDefault();

		if (window.filesToUpload.length === 0 || typeof window.filesToUpload === "undefined") {
		alert("No files are selected.");
		return false;
		}

		// Now, upload the files below...
		// https://developer.mozilla.org/en-US/docs/Using_files_from_web_applications#Handling_the_upload_process_for_a_file.2C_asynchronously
	});

	deletePreview = function (ele, i) {
		"use strict";
		try {
		$(ele).parent().remove();
		window.filesToUpload.splice(i, 1);
		} catch (e) {
		console.log(e.message);
		}
	}

	$("#file").on('change', function() {
		"use strict";

		// create an empty array for the files to reside.
		window.filesToUpload = [];

		if (this.files.length >= 1) {
		$("[id^=previewImg]").remove();
		$.each(this.files, function(i, img) {
			var reader = new FileReader(),
			newElement = $("<div id='previewImg" + i + "' class='previewBox'><img /></div>"),
			deleteBtn = $("<span class='delete' onClick='deletePreview(this, " + i + ")'>X</span>").prependTo(newElement),
			preview = newElement.find("img");

			reader.onloadend = function() {
			preview.attr("src", reader.result);
			preview.attr("alt", img.name);
			};

			try {
			window.filesToUpload.push(document.getElementById("file").files[i]);
			} catch (e) {
			console.log(e.message);
			}

			if (img) {
			reader.readAsDataURL(img);
			} else {
			preview.src = "";
			}

			newElement.appendTo("#filediv");
		});
		}
	});
	function preview_images() 
	{
		var total_file=document.getElementById("images").files.length;
		$('#image_preview').empty();
		for(var i=0;i<total_file;i++)
		{
			$('#image_preview').append("<div class='col-md-3'><img class='img-responsive' src='"+URL.createObjectURL(event.target.files[i])+"'></div>");
		}
	}
</script>
<style>
#formdiv {
  text-align: center;
}
#file {
  color: green;
  padding: 5px;
  border: 1px dashed #123456;
  background-color: #f9ffe5;
}
#img {
  width: 17px;
  border: none;
  height: 17px;
  margin-left: -20px;
  margin-bottom: 191px;
}
.upload {
  width: 100%;
  height: 30px;
}
.previewBox {
  text-align: center;
  position: relative;
  width: 150px;
  height: 150px;
  margin-right: 10px;
  margin-bottom: 20px;
  float: left;
}
.previewBox img {
  height: 150px;
  width: 150px;
  padding: 5px;
  border: 1px solid rgb(232, 222, 189);
}
.delete {
  color: red;
  font-weight: bold;
  position: absolute;
  top: 0;
  cursor: pointer;
  width: 20px;
  height:  20px;
  border-radius: 50%;
  background: #ccc;
}
</style>
<?php
$action = !empty($obj) ? site_url('admin/product?act=upd&id=' .$obj->id . "&token=" . $infoLog->token) : site_url('admin/product?act=upd&token=' . $infoLog->token);
$image_01 = !empty($obj) && $obj->img1 != "" ? base_url('assets/public/avatar/' . $obj->img1) : "";
$image_02 = !empty($obj) && $obj->img2 != "" ? base_url('assets/public/avatar/' . $obj->img2) : "";
$image_03 = !empty($obj) && $obj->img3 != "" ? base_url('assets/public/avatar/' . $obj->img3) : "";
$image_04 = !empty($obj) && $obj->img4 != "" ? base_url('assets/public/avatar/' . $obj->img4) : "";
$image_05 = !empty($obj) && $obj->img5 != "" ? base_url('assets/public/avatar/' . $obj->img5) : "";
$image_06 = !empty($obj) && $obj->img6 != "" ? base_url('assets/public/avatar/' . $obj->img6) : "";
$image_07 = !empty($obj) && $obj->img7 != "" ? base_url('assets/public/avatar/' . $obj->img7) : "";
$image_08 = !empty($obj) && $obj->img8 != "" ? base_url('assets/public/avatar/' . $obj->img8) : "";


?>
<!-- begin .app-main -->
<div class="app-main">

	<!-- begin .main-heading -->
	<header class="main-heading shadow-2dp">
		<!-- begin dashhead -->
		<div class="dashhead bg-white">
			<div class="dashhead-titles">
				<h3 class="dashhead-title">Sản phẩm</h3>
			</div>
		</div>
		<!-- END: dashhead -->
	</header>
	<!-- END: .main-heading -->

	<!-- begin .main-content -->
	<div class="main-content bg-clouds">
		<!-- begin .container-fluid -->
		<div class="container-fluid p-t-15">
			<div class="box b-a">
				<div class="box-body">
					<?php if (isset($_SESSION['system_msg'])) {
						echo $_SESSION['system_msg'];
						unset($_SESSION['system_msg']);
					} ?>
					<div class="row">
						<?php echo form_open_multipart($action, array('autocomplete' => "off", 'id' => "userform")); ?>
						<input type="hidden" id="id" name="id" value="<?php echo !empty($obj) ? $id : "" ?>">
						<div class="col-md-4">
							<div class="form-group">
								<label>Loại bất động sản</label>
								<select name="category_id" class="form-control">
									<?php
									$arr = array();
									foreach ($cates as $key) {
										if ($key->level == 0) {
											$arr[] = $key;
											foreach ($cates as $key2) {
												if ($key2->level == 1 && $key->id == $key2->parent) {
													$arr[] = $key2;
													foreach ($cates as $key3) {
														if ($key3->level == 2 && $key2->id == $key3->parent) {
															$arr[] = $key3;
														}
													}
												}
											}
										}
									}
									?>
									<?php foreach ($arr as $key) : ?>
										<option value="<?php echo $key->id ?>" <?php echo  !empty($obj) && $obj->category_id == $key->id ? "selected" : "" ?>><?php if ($key->level == 1) echo '&nbsp;&nbsp;&nbsp;-';
										else if ($key->level == 2) echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;+';
										echo $key->name ?></option>
									<?php endforeach; ?>


								</select>
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group ">
								<label class="control-label">Thuộc dự án</label><br>
								<?php if ($partner) : ?>
									<select name="partner_id" class="form-control">
										<?php foreach ($partner as $item) : ?>
											<option value="<?php echo $item->id; ?>" <?php echo !empty($obj) && $obj->partner_id == $item->id ? "selected" : "" ?>> <?php echo $item->name; ?></option>
										<?php endforeach; ?>
									</select>
								<?php endif; ?>
							</div>
						</div>
						<div class="col-md-2">
							<div class="form-group ">
								<label class="control-label">Phòng ngủ</label>
								<input type="text" class="form-control" name="room" id="price" required value="<?php echo !empty($obj) ?  $obj->room : ""; ?>" />
							</div>
						</div>
						<div class="col-md-2">
							<div class="form-group ">
								<label class="control-label">Phòng tắm</label>
								<input type="text" class="form-control" name="tolet" id="price" required value="<?php echo !empty($obj) ?  $obj->tolet : ""; ?>" />
							</div>
						</div>
						<div class="col-md-1">
							<div class="form-group">
								<label>Tầng</label>
								<input type="text" class="form-control" name="floor" id="floor"  value="<?php echo !empty($obj) ?  $obj->floor : ""; ?>" />
							</div>
						</div>
						<div class="col-md-2">
							<div class="form-group ">
								<label class="control-label">Furnished</label>
								<select name="furnished" class="form-control">
									<option value="Fully" <?php echo  !empty($obj) && $obj->furnished == 'Fully' ? "selected" : "" ?>>Đầy đủ</option>
									<option value="Partly" <?php echo  !empty($obj) && $obj->furnished == 'Partly' ? "selected" : "" ?>>Một phần</option>
									<option value="Unfurnshed" <?php echo  !empty($obj) && $obj->furnished == 'Unfurnshed' ? "selected" : "" ?>>Căn bản</option>
								</select>							
							</div>
						</div>
						<div class="col-md-2">
							<div class="form-group ">
								<label class="control-label">Ownership</label>
								<select name="ownership" class="form-control">
									<option value="freehold" <?php echo  !empty($obj) && $obj->ownership == 'freehold' ? "selected" : "" ?>>Chính chủ/Freehold</option>
									<option value="leasehold" <?php echo  !empty($obj) && $obj->ownership == 'leasehold' ? "selected" : "" ?>>Cho thuê/Leasehold</option>
								</select>							
							</div>
						</div>
						<div class="col-md-1">
							<div class="form-group ">
								<label class="control-label">Diện tích</label>
								<input type="text" class="form-control" name="area" id="price"  value="<?php echo !empty($obj) ?  $obj->area : ""; ?>" />
							</div>
						</div>
						
						<div class="col-md-2">
							<div class="form-group ">
								<label class="control-label">Giá bán/cho thuê VNĐ</label>
								<input type="text" class="form-control" name="price" id="price"  value="<?php echo !empty($obj) ?  $obj->price : ""; ?>" />
							</div>
						</div>
						<div class="col-md-2">
							<div class="form-group ">
								<label class="control-label">Giá bán/cho thuê USD</label>
								<input type="text" class="form-control" name="price" id="price"  value="<?php echo !empty($obj) ?  $obj->price : ""; ?>" />
							</div>
						</div>
						<div class="col-md-2">
							<div class="form-group">
								<label>Hiện trạng</label>
								<select name="status_product" class="form-control">
									<option value="ban.png" <?php echo  !empty($obj) && $obj->status_product == "chothue.png" ? "selected" : "" ?>>Bán</option>
									<option value="daban.png" <?php echo  !empty($obj) && $obj->status_product == "dangban.png" ? "selected" : "" ?>>Đã Bán</option>
									<option value="chothue.png" <?php echo  !empty($obj) && $obj->status_product == "chothue.png" ? "selected" : "" ?>>Cho Thuê</option>
									<option value="dathue.png" <?php echo  !empty($obj) && $obj->status_product == "dathue.png" ? "selected" : "" ?>>Đã Thuê</option>
									<option value="moidang.png" <?php echo  !empty($obj) && $obj->status_product == "moidang.png" ? "selected" : "" ?>>Mới Đăng</option>
									<option value="sapra.png" <?php echo  !empty($obj) && $obj->status_product == "sapra.png" ? "selected" : "" ?>>Sắp Ra</option>

								</select>
							</div>
						</div>
						<div class="clearfix"></div>
						<div class="col-md-12">
							<div class="checkbox" style="background-color:#E6E6E6;padding:8px; border-radius:5px; width:fit-content">
								<label style="color:red"><input type="checkbox" onclick="toggle(this);">Chọn tất cả</label>
							</div>
						</div>
						<div class="col-md-2">
							<div class="checkbox" style="background-color:#E6E6E6;padding:8px; border-radius:5px; width:fit-content">
								<label><input type="checkbox"  <?php if(!empty($obj) && $obj->ac_heating=='on') echo 'checked'?> id="expend" name="ac_heating">Máy lạnh</label>
							</div>
						</div>
						<div class="col-md-2">
							<div class="checkbox" style="background-color:#E6E6E6;padding:8px; border-radius:5px; width:fit-content">
								<label><input type="checkbox" <?php if(!empty($obj) && $obj->balcony=='on') echo 'checked'?> id="expend" name="balcony">Khu vực BBQ</label>
							</div>
						</div>
						<div class="col-md-2">
							<div class="checkbox" style="background-color:#E6E6E6;padding:8px; border-radius:5px; width:fit-content">
								<label><input type="checkbox" <?php if(!empty($obj) && $obj->clubhouse=='on') echo 'checked'?> id="expend" name="clubhouse">CCTV</label>
							</div>
						</div>
						<div class="col-md-2">
							<div class="checkbox" style="background-color:#E6E6E6;padding:8px; border-radius:5px; width:fit-content">
								<label><input type="checkbox" <?php if(!empty($obj) && $obj->dishwasher=='on') echo 'checked'?> id="expend" name="dishwasher">Nhân viên lễ tân</label>
							</div>
						</div>
						<div class="col-md-2">
							<div class="checkbox" style="background-color:#E6E6E6;padding:8px; border-radius:5px; width:fit-content">
								<label><input type="checkbox" <?php if(!empty($obj) && $obj->elevator=='on') echo 'checked'?> id="expend" name="elevator">Phòng tập</label>
							</div>
						</div>
						<div class="col-md-2">
							<div class="checkbox" style="background-color:#E6E6E6;padding:8px; border-radius:5px; width:fit-content">
								<label><input type="checkbox" <?php if(!empty($obj) && $obj->fitness_center=='on') echo 'checked'?> id="expend" name="fitness_center">Sân vườn</label>
							</div>
						</div>
						<div class="col-md-2">
							<div class="checkbox" style="background-color:#E6E6E6;padding:8px; border-radius:5px; width:fit-content">
								<label><input type="checkbox" <?php if(!empty($obj) && $obj->granite_countertops=='on') echo 'checked'?> id="expend" name="granite_countertops">Thư viện</label>
							</div>
						</div>
						<div class="col-md-2">
							<div class="checkbox" style="background-color:#E6E6E6;padding:8px; border-radius:5px; width:fit-content">
								<label><input type="checkbox" <?php if(!empty($obj) && $obj->laundry_facilities=='on') echo 'checked'?> id="expend" name="laundry_facilities">Hướng núi</label>
							</div>
						</div>
						<div class="col-md-2">
							<div class="checkbox" style="background-color:#E6E6E6;padding:8px; border-radius:5px; width:fit-content">
								<label><input type="checkbox" <?php if(!empty($obj) && $obj->modern_kitchen=='on') echo 'checked'?> id="expend" name="modern_kitchen">Bãi đậu xe</label>
							</div>
						</div>
						<div class="col-md-2">
							<div class="checkbox" style="background-color:#E6E6E6;padding:8px; border-radius:5px; width:fit-content">
								<label><input type="checkbox" <?php if(!empty($obj) && $obj->pet_friendly=='on') echo 'checked'?> id="expend" name="pet_friendly">Sân chơi</label>
							</div>
						</div>
						<div class="col-md-2">
							<div class="checkbox" style="background-color:#E6E6E6;padding:8px; border-radius:5px; width:fit-content">
								<label><input type="checkbox" <?php if(!empty($obj) && $obj->pool=='on') echo 'checked'?> id="expend" name="pool">Hướng sông/biển</label>
							</div>
						</div>
						<div class="col-md-2">
							<div class="checkbox" style="background-color:#E6E6E6;padding:8px; border-radius:5px; width:fit-content">
								<label><input type="checkbox" <?php if(!empty($obj) && $obj->spa=='on') echo 'checked'?> id="expend" name="spa">Bảo vệ</label>
							</div>
						</div>
						<div class="col-md-2">
							<div class="checkbox" style="background-color:#E6E6E6;padding:8px; border-radius:5px; width:fit-content">
								<label><input type="checkbox" <?php if(!empty($obj) && $obj->storage_units=='on') echo 'checked'?> id="expend" name="storage_units">Tầng trệt</label>
							</div>
						</div>
						<div class="col-md-2">
							<div class="checkbox" style="background-color:#E6E6E6;padding:8px; border-radius:5px; width:fit-content">
								<label><input type="checkbox" <?php if(!empty($obj) && $obj->tennis_court=='on') echo 'checked'?> id="expend" name="tennis_court">Hồ bơi</label>
							</div>
						</div>
						<div class="col-md-2">
							<div class="checkbox" style="background-color:#E6E6E6;padding:8px; border-radius:5px; width:fit-content">
								<label><input type="checkbox" <?php if(!empty($obj) && $obj->valet_parking=='on') echo 'checked'?> id="expend" name="valet_parking">Sân tennis</label>
							</div>
						</div>
						<div class="col-md-2">
							<div class="checkbox" style="background-color:#E6E6E6;padding:8px; border-radius:5px; width:fit-content">
								<label><input type="checkbox" <?php if(!empty($obj) && $obj->wifi_in_common_areas=='on') echo 'checked'?> id="expend" value="" name="wifi_in_common_areas">WiFi</label>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group ">
								<label class="control-label">Tiêu đề tiếng việt</label>
								<input type="text" class="form-control" name="name" id="name" required  value="<?php echo !empty($obj) ? $obj->name : ""; ?>" />
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group ">
								<label class="control-label">Tiêu đề tiếng anh</label>
								<input type="text" class="form-control" name="name_en" id="name" required  value="<?php echo !empty($obj) ? $obj->name_en : ""; ?>" />
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group ">
								<label class="control-label">Mô tả ngắn tiếng việt</label>
								<textarea  id="content" class="form-control" name="short_des" id="short_des"><?php echo !empty($obj) ?  $obj->short_des : ""; ?></textarea>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group ">
								<label class="control-label">Mô tả ngắn tiếng anh</label>
								<textarea  id="content" class="form-control" name="short_des_en" id="short_des"><?php echo !empty($obj) ?  $obj->short_des_en : ""; ?></textarea>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group ">
								<label class="control-label">Mô tả chi tiết tiếng việt</label>
								<textarea  id="content" class="form-control" name="detail_des" id="short_des"><?php echo !empty($obj) ?  $obj->detail_des : ""; ?></textarea>
							</div>
							<script>
								var editor = CKEDITOR.replace('detail_des', {
									language: 'vi',
									filebrowserBrowseUrl: '<?php echo base_url() . "filemanager/ckfinder/ckfinder.html" ?>',

									filebrowserImageBrowseUrl: '<?php echo base_url() . "filemanager/ckfinder/ckfinder.html?type=Images" ?>',

									filebrowserFlashBrowseUrl: '<?php echo base_url() . "filemanager/ckfinder/ckfinder.html?type=Flash" ?>',

									filebrowserUploadUrl: '<?php echo base_url() . "filemanager/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files" ?>',

									filebrowserImageUploadUrl: '<?php echo base_url() . "filemanager/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images" ?>',

									filebrowserFlashUploadUrl: '<?php echo base_url() . "filemanager/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash" ?>',

								});
							</script>
						</div>
						<div class="col-md-6">
							<div class="form-group ">
								<label class="control-label">Mô tả chi tiết tiếng anh</label>
								<textarea  id="content" class="form-control" name="detail_des_en" id="short_des"><?php echo !empty($obj) ?  $obj->detail_des_en : ""; ?></textarea>
							</div>
							<script>
								var editor = CKEDITOR.replace('detail_des_en', {
									language: 'vi',
									filebrowserBrowseUrl: '<?php echo base_url() . "filemanager/ckfinder/ckfinder.html" ?>',

									filebrowserImageBrowseUrl: '<?php echo base_url() . "filemanager/ckfinder/ckfinder.html?type=Images" ?>',

									filebrowserFlashBrowseUrl: '<?php echo base_url() . "filemanager/ckfinder/ckfinder.html?type=Flash" ?>',

									filebrowserUploadUrl: '<?php echo base_url() . "filemanager/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files" ?>',

									filebrowserImageUploadUrl: '<?php echo base_url() . "filemanager/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images" ?>',

									filebrowserFlashUploadUrl: '<?php echo base_url() . "filemanager/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash" ?>',

								});
							</script>
						</div> 
						<div class="col-md-12">
							<div class="form-group">
								<label>Hình đại diện</label>
								<div>      								
									<input type="file" class="form-control" id="images" name="images[]" onchange="preview_images();" multiple/>
  								</div>
								<div class="row" id="image_preview">
									<?php if(isset($obj)):?>
										<?php if($image_01!=""):?>
											<div class='col-md-3'><img class='img-responsive' src='<?php echo $image_01?>'></div>
										<?php endif?>
										<?php if($image_02!=""):?>
											<div class='col-md-3'><img class='img-responsive' src='<?php echo $image_02?>'></div>
										<?php endif?>
										<?php if($image_03!=""):?>
											<div class='col-md-3'><img class='img-responsive' src='<?php echo $image_03?>'></div>
										<?php endif?>
										<?php if($image_04!=""):?>
											<div class='col-md-3'><img class='img-responsive' src='<?php echo $image_04?>'></div>
										<?php endif?>
										<?php if($image_05!=""):?>
											<div class='col-md-3'><img class='img-responsive' src='<?php echo $image_05?>'></div>
										<?php endif?>
										<?php if($image_06!=""):?>
											<div class='col-md-3'><img class='img-responsive' src='<?php echo $image_06?>'></div>
										<?php endif?>
										<?php if($image_07!=""):?>
											<div class='col-md-3'><img class='img-responsive' src='<?php echo $image_07?>'></div>
										<?php endif?>
										<?php if($image_08!=""):?>
											<div class='col-md-3'><img class='img-responsive' src='<?php echo $image_08?>'></div>
										<?php endif?>

									<?php endif?>
								</div>
							</div>
						</div>
						<div class="clearfix"></div>	
						
						
						
						<div class="clearfix"></div>
						<div class="col-md-3">
							<!-- <a class="btn btn-default" href="<?php echo site_url('admin/product'); ?>">Quay lại</a> -->
							<button type="reset" class="btn btn-warning">Huỷ</button>
							<button type="submit" id="formSubmit" class="btn btn-primary">Gửi</button>
						</div>
						<?php echo form_close(); ?>
					</div>


		</div>
		<!-- END: .container-fluid -->

	</div>
	<!-- END: .main-content -->


</div>
<!-- END: .app-main -->
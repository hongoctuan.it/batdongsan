<?php

$action = $obj?site_url('admin/category?act=upd&id='.$obj->id."&token=".$infoLog->token):site_url('admin/category?act=upd&token='.$infoLog->token);
$image_01 = $obj&&$obj->img!=""?base_url('assets/public/avatar/'.$obj->img):base_url('assets/public/avatar/no-avatar.png');

?>
<!-- begin .app-main -->
<div class="app-main">

	<!-- begin .main-heading -->
	<header class="main-heading shadow-2dp">
		<!-- begin dashhead -->
		<div class="dashhead bg-white">
			<div class="dashhead-titles">
				<h3 class="dashhead-title">Category<strong><?php if(isset($cat_parent)) echo $cat_parent->name ?> </strong></h3>
			</div>
		</div>
		<!-- END: dashhead -->
	</header>
	<!-- END: .main-heading -->

	<!-- begin .main-content -->
	<div class="main-content bg-clouds">

		<!-- begin .container-fluid -->
		<div class="container-fluid p-t-15">
			<div class="box b-a">
				<div class="box-body">
					<?php if(isset($_SESSION['system_msg'])){ echo $_SESSION['system_msg'];unset($_SESSION['system_msg']); }?>
					<div class="row">
						<?php echo form_open_multipart($action,array('autocomplete'=>"off",'id'=>"userform"));?>
							<input type="hidden" id="id" name="id" value="<?php echo $obj?$id:"" ?>">
							<input type="hidden" id="level" name="level" value="<?php echo $obj?$obj->level:"" ?>">
							<div class="col-md-6">
								<div class="form-group required">
									<label class="control-label">Tên Tiếng Việt</label>
									<input type="text" class="form-control" name="name" id="name" required value="<?php echo $obj?$obj->name:"";?>"/>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group required">
									<label class="control-label">Tên Tiếng Anh</label>
									<input type="text" class="form-control" name="name_en" id="name" required value="<?php echo $obj?$obj->name_en:"";?>"/>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label">Slug</label>
									<input type="text" class="form-control" name="slug" id="slug" disabled value="<?php echo $obj?$obj->slug:"";?>"/>
								</div>
							</div>
							<div class="col-md-3">
								<div class="form-group">
									<label>Icon
									</label>
									<div>
										<img id="imgFile_01" class="imgFile" alt="Avatar" src="<?php echo $image_01?>" width="50px"/>
										<input type="file" name="image_01" id="chooseImgFile" onchange="document.getElementById('imgFile_01').src = window.URL.createObjectURL(this.files[0])">
									</div>
								</div>
							</div>
							<div class="clearfix"></div>
							<div class="col-md-3">
								<a class="btn btn-default" href="<?php echo site_url('admin/category');?>">Back</a>
								<button type="reset" class="btn btn-warning">Cacel</button>
								<button type="submit" id="formSubmit" class="btn btn-primary">Save</button>
							</div>
						<?php echo form_close(); ?>
					</div>

		</div>
		<!-- END: .container-fluid -->

	</div>
	<!-- END: .main-content -->
</div>
<!-- END: .app-main -->
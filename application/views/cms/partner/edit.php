<?php
if(isset($id)){
	$action = site_url('admin/partner?act=upd&id='.$id.'&token='.$infoLog->token);
}else{
	$action = site_url('admin/partner?act=new&token='.$infoLog->token);
}
$logo = $obj && $obj->logo != "" ? base_url('assets/public/avatar/' . $obj->logo) : base_url('assets/public/avatar/no-avatar.png');
$image01 = $obj && $obj->img1 != "" ? base_url('assets/public/avatar/' . $obj->img1) : base_url('assets/public/avatar/no-avatar.png');
$image02 = $obj && $obj->img2 != "" ? base_url('assets/public/avatar/' . $obj->img2) : base_url('assets/public/avatar/no-avatar.png');
$image03 = $obj && $obj->img3 != "" ? base_url('assets/public/avatar/' . $obj->img3) : base_url('assets/public/avatar/no-avatar.png');
$image04 = $obj && $obj->img4 != "" ? base_url('assets/public/avatar/' . $obj->img4) : base_url('assets/public/avatar/no-avatar.png');
$image05 = $obj && $obj->img5 != "" ? base_url('assets/public/avatar/' . $obj->img5) : base_url('assets/public/avatar/no-avatar.png');


?>
<!-- begin .app-main -->
<div class="app-main">

	<!-- begin .main-heading -->
	<header class="main-heading shadow-2dp">
		<!-- begin dashhead -->
		<div class="dashhead bg-white">
			<div class="dashhead-titles">
				<h3 class="dashhead-title">Dự án</h3>
			</div>
		</div>
		<!-- END: dashhead -->
	</header>
	<!-- END: .main-heading -->

	<!-- begin .main-content -->
	<div class="main-content bg-clouds">

		<!-- begin .container-fluid -->
		<div class="container-fluid p-t-15">
			<div class="box b-a">
				<div class="box-body">
					<?php if(isset($_SESSION['system_msg'])){ echo $_SESSION['system_msg'];unset($_SESSION['system_msg']); }?>
					<div class="row">
						<?php echo form_open_multipart($action,array('autocomplete'=>"off",'id'=>"userform"));?>
							<div class="col-md-3">
								<div class="form-group required">
									<label class="control-label">Tên Đối tác</label>
									<input type="text" class="form-control" name="Partner[name]" id="name" required value="<?php if(isset($obj->name)) echo $obj->name;?>"/>
								</div>
							</div>
							<div class="col-md-5">
								<div class="form-group required">
									<label class="control-label">Địa chỉ (<a href="https://www.latlong.net/convert-address-to-lat-long.html" target="_blank" style="color:red">Click để lấy tọa độ Lat/Long</a>)</label>
									<input type="text" class="form-control" name="Partner[address]" id="address" required value="<?php if(isset($obj->address)) echo $obj->address;?>"/>
								</div>
							</div>
							<div class="col-md-2">
								<div class="form-group required">
									<label class="control-label">Latitude</label>
									<input type="text" class="form-control" name="Partner[lating]" id="lating" required value="<?php if(isset($obj->lating)) echo $obj->lating;?>"/>
								</div>
							</div>
							<div class="col-md-2">
								<div class="form-group required">
									<label class="control-label">Longitude</label>
									<input type="text" class="form-control" name="Partner[longing]" id="longing" required value="<?php if(isset($obj->longing)) echo $obj->longing;?>"/>
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group">
									<label class="control-label">Slug (có thể bỏ trống)</label>
									<input type="text" class="form-control" name="Partner[slug]" id="slug" disable value="<?php echo $obj ? $obj->slug : ""; ?>" />
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group">
									<label>Location</label>
									<select name="Partner[location]" class="form-control">
									<?php foreach ($locations as $item) : ?>
										<option value="<?php echo $item->id ?>" <?php echo $obj && $obj->location == $item->id ? "selected" : "" ?>><?php echo $item->province?> - <?php echo $item->district?></option>
									<?php endforeach; ?>
								</select>
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group">
									<label class="control-label">Chủ dự án</label>
									<input type="text" class="form-control" name="Partner[constructor]" id="constructor"  value="<?php echo $obj?$obj->constructor:"";?>"/>
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group">
									<label>Logo</label>
									<div>
										<img id="logo" class="imgFile" alt="Avatar" src="<?php echo $logo?>" width="50px" height="50px"/>
										<input type="file" name="logo" id="chooseImgFile" onchange="document.getElementById('logo').src = window.URL.createObjectURL(this.files[0])">
									</div>
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group">
									<label>Image 01
									</label>
									<div>
										<img id="image01" class="imgFile" alt="Avatar" src="<?php echo $image01?>" width="70px" height="40px"/>
										<input type="file" name="image01" id="chooseImgFile" onchange="document.getElementById('image01').src = window.URL.createObjectURL(this.files[0])">
									</div>
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group">
									<label>Image 02
									</label>
									<div>
										<img id="image02" class="imgFile" alt="Avatar" src="<?php echo $image02?>" width="35px" height="20px"/>
										<input type="file" name="image02" id="chooseImgFile" onchange="document.getElementById('image02').src = window.URL.createObjectURL(this.files[0])">
									</div>
								</div>
							</div>
							<div class="clearfix"></div>
							<div class="col-md-4">
								<div class="form-group">
									<label>Image 03
									</label>
									<div>
										<img id="image03" class="imgFile" alt="Avatar" src="<?php echo $image03?>" width="35px" height="20px"/>
										<input type="file" name="image03" id="chooseImgFile" onchange="document.getElementById('image03').src = window.URL.createObjectURL(this.files[0])">
									</div>
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group">
									<label>Image 04
									</label>
									<div>
										<img id="image04" class="imgFile" alt="Avatar" src="<?php echo $image04?>" width="35px" height="20px"/>
										<input type="file" name="image04" id="chooseImgFile" onchange="document.getElementById('image04').src = window.URL.createObjectURL(this.files[0])">
									</div>
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group">
									<label>Image 05
									</label>
									<div>
										<img id="image05" class="imgFile" alt="Avatar" src="<?php echo $image05?>" width="35px" height="20px"/>
										<input type="file" name="image05" id="chooseImgFile" onchange="document.getElementById('image05').src = window.URL.createObjectURL(this.files[0])">
									</div>
								</div>
							</div>
							<div class="col-md-12"></div>
							<div class="col-md-2">
								<div class="form-group required">
									<label class="control-label">Active Listing</label>
									<input type="text" class="form-control" name="Partner[activelisting]" id="activelisting" required value="<?php if(isset($obj->activelisting)) echo $obj->activelisting;?>"/>
								</div>
							</div>
							<div class="col-md-2">
								<div class="form-group required">
									<label class="control-label">Average RENT price:</label>
									<input type="text" class="form-control" name="Partner[rentbedroom01]" id="rentbedroom01" placeholder="1 bedroom"  value="<?php if(isset($obj->rentbedroom01)) echo $obj->rentbedroom01;?>"/>
									<input type="text" class="form-control" name="Partner[rentbedroom02]" id="rentbedroom02" placeholder="2 bedroom"  value="<?php if(isset($obj->rentbedroom02)) echo $obj->rentbedroom02;?>"/>
									<input type="text" class="form-control" name="Partner[rentbedroom03]" id="rentbedroom03" placeholder="3 bedroom"  value="<?php if(isset($obj->rentbedroom03)) echo $obj->rentbedroom03;?>"/>
									<input type="text" class="form-control" name="Partner[rentbedroom04]" id="rentbedroom04" placeholder="4 bedroom" value="<?php if(isset($obj->rentbedroom04)) echo $obj->rentbedroom04;?>"/>
									<input type="text" class="form-control" name="Partner[rentbedroom05]" id="rentbedroom05" placeholder="5 bedroom" value="<?php if(isset($obj->rentbedroom05)) echo $obj->rentbedroom05;?>"/>
									<input type="text" class="form-control" name="Partner[rentpenthouse]" id="rentpenthouse" placeholder="Penthouse/ Duplex" value="<?php if(isset($obj->rentpenthouse)) echo $obj->rentpenthouse;?>"/>
								</div>
							</div>
							<div class="col-md-2">
								<div class="form-group required">
									<label class="control-label">Average SALE price:</label>
									<input type="text" class="form-control" name="Partner[salebedroom01]" id="salebedroom01"  placeholder="1 bedroom" value="<?php if(isset($obj->salebedroom01)) echo $obj->salebedroom01;?>"/>
									<input type="text" class="form-control" name="Partner[salebedroom02]" id="salebedroom02"  placeholder="2 bedroom" value="<?php if(isset($obj->salebedroom02)) echo $obj->salebedroom02;?>"/>
									<input type="text" class="form-control" name="Partner[salebedroom03]" id="salebedroom03"  placeholder="3 bedroom" value="<?php if(isset($obj->salebedroom03)) echo $obj->salebedroom03;?>"/>
									<input type="text" class="form-control" name="Partner[salebedroom04]" id="salebedroom04"  placeholder="4 bedroom" value="<?php if(isset($obj->salebedroom04)) echo $obj->salebedroom04;?>"/>
									<input type="text" class="form-control" name="Partner[salebedroom05]" id="salebedroom05"  placeholder="5 bedroom" value="<?php if(isset($obj->salebedroom05)) echo $obj->salebedroom05;?>"/>		
									<input type="text" class="form-control" name="Partner[salepenthouse]" id="salepenthouse"  placeholder="Penthouse/ Duplex" value="<?php if(isset($obj->salepenthouse)) echo $obj->salepenthouse;?>"/>								</div>
						</div>
							</div>
							<!-- <div class="col-md-2">
								<div class="form-group required">
									<label class="control-label">Property Type</label>
									<input type="text" class="form-control" name="Partner[propertytype]" id="propertytype" required value="<?php if(isset($obj->propertytype)) echo $obj->propertytype;?>"/>
								</div>
							</div>
							<div class="col-md-2">
								<div class="form-group required">
									<label class="control-label">Sale Price</label>
									<input type="text" class="form-control" name="Partner[saleprice]" id="saleprice" required value="<?php if(isset($obj->saleprice)) echo $obj->saleprice;?>"/>
								</div>
							</div> -->
							<div class="col-md-12">
							<div class="form-group required">
								<label class="control-label">Mô tả chi tiết</label>
								<textarea required id="content" class="form-control" name="Partner[description]" id="description"><?php echo !empty($obj) ?  $obj->description : ""; ?></textarea>
							</div>
							<script>
								var editor = CKEDITOR.replace('Partner[description]', {
									language: 'vi',
									filebrowserBrowseUrl: '<?php echo base_url() . "filemanager/ckfinder/ckfinder.html" ?>',

									filebrowserImageBrowseUrl: '<?php echo base_url() . "filemanager/ckfinder/ckfinder.html?type=Images" ?>',

									filebrowserFlashBrowseUrl: '<?php echo base_url() . "filemanager/ckfinder/ckfinder.html?type=Flash" ?>',

									filebrowserUploadUrl: '<?php echo base_url() . "filemanager/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files" ?>',

									filebrowserImageUploadUrl: '<?php echo base_url() . "filemanager/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images" ?>',

									filebrowserFlashUploadUrl: '<?php echo base_url() . "filemanager/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash" ?>',

								});
							</script>
						</div>
							<div class="clearfix"></div>
							<div class="col-md-3">
								<a class="btn btn-default" href="<?php echo site_url('admin/partner');?>">Back</a>
								<button type="reset" class="btn btn-warning">Cancel</button>
								<button type="submit" id="formSubmit" class="btn btn-primary">Save</button>
							</div>
						<?php echo form_close(); ?>
					</div>

		</div>
		<!-- END: .container-fluid -->

	</div>
	<!-- END: .main-content -->

	
</div>
<!-- END: .app-main -->
<?php
$action = $obj?site_url('admin/companyInfo?act=upd&token='.$infoLog->token):site_url('admin/companyInfo?act=upd&token='.$infoLog->token);
?>
<!-- begin .app-main -->
<div class="app-main">

	<!-- begin .main-heading -->
	<header class="main-heading shadow-2dp">
		<!-- begin dashhead -->
		<div class="dashhead bg-white">
			<div class="dashhead-titles">
				<h6 class="dashhead-subtitle">
					Thông tin công ty
			</div>
		</div>
		<!-- END: dashhead -->
	</header>
	<!-- END: .main-heading -->

	<!-- begin .main-content -->
	<div class="main-content bg-clouds">

		<!-- begin .container-fluid -->
		<div class="container-fluid p-t-15">
			<div class="box b-a">
				<div class="box-body">
					<?php if(isset($_SESSION['system_msg'])){ echo $_SESSION['system_msg'];unset($_SESSION['system_msg']); }?>
					<div class="row">
						<?php echo form_open_multipart($action,array('autocomplete'=>"off",'id'=>"userform"));?>
						<?php foreach($infos as $obj):?>
							<?php if($obj->id <= 7): ?>
								<div class="col-md-4">
									<div class="form-group required">
										<label class="control-label"><?php echo $obj?$obj->info:'';?></label>
										<input type="text" class="form-control" name="<?php echo $obj?$obj->info:'';?>" id="<?php echo $obj?$obj->info:'';?>" required value="<?php echo $obj?$obj->value:" ";?>"/>
									</div>
								</div>
							<?php endif;?>
						<?php endforeach;?>
						<div class="col-md-12">
							<?php $obj=$infos[7]; ?>
							<div class="form-group required">
							</div>
							<div class="form-group required">
								<label class="control-label"><?php echo $obj?$obj->info:'';?></label>
								<textarea type="text" class="form-control" name="<?php echo $obj?$obj->info:'';?>" id="<?php echo $obj?$obj->info:'';?>" required><?php echo $obj?$obj->value:" ";?></textarea>
							</div>
							<script>
								var editor = CKEDITOR.replace('<?php echo $obj?$obj->info:'';?>', {
									language: 'vi',
									filebrowserBrowseUrl: '<?php echo base_url() . "filemanager/ckfinder/ckfinder.html" ?>',

									filebrowserImageBrowseUrl: '<?php echo base_url() . "filemanager/ckfinder/ckfinder.html?type=Images" ?>',

									filebrowserFlashBrowseUrl: '<?php echo base_url() . "filemanager/ckfinder/ckfinder.html?type=Flash" ?>',

									filebrowserUploadUrl: '<?php echo base_url() . "filemanager/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files" ?>',

									filebrowserImageUploadUrl: '<?php echo base_url() . "filemanager/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images" ?>',

									filebrowserFlashUploadUrl: '<?php echo base_url() . "filemanager/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash" ?>',

								});
							</script>
						</div>
						<div class="col-md-12">
							<?php $obj=$infos[8]; ?>
							<div class="form-group required">
								
							</div>
							<div class="form-group required">
								<label class="control-label"><?php echo $obj?$obj->info:'';?></label>
								<textarea type="text" class="form-control" name="<?php echo $obj?$obj->info:'';?>" id="<?php echo $obj?$obj->info:'';?>" required><?php echo $obj?$obj->value:" ";?></textarea>
							</div>
							<script>
								var editor = CKEDITOR.replace('<?php echo $obj?$obj->info:'';?>', {
									language: 'vi',
									filebrowserBrowseUrl: '<?php echo base_url() . "filemanager/ckfinder/ckfinder.html" ?>',

									filebrowserImageBrowseUrl: '<?php echo base_url() . "filemanager/ckfinder/ckfinder.html?type=Images" ?>',

									filebrowserFlashBrowseUrl: '<?php echo base_url() . "filemanager/ckfinder/ckfinder.html?type=Flash" ?>',

									filebrowserUploadUrl: '<?php echo base_url() . "filemanager/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files" ?>',

									filebrowserImageUploadUrl: '<?php echo base_url() . "filemanager/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images" ?>',

									filebrowserFlashUploadUrl: '<?php echo base_url() . "filemanager/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash" ?>',

								});
							</script>
						</div>
						<div class="col-md-12">
							<?php $obj=$infos[9]; ?>
							<div class="form-group required">
								<label class="control-label"><?php echo $obj?$obj->info:'';?></label>
								<textarea type="text" class="form-control" name="<?php echo $obj?$obj->info:'';?>" id="<?php echo $obj?$obj->info:'';?>" required><?php echo $obj?$obj->value:" ";?></textarea>
							</div>
							<script>
								var editor = CKEDITOR.replace('<?php echo $obj?$obj->info:'';?>', {
									language: 'vi',
									filebrowserBrowseUrl: '<?php echo base_url() . "filemanager/ckfinder/ckfinder.html" ?>',

									filebrowserImageBrowseUrl: '<?php echo base_url() . "filemanager/ckfinder/ckfinder.html?type=Images" ?>',

									filebrowserFlashBrowseUrl: '<?php echo base_url() . "filemanager/ckfinder/ckfinder.html?type=Flash" ?>',

									filebrowserUploadUrl: '<?php echo base_url() . "filemanager/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files" ?>',

									filebrowserImageUploadUrl: '<?php echo base_url() . "filemanager/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images" ?>',

									filebrowserFlashUploadUrl: '<?php echo base_url() . "filemanager/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash" ?>',

								});
							</script>
						</div>
						<div class="col-md-12">
							<?php $obj=$infos[10]; ?>
							<div class="form-group required">
								<label class="control-label"><?php echo $obj?$obj->info:'';?></label>
								<textarea type="text" class="form-control" name="<?php echo $obj?$obj->info:'';?>" id="<?php echo $obj?$obj->info:'';?>" required><?php echo $obj?$obj->value:" ";?></textarea>
							</div>
							<script>
								var editor = CKEDITOR.replace('<?php echo $obj?$obj->info:'';?>', {
									language: 'vi',
									filebrowserBrowseUrl: '<?php echo base_url() . "filemanager/ckfinder/ckfinder.html" ?>',

									filebrowserImageBrowseUrl: '<?php echo base_url() . "filemanager/ckfinder/ckfinder.html?type=Images" ?>',

									filebrowserFlashBrowseUrl: '<?php echo base_url() . "filemanager/ckfinder/ckfinder.html?type=Flash" ?>',

									filebrowserUploadUrl: '<?php echo base_url() . "filemanager/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files" ?>',

									filebrowserImageUploadUrl: '<?php echo base_url() . "filemanager/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images" ?>',

									filebrowserFlashUploadUrl: '<?php echo base_url() . "filemanager/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash" ?>',

								});
							</script>
						</div>
						<div class="col-md-12">
							<?php $obj=$infos[11]; ?>
							<div class="form-group required">
								<label class="control-label"><?php echo $obj?$obj->info:'';?></label>
								<textarea type="text" class="form-control" name="<?php echo $obj?$obj->info:'';?>" id="<?php echo $obj?$obj->info:'';?>" required><?php echo $obj?$obj->value:" ";?></textarea>
							</div>
							<script>
								var editor = CKEDITOR.replace('<?php echo $obj?$obj->info:'';?>', {
									language: 'vi',
									filebrowserBrowseUrl: '<?php echo base_url() . "filemanager/ckfinder/ckfinder.html" ?>',

									filebrowserImageBrowseUrl: '<?php echo base_url() . "filemanager/ckfinder/ckfinder.html?type=Images" ?>',

									filebrowserFlashBrowseUrl: '<?php echo base_url() . "filemanager/ckfinder/ckfinder.html?type=Flash" ?>',

									filebrowserUploadUrl: '<?php echo base_url() . "filemanager/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files" ?>',

									filebrowserImageUploadUrl: '<?php echo base_url() . "filemanager/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images" ?>',

									filebrowserFlashUploadUrl: '<?php echo base_url() . "filemanager/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash" ?>',

								});
							</script>
						</div>
						<div class="col-md-12">
							<?php $obj=$infos[12]; ?>
							<div class="form-group required">
								<label class="control-label"><?php echo $obj?$obj->info:'';?></label>
								<textarea type="text" class="form-control" name="<?php echo $obj?$obj->info:'';?>" id="<?php echo $obj?$obj->info:'';?>" required><?php echo $obj?$obj->value:" ";?></textarea>
							</div>
							<script>
								var editor = CKEDITOR.replace('<?php echo $obj?$obj->info:'';?>', {
									language: 'vi',
									filebrowserBrowseUrl: '<?php echo base_url() . "filemanager/ckfinder/ckfinder.html" ?>',

									filebrowserImageBrowseUrl: '<?php echo base_url() . "filemanager/ckfinder/ckfinder.html?type=Images" ?>',

									filebrowserFlashBrowseUrl: '<?php echo base_url() . "filemanager/ckfinder/ckfinder.html?type=Flash" ?>',

									filebrowserUploadUrl: '<?php echo base_url() . "filemanager/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files" ?>',

									filebrowserImageUploadUrl: '<?php echo base_url() . "filemanager/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images" ?>',

									filebrowserFlashUploadUrl: '<?php echo base_url() . "filemanager/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash" ?>',

								});
							</script>
						</div>
						<div class="clearfix"></div>
						<div class="col-md-3">
							<a class="btn btn-default" href="<?php echo site_url('admin/companyInfo');?>">Quay lại</a>
							<button type="reset" class="btn btn-warning">Huỷ</button>
							<button type="submit" id="formSubmit" class="btn btn-primary">Gửi</button>
						</div>
						<?php echo form_close(); ?>
					</div>

		</div>
		<!-- END: .container-fluid -->

	</div>
	<!-- END: .main-content -->


</div>
<!-- END: .app-main -->
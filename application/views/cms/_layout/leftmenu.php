<!-- begin .app-side -->
<aside class="app-side">
	<!-- begin .side-content -->
	<div class="side-content">
		<!-- begin user-panel -->
		<div class="user-panel">
			<div class="user-image">
				<a href="#">
					<img class="img-circle" src="<?php echo $avatar;?>" alt="<?php echo $infoLog->userName?>">
				</a>
			</div>
			<div class="user-info">
				<h5><?php echo $infoLog->userName?></h5>
				<ul class="nav">
					<li class="dropdown">
						<a href="#" class="text-turquoise small dropdown-toggle bg-transparent" data-toggle="dropdown">
							<i class="fa fa-fw fa-circle">
							</i> Online
						</a>
						<ul class="dropdown-menu animated flipInY pull-right">
							<li>
								<a href="<?php echo site_url('admin/user?act=profile&id='.$infoLog->logid."&token=".$infoLog->token)?>">Profile</a>
							</li>
							<li role="separator" class="divider"></li>
							<li>
								<a href="<?php echo site_url('admin/logout')?>">
									<i class="fa fa-fw fa-sign-out"></i> Logout
								</a>
							</li>
						</ul>
					</li>
				</ul>
			</div>
		</div>
		<!-- END: user-panel -->
		<!-- begin .side-nav -->
		<nav class="side-nav">
			<!-- BEGIN: nav-content -->
			<ul class="metismenu nav nav-inverse nav-bordered nav-stacked" data-plugin="metismenu">
				<li class="nav-divider"></li>
								<!-- BEGIN: product -->
								<?php if(checkcontroller("product")){?>
				<li>
					<a href="<?php echo site_url('admin/product')?>" <?php echo $this->controller=="product"?'class="active"':''?> >
						<span class="nav-icon">
							<i class="fa fa-fw fa-cubes"></i>
						</span>
						<span class="nav-title">Sản Phẩm</span>
					</a>
				</li>
				<?php } ?>
				<?php if(checkcontroller("userpostlist")){?>
				<li>
					<a href="<?php echo site_url('admin/userpostlist')?>" <?php echo $this->controller=="userpostlist"?'class="active"':''?> >
						<span class="nav-icon">
							<i class="fa fa-fw fa-cubes"></i>
						</span>
						<span class="nav-title">Duyệt Sản phẩm</span>
					</a>
				</li>
				<?php } ?>
				<!-- END: product -->
				<!-- BEGIN: Media -->
				<?php if(checkcontroller("category")){?>
				<li>
					<a href="<?php echo site_url('admin/category')?>" <?php echo $this->controller=="category"?'class="active"':''?> >
						<span class="nav-icon">
							<i class="fa fa-fw fa-cubes"></i>
						</span>
						<span class="nav-title">Danh Mục</span>
					</a>
				</li>
				<?php } ?>
				<!-- END: Media -->
				<?php if(checkcontroller("news")){?>
				<li>
					<a href="<?php echo site_url('admin/news')?>" <?php echo $this->controller=="news"?'class="active"':''?> >
						<span class="nav-icon">
							<i class="fa fa-fw fa-user"></i>
						</span>
						<span class="nav-title">Tin tức</span>
					</a>
				</li>
				<?php } ?>
				<?php if(checkcontroller("news")){?>
				<li>
					<a href="<?php echo site_url('admin/news_en')?>" <?php echo $this->controller=="news_en"?'class="active"':''?> >
						<span class="nav-icon">
							<i class="fa fa-fw fa-user"></i>
						</span>
						<span class="nav-title">Tin tức (English)</span>
					</a>
				</li>
				<?php } ?>
				<!-- BEGIN: user -->
				<?php if(checkcontroller("user")){?>
				<li>
					<a href="<?php echo site_url('admin/user')?>" <?php echo $this->controller=="user"?'class="active"':''?> >
						<span class="nav-icon">
							<i class="fa fa-fw fa-user"></i>
						</span>
						<span class="nav-title">User</span>
					</a>
				</li>
				<?php } ?>
				<!-- END: user -->
				
				<?php if(checkcontroller("location")){?>
				<li>
					<a href="<?php echo site_url('admin/location')?>" <?php echo $this->controller=="location"?'class="active"':''?> >
						<span class="nav-icon">
							<i class="fa fa-fw fa-user"></i>
						</span>
						<span class="nav-title">Location</span>
					</a>
				</li>
				<?php } ?>
				<?php if(checkcontroller("partner")){?>
				<li>
					<a href="<?php echo site_url('admin/partner')?>" <?php echo ($this->controller=="partner"&!isset($_GET['act']))?'class="active"':''?> >
						<span class="nav-icon">
							<i class="fa fa-fw fa-user"></i>
						</span>
						<span class="nav-title">Dự Án</span>
					</a>
				</li>
				<?php } ?>
				<?php if(checkcontroller("introduce")){?>
				<li>
					<a href="<?php echo site_url('admin/introduce')?>" <?php echo $this->controller=="introduce"?'class="active"':''?> >
						<span class="nav-icon">
							<i class="fa fa-fw fa-user"></i>
						</span>
						<span class="nav-title">Giới Thiệu</span>
					</a>
				</li>
				<?php } ?>
				<?php if(checkcontroller("introduce_en")){?>
				<li>
					<a href="<?php echo site_url('admin/introduce_en')?>" <?php echo $this->controller=="introduce_en"?'class="active"':''?> >
						<span class="nav-icon">
							<i class="fa fa-fw fa-user"></i>
						</span>
						<span class="nav-title">Giới Thiệu (En)</span>
					</a>
				</li>
				<?php } ?>
				<?php if(checkcontroller("companyInfo")){?>
				<li>
					<a href="<?php echo site_url('admin/companyInfo')?>" <?php echo $this->controller=="companyInfo"?'class="active"':''?> >
						<span class="nav-icon">
							<i class="fa fa-fw fa-user"></i>
						</span>
						<span class="nav-title">Thông tin công ty</span>
					</a>
				</li>
				<?php } ?>
			
				<li role="separator" class="nav-divider"></li>
				<li class="nav-header">Đối Tác</li>
				
				<li>
					<a href="<?php echo site_url('admin/partner?act=registry-list&token='.$infoLog->token)?>" <?php echo $this->controller=="partner"&&isset($_GET['act'])&&$_GET['act']=='registry-list'?'class="active"':''?> >
						<span class="nav-icon">
							<i class="fa fa-fw fa-user"></i>
						</span>
						<span class="nav-title">Danh sách đối tác liên hệ</span>
					</a>
				</li>
				<li>
					<a href="<?php echo site_url('admin/partner?act=registry-news-list&token='.$infoLog->token)?>" <?php echo $this->controller=="partner"&&isset($_GET['act'])&&$_GET['act']=='registry-news-list'?'class="active"':''?> >
						<span class="nav-icon">
							<i class="fa fa-fw fa-user"></i>
						</span>
						<span class="nav-title">Danh sách đăng ký nhận tin</span>
					</a>
				</li>
				<li>
					<a href="<?php echo site_url('admin/SEO')?>" <?php echo $this->controller=="SEO"?'class="active"':''?> >
						<span class="nav-icon">
							<i class="fa fa-fw fa-user"></i>
						</span>
						<span class="nav-title">SEO</span>
					</a>
				</li>
				<!-- END: user -->
			</ul>
			<!-- END: nav-content -->
		</nav>
		<!-- END: .side-nav -->
	</div>
	<!-- END: .side-content -->
</aside>
<!-- END: .app-side -->
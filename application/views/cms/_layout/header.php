<!doctype html>
<html lang="en">

<head>
	<base href="<?php echo base_url()?>">
	<meta http-equiv=”content-language” content=”vi” />
	<meta http-equiv=”Content-Type” content=”text/html; charset=utf-8″ />
	<!-- <link href=”favicon.ico” rel=”shortcut icon” type=”image/x-icon” /> -->
	<meta name='revisit-after' content='1 days' />
	<meta name=”robots” content=”all” />
	<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
	<title>Olivia - Dashboard</title>

	<!-- Vendor stylesheet files. REQUIRED -->
	<!-- BEGIN: Vendor  -->
	<link rel="stylesheet" href="statics/directory/css/vendor.css">
	<!-- END: core stylesheet files -->

	<!-- Plugin stylesheet files. OPTIONAL -->

	<link rel="stylesheet" href="statics/directory/vendor/jqvmap/jqvmap.css">

	<link rel="stylesheet" href="statics/directory/vendor/dragula/dragula.css">

	<link rel="stylesheet" href="statics/directory/vendor/perfect-scrollbar/perfect-scrollbar.css">

	<!-- END: plugin stylesheet files -->

	<!-- Theme main stlesheet files. REQUIRED -->
	<link rel="stylesheet" href="statics/directory/css/chl.css">
	<link id="theme-list" rel="stylesheet" href="statics/directory/css/theme-peter-river.css">
	<!-- END: theme main stylesheet files -->

	<!-- begin pace.js  -->
	<link rel="stylesheet" href="statics/directory/vendor/pace/themes/blue/pace-theme-minimal.css">
	<script src="<?php echo base_url('filemanager')?>/ckeditor/ckeditor.js"></script>
	<script src="statics/directory/vendor/pace/pace.js"></script>
	<!-- END: pace.js  -->
	<script type="text/javascript">
		var base_url = '<?php echo base_url();?>';
		var token = '<?php echo $infoLog->token?>';
	</script>
	<style>
		.box-body{
			min-height:705px;
		}
	</style>
</head>

<body>
	<!-- begin .app -->
	<div class="app">
		<!-- begin .app-wrap -->
		<div class="app-wrap">


			<!-- begin .app-container -->
			<div class="app-container">
				<?php $this->load->view('cms/_layout/leftmenu')?>
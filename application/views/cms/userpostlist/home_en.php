<?php $action = site_url('admin/userpostlist_en?act=exp&token='.$infoLog->token)?>

<script>
function changeAction(val,id,token){
//    alert(window.location.href+'?act=hot&id='+id+"&val="+val+"&token="+token);
	$.ajax({
		type: "GET",
		url : window.location.href+'?act=hot&id='+id+"&val="+val+"&token="+token,
		error: function (data) {
			console.log("Error occurred when retrieving job executions");
		},
		success: function (data) {
			//how to get the tab object and populate data?
			location.reload();
		}
	});
}
function toggle(source) {
    var checkboxes = document.querySelectorAll('input[type="checkbox"]');
    for (var i = 0; i < checkboxes.length; i++) {
        if (checkboxes[i] != source)
            checkboxes[i].checked = source.checked;
    }
}
</script>
<div class="app-main">
	<!-- begin .main-heading -->
	<header class="main-heading shadow-2dp">
		<!-- begin dashhead -->
		<div class="dashhead bg-white">
			<div class="dashhead-titles">
				<h3 class="dashhead-title">Danh sách sản phẩm</h3>
			</div>
		</div>
		<!-- END: dashhead -->
	</header>
	<!-- END: .main-heading -->

	<!-- begin .main-content -->
	<div class="main-content bg-clouds">

		<!-- begin .container-fluid -->
		<div class="container-fluid p-t-15">
			<div class="box b-a">
				<div class="box-body">
				<?php echo form_open($action);?>

					<?php if(isset($_SESSION['system_msg'])){ echo $_SESSION['system_msg'];unset($_SESSION['system_msg']); }?>
					<div class="p-b-15">
						<button type="submit" style="margin-right:10px" class="btn btn-primary pull-right"></span> Export data</button>
					</div>
					<table data-plugin="datatables" class="table table-striped table-bordered" width="100%" cellspacing="0">
						<thead>
							<tr>
								<th>#</th>
								<th style="text-align:center"><input type="checkbox" value="0" name="checkall" onclick="toggle(this);"></th>
								<th>Tên Sản Phẩm</th>
								<th>Loại Sản Phẩm</th>
								<th>Giá Sản Phẩm</th>
								<th>Hình Ảnh</th>
								<th>SP Nổi Bật</th>
								<th>Duyệt</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
						<?php if($product):
							foreach($product as $key=>$obj){
						?>
							<tr>
								<td><?php echo $obj->id?></td>
								<td style="text-align:center"><input type="checkbox" name="check[<?php echo $obj->id?>]"  value="<?php echo $obj->id?>"></td>
								<td style="width:15%">
									<?php echo $obj->name?>
								</td>
								<td style="width:10%"><?php echo $obj->category?></td>
								<td style="width:10%">
									<?php echo number_format($obj->price);?>
								</td>
								<td><img src="<?php echo base_url('assets/public/avatar/').$obj->img1;?>" alt="" class="img-fluid" style="max-width:200px"></td>
								<td>
								<select  class="form-control" onchange="changeAction(this.value,<?php echo $obj->id?>,'<?php echo $infoLog->token?>')">
									<option value="0">Chọn</option>
									<option <?php echo $obj && $obj->hot == "1" ? "selected" : "" ?>>1</option>
									<option <?php echo $obj && $obj->hot == "2" ? "selected" : "" ?>>2</option>
									<option <?php echo $obj && $obj->hot == "3" ? "selected" : "" ?>>3</option>
									<option <?php echo $obj && $obj->hot == "4" ? "selected" : "" ?>>4</option>
									<option <?php echo $obj && $obj->hot == "5" ? "selected" : "" ?>>5</option>
								</select>
								
								</td>
								<td class="text-center">
									<?php if($obj->lock==0):?>
									<p class="nav-icon">
											<i class="fa fa-fw fa-lock" style="color:red;font-size:2.5rem"></i>
							</p>
									<?php endif;?>
									<?php if($obj->lock==0):?>
									<a href="<?php echo site_url('admin/userpostlist_en?act=unlock&id='.$obj->id."&token=".$infoLog->token);?>" title="Edit" class="btn btn-success">
										Duyệt
									</a>
									<?php else:?>
									<a href="<?php echo site_url('admin/userpostlist_en?act=lock&id='.$obj->id."&token=".$infoLog->token);?>" title="Edit" class="btn btn-warning">
										Khoá
									</a>
									<?php endif;?></td>
								<td>
									<a href="javascript:void(0);" title="Delete" id="btndelete" module="userpostlist_en" data-id="<?php echo $obj->id?>" data-toggle="modal" data-target="#deleteModal" class="btn btn-danger">
										Xoá SP
									</a>
								</td>
							</tr>
						<?php } endif;?>
						
						</tbody>
						
					</table>
					<?php echo isset($links)?$links:""; ?>
					<?php echo form_close(); ?>
				</div>


		</div>
		<!-- END: .container-fluid -->
<!-- 
	</div> -->
	<!-- END: .main-content -->
	
	<?php if(checkaction($this->data['cslug'],'delete')){?>
	<div class="modal fade" id="deleteModal" tabindex="-2" role="dialog" aria-labelledby="deleteModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<h4 class="modal-title" id="deleteModalLabel">Xoá sản phẩm</h4>
				</div>
				<div class="modal-body">
					<div class="md-content">
						Bạn muốn xoá sản phảm?    
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal" id="closeCPModal">Đóng</button>
					<a href="<?php echo site_url('admin/product?act=del&id='.$obj->id."&token=".$infoLog->token);?>" id="confirmDelete" class="btn btn-primary">Xác nhận</a>
				</div>
			</div>
		</div>
	</div>
	<?php }?>
</div>
<!-- END: .app-main -->
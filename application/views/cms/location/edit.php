<?php
	$action = $obj?site_url('admin/location?act=upd&id='.$obj->id."&token=".$infoLog->token):site_url('admin/location?act=upd&token='.$infoLog->token);
$image_01 = $obj&&$obj->img!=""?base_url('assets/public/avatar/'.$obj->img):base_url('assets/public/avatar/no-avatar.png');
?>
<!-- begin .app-main -->
<div class="app-main">

	<!-- begin .main-heading -->
	<header class="main-heading shadow-2dp">
		<!-- begin dashhead -->
		<div class="dashhead bg-white">
			<div class="dashhead-titles">
				<h3 class="dashhead-title">Location</h3>
			</div>
		</div>
		<!-- END: dashhead -->
	</header>
	<!-- END: .main-heading -->

	<!-- begin .main-content -->
	<div class="main-content bg-clouds">

		<!-- begin .container-fluid -->
		<div class="container-fluid p-t-15">
			<div class="box b-a">
				<div class="box-body">
					<?php if(isset($_SESSION['system_msg'])){ echo $_SESSION['system_msg'];unset($_SESSION['system_msg']); }?>
					<div class="row">
						<?php echo form_open_multipart($action,array('autocomplete'=>"off",'id'=>"userform"));?>
							<input type="hidden" id="id" name="id" value="<?php echo $obj?$id:"" ?>">
							<div class="col-md-6">
								<div class="form-group required">
									<label class="control-label">Tỉnh</label>
									<input type="text" class="form-control" name="province" id="province" required value="<?php echo $obj?$obj->province:"";?>"/>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group required">
									<label class="control-label">Province</label>
									<input type="text" class="form-control" name="province_en" id="province" required value="<?php echo $obj?$obj->province_en:"";?>"/>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group required">
									<label class="control-label">Quận/Huyện</label>
									<input type="text" class="form-control" name="district" id="district" required value="<?php echo $obj?$obj->district:"";?>"/>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group required">
									<label class="control-label">District</label>
									<input type="text" class="form-control" name="district_en" id="district" required value="<?php echo $obj?$obj->district_en:"";?>"/>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group required">
									<label class="control-label">Slug</label>
									<input type="text" class="form-control" name="slug" id="slug" disabled value="<?php echo $obj?$obj->slug:"";?>"/>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label>Image</label>
									<div>
										<img id="imgFile_01" class="imgFile" alt="Avatar" src="<?php echo $image_01?>" width="220px" />
										<input type="file" name="image_01" id="chooseImgFile" onchange="document.getElementById('imgFile_01').src = window.URL.createObjectURL(this.files[0])">
									</div>
								</div>
							</div>
							<div class="clearfix"></div>
							<div class="col-md-3">
								<a class="btn btn-default" href="<?php echo site_url('admin/location');?>">Back</a>
								<button type="reset" class="btn btn-warning">Cacel</button>
								<button type="submit" id="formSubmit" class="btn btn-primary">Save</button>
							</div>
						<?php echo form_close(); ?>
					</div>
		</div>
		<!-- END: .container-fluid -->

	</div>
	<!-- END: .main-content -->

	
</div>
<!-- END: .app-main -->

    <section class="news-content-banner">

        <div class="news-content-banner-wrap text-center" style="background-image: url('<?php echo site_url('assets/public/avatar/').$news_data->img;?>">
            <h1 class="news-content-banner-title text-white text-center">
                <?php echo $news_data->name;?>
            </h1>
        </div>
        </div>
    </section>

    <section class="news-content-content my-5">
        <div class="container">
            <div class="row">
                
                <div class="col-md-12 text-center">
                    <div class="news-content-image text-left">
                        <div class="news-content-date">
                            <p><?php echo date('d',strtotime($news_data->created_at));?>
                                <span>Th <?php echo date('m',strtotime($news_data->created_at));?></span>
                            </p>
                        </div>
                    </div>
                    <div class="news-content text-left py-3">
                        <p class="news-content-data"><?php echo $news_data->content;?>
                    </div>
                    <div class="fb-share-button" data-href="<?php echo site_url().$this->uri->uri_string();?>" data-layout="button" data-size="large"></div>
                </div>
            </div>
    </section>

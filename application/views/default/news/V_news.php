
    <section class="news-banner">

        <div class="news-banner-wrap text-center">

            <h1 class="news-banner-title text-white text-center">
               <?php echo isset($cate)?$cate->name:"Tin Tức";?>
            </h1>
        </div>

    </section>

    <section class="news-main my-5">
        <div class="container">
            <div class="row" style="margin-top: 60px;margin-bottom: 40px;">
                <?php foreach($news_datas as $news_data):?>
                        <div class="col-md-4" style="margin-top:20px">
                        <?php if($this->uri->segment(1)=="blogs"):?>
                            <a href="<?php echo site_url('blogs/').$news_data->slug;?>">
                        <?php else:?>
                            <a href="<?php echo site_url('tin-tuc/').$news_data->slug;?>">
                        <?php endif;?>
                                <div class="news-item" >
                                    <img src='<?php echo site_url('assets/public/avatar/').$news_data->img;?>' width="100%" height="200px"/>
                                    <p class="new-title"><?php echo $news_data->name;?></p>
                                    <p class="news-content"><?php $description = strip_tags($news_data->description); //Lược bỏ các tags HTML
                                            echo substr($description, 0, 150);?></p>
                                </div>
                            </a>
                        </div>
                <?php endforeach;?>
            </div>

            <!-- <nav aria-label="Page">
                <ul class="pagination justify-content-center">
                    <li class="page-item <?php echo $this->page==1?"disabled":"";?>">
                        <a class="page-link text-dark" href="<?php echo site_url("tin-tuc/danh-muc");echo isset($slug)?"/".$slug:"";echo "?page=".($current_page-1);?>" tabindex="-1">
                            Trước</a>
                    </li>
                    <?php if($current_page >1):?>
                    <li class="page-item">
                        <a class="page-link page text-dark" href="<?php echo site_url("tin-tuc/danh-muc");echo isset($slug)?"/".$slug:""; echo "?page=".($current_page-1);?>"><?php echo $current_page - 1;?></a>
                    </li>
                    <?php endif;?>
                    <li class="page-item" >
                        <a class="page-link page-active text-dark"  href="<?php echo site_url("tin-tuc/danh-muc");echo isset($slug)?"/".$slug:""; echo "?page=".$current_page;?>"></a>
                        <p class="page-link page-active"><?php echo $current_page;?></p>
                    </li>
                    <?php if($current_page < $total_pages):?>
                    <li class="page-item">
                        <a class="page-link page text-dark" href="<?php echo site_url("tin-tuc/danh-muc");echo isset($slug)?"/".$slug:""; echo "?page=".($current_page+1);?>"><?php echo $current_page + 1;?></a>
                    </li>
                    <?php endif;?>
                    <li class="page-item <?php echo $this->page>=$total_pages?"disabled":"";?>">
                        <a class="page-link text-dark" href="<?php echo site_url("tin-tuc/danh-muc");echo isset($slug)?"/".$slug:""; echo "?page=".($current_page+1);?>">Sau</a>
                    </li>
                </ul>
            </nav> -->
        </div>
    </section>

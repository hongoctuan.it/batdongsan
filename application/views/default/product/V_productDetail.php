<style>
    .img-hover-zoom {
        height: 100%; /* Modify this according to your need */
        overflow: hidden; /* Removing this will break the effects */
    }
    /* Quick-zoom Container */
    .img-hover-zoom--quick-zoom img {
    transform-origin: 0 0;
    transition: transform .25s, visibility .25s ease-in;
    }

    /* The Transformation */
    .img-hover-zoom--quick-zoom:hover img {
    transform: scale(1.03);
    }
}
</style>
<div class="container-fluid" style="margin-top:50px; padding:0px">
    <div class="row col-md-12 content-banner" style="padding:0px; margin:0px">
        <?php include_once 'sliders.php' ?>
    </div>
    <div class="content" style="padding:0px">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-9" style="padding:0px">
                    <p style="font-size:30px;font-weight:600;color:#fc0;margin: 0px;"><?php echo $partner->name!="Tự Do"?$partner->name:$product->name?></p>
                    <span><i class="fa fa-location-arrow"></i>&nbsp;&nbsp;Địa chỉ:</span><span style="font-size:20px;font-weight:600;"> <?php echo $partner->name!="Tự Do"?$partner->address:$product->address?></span>
                    </br>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="col-md-12">
                                <span><i class="fa fa-bed"></i>&nbsp;&nbsp;Số phòng:</span><span style="font-size:20px;font-weight:600;"> <?php echo $partner->name!="Tự Do"?$partner->roomtype:$product->room?></span>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="col-md-12">
                            <?php if($product->status_product!="ban.png"||$product->status_product!="daban.png"||$product->status_product!="sapra.png"):?>
                                <span><i class="fa fa-credit-card"></i>&nbsp;&nbsp;Giá bán:</span><span style="font-size:20px;font-weight:600;"> <?php echo $product->price?></span>
                            <?php elseif($product->status_product!="chothue.png"||$product->status_product!="dathue.png"||$product->status_product!="moidang.png"):?>
                                <span><i class="fa fa-credit-card"></i>&nbsp;&nbsp;Giá Thuê:</span><span style="font-size:20px;font-weight:600;"> <?php echo $product->price?></span>
                            <?php endif;?>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="col-md-12">
                                <span>&nbsp;&nbsp;Diện tích:</span><span style="font-size:20px;font-weight:600;"> <?php echo $product->area?></span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="des-title" style="padding-top:10px;font-weight:600;margin: 0px;color:#fc0" >
                            Tiện Nghi
                        </div>                        
                        <div class="col-md-9 col-12">
                            <div class="row">
                                <div class="col-md-4 col-6 specification-item">
                                    <div style="float:left">
                                        <p class="specification-info" <?php if($product->ac_heating!='on') echo 'style="color:gray"'?>><i class="fa fa-check" aria-hidden="true"></i> Máy lạnh</p>
                                    </div>
                                </div> 
                                <div class="col-md-4 col-6 specification-item">
                                    <div style="float:left">
                                        <p class="specification-info" <?php if($product->balcony!='on') echo 'style="color:gray"'?>><i class="fa fa-check" aria-hidden="true"></i>Khu vực BBQ</p>
                                    </div>
                                </div>
                                <div class="col-md-4 col-6 specification-item">
                                    <div style="float:left">
                                        <p class="specification-info" <?php if($product->clubhouse!='on') echo 'style="color:gray"'?>><i class="fa fa-check" aria-hidden="true"></i>CCTV</p>
                                    </div>
                                </div> 
                                <div class="col-md-4 col-6 specification-item">
                                    <div style="float:left">
                                        <p class="specification-info" <?php if($product->dishwasher!='on') echo 'style="color:gray"'?>><i class="fa fa-check" aria-hidden="true"></i>Nhân viên lễ tân</p>
                                    </div>
                                </div>
                                <div class="col-md-4 col-6 specification-item">
                                    <div style="float:left">
                                        <p class="specification-info" <?php if($product->elevator!='on') echo 'style="color:gray"'?>><i class="fa fa-check" aria-hidden="true"></i>Phòng tập</p>
                                    </div>
                                </div> 
                                <div class="col-md-4 col-6 specification-item">
                                    <div style="float:left">
                                        <p class="specification-info" <?php if($product->fitness_center!='on') echo 'style="color:gray"'?>><i class="fa fa-check" aria-hidden="true"></i>Sân vườn</p>
                                    </div>
                                </div>
                                <div class="col-md-4 col-6 specification-item">
                                    <div style="float:left">
                                        <p class="specification-info" <?php if($product->granite_countertops!='on') echo 'style="color:gray"'?>><i class="fa fa-check" aria-hidden="true"></i>Thư viện</p>
                                    </div>
                                </div> 
                                <div class="col-md-4 col-6 specification-item">
                                    <div style="float:left">
                                        <p class="specification-info" <?php if($product->laundry_facilities!='on') echo 'style="color:gray"'?>><i class="fa fa-check" aria-hidden="true"></i>Hướng núi</p>
                                    </div>
                                </div>
                                <div class="col-md-4 col-6 specification-item">
                                    <div style="float:left">
                                        <p class="specification-info" <?php if($product->modern_kitchen!='on') echo 'style="color:gray"'?>><i class="fa fa-check" aria-hidden="true"></i>Bãi đậu xe</p>
                                    </div>
                                </div> 
                                <div class="col-md-4 col-6 specification-item">
                                    <div style="float:left">
                                        <p class="specification-info" <?php if($product->pet_friendly!='on') echo 'style="color:gray"'?>><i class="fa fa-check" aria-hidden="true"></i>Sân chơi</p>
                                    </div>
                                </div>
                                <div class="col-md-4 col-6 specification-item">
                                    <div style="float:left">
                                        <p class="specification-info" <?php if($product->pool!='on') echo 'style="color:gray"'?>><i class="fa fa-check" aria-hidden="true"></i>Hướng sông/biển</p>
                                    </div>
                                </div> 
                                <div class="col-md-4 col-6 specification-item">
                                    <div style="float:left">
                                        <p class="specification-info" <?php if($product->spa!='on') echo 'style="color:gray"'?>><i class="fa fa-check" aria-hidden="true"></i>Bảo vệ</p>
                                    </div>
                                </div>
                                <div class="col-md-4 col-6 specification-item">
                                    <div style="float:left">
                                        <p class="specification-info" <?php if($product->storage_units!='on') echo 'style="color:gray"'?>><i class="fa fa-check" aria-hidden="true"></i>Tấng trệt</p>
                                    </div>
                                </div> 
                                <div class="col-md-4 col-6 specification-item">
                                    <div style="float:left">
                                        <p class="specification-info" <?php if($product->tennis_court!='on') echo 'style="color:gray"'?>><i class="fa fa-check" aria-hidden="true"></i>Hồ bơi</p>
                                    </div>
                                </div>
                                <div class="col-md-4 col-6 specification-item">
                                    <div style="float:left">
                                        <p class="specification-info" <?php if($product->valet_parking!='on') echo 'style="color:gray"'?>><i class="fa fa-check" aria-hidden="true"></i>Sân tennis</p>
                                    </div>
                                </div> 
                                <div class="col-md-4 col-6 specification-item">
                                    <div style="float:left">
                                        <p class="specification-info" <?php if($product->wifi_in_common_areas!='on') echo 'style="color:gray"'?>><i class="fa fa-check" aria-hidden="true"></i>WiFi</p>
                                    </div>
                                </div> 
                                
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="row">
                        <div class="col-md-12" style="margin-top:15px">
                            <input type="hidden" name="productcontact" class="productcontact" value="<?php echo $product->slug ?>">
                            
                            <a type="button" class="btn btn-info" href="tel:<?php echo $phone?>" style="width:100%; margin-bottom:10px"><i class="fa fa-phone-square" aria-hidden="true" style="font-size:25px; margin-right:10px;top:2px; position:relative"></i><?php echo $phone?></a>
                            <a type="button" class="btn btn-info" href="viber://chat?number=<?php echo $viber?>" style="width:100%; margin-bottom:10px; background-color:#665cac">Viber</a>
                            <a type="button" class="btn btn-info" href="https://api.whatsapp.com/send?phone=<?php echo $whatsapp?>" style="width:100%; margin-bottom:10px; background-color:#01e675">Whatsapp</a>
                            <a type="button" class="btn btn-info" href="https://chat.zalo.me/?phone=<?php echo $zalo?>" style="width:100%; margin-bottom:10px; background-color:#0cb3ff"><img src="<?php echo site_url('assets/zalo.png')?>" width="25px" style="margin-right:10px"/>Liên hệ với zalo</a>
                            <a type="button" class="btn btn-info" href="<?php echo $facebook?>" style="width:100%; margin-bottom:10px; background-color:#2d88ff"><img src="<?php echo site_url('assets/facebook.png')?>" width="25px" style="margin-right:10px"/>Liên hệ với Facebook</a>

                            <input type="text" name="representator" required class="partner-representator form-control contactinput" id="usr" placeholder="Nhập họ tên">
                            <input type="email" class="partner-email form-control contactinput" name="email" aria-describedby="emailHelp" placeholder="Nhập email">
                            <input type="text" class="partner-phone form-control contactinput" name="phone" placeholder="Nhập số điện thoại">
                            <input type="text" class="partner-request form-control contactinput" name="request" placeholder="Nhập yêu cầu">

                            <button type="button" class="partner-registry-btn btn btn-default contactinput" style="background-color:#fc0; width:100% !important; margin-bottom:20px"><i class="fa fa-envelope" aria-hidden="true"></i> Gửi liên hệ</button>
                            <div id="error-message"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row">  
                <div class="des-title" style="font-size:30px;font-weight:600;margin: 0px;color:#fc0" >
                    Mô tả chi tiết
                </div>
                <div class="description">
                    <?php echo $product->detail_des ?>
                </div>
                <div class="des-title" style="font-size:30px;font-weight:600;margin: 0px;color:#fc0" >
                    Thông tin dự án
                </div>
                <div class="description">
                    <?php echo $product->partner_des ?>
                </div>
                <div class="des-title" style="font-size:30px;font-weight:600;color:#fc0;margin: 0px;">
                    Sản phẩm liên quan
                </div>
                <div class="container-fluid">
                    <div class="row">
                        <?php foreach($relatedproduct as $item):?>
                        <a class="col-md-3" style="color:black" href="<?php echo site_url("san-pham/".$item->slug)?>">
                            <div class="related-item" style="background-image:url('<?php echo site_url("assets/public/avatar/".$item->img1) ?>'); width:100%; height:300px; background-position:center; padding:0px">
                                <div class="related-content">
                                    <p class="related-content-title"><?php echo $item->productname;?> <?php echo $item->category_name;?> 
                                    <?php 
                                        if($item->status_product=='chothue.png')
                                            echo 'Cho thuê';
                                        else if($item->status_product=='dathue.png')
                                            echo 'Đã thuê';
                                        else if($item->status_product=='ban.png')
                                            echo 'Bán';
                                        else if($item->status_product=='daban.png')
                                            echo 'Đã bán';
                                        else if($item->status_product=='sapra.png')
                                            echo 'Sắp ra';
                                        else
                                            echo 'Mới đăng';
                                    ?>
                                    </p>
                                    <p class="related-content-address"><?php echo $item->address;?></p>
                                    <p class="related-content-info"><i class="fa fa-bed"></i> <?php echo $item->room;?> <i class="fa fa-bath"></i> <?php echo $item->tolet;?> <i class="fa fa-map"> <?php echo $item->area;?></i></p>
                                </div>
                            </div>
                        </a>
                        <?php endforeach;?>
                    </div>
                </div>
            <div>
        </div>
    </div>
</div>
</div>

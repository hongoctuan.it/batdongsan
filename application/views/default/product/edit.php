<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script>
	function toggle(source) {
    var checkboxes = document.querySelectorAll('input[id="expend"]');
    for (var i = 0; i < checkboxes.length; i++) {
        if (checkboxes[i] != source)
            checkboxes[i].checked = source.checked;
    }
}
</script>
<?php
$action = isset($obj) ? site_url('dang-san-pham?id=' . $obj->id ) : site_url('dang-san-pham');
$image_01 = isset($obj) && $obj->img1 != "" ? base_url('assets/public/avatar/' . $obj->img1) : base_url('assets/public/avatar/no-avatar.png');
$image_02 = isset($obj) && $obj->img2 != "" ? base_url('assets/public/avatar/' . $obj->img2) : base_url('assets/public/avatar/no-avatar.png');
$image_03 = isset($obj) && $obj->img3 != "" ? base_url('assets/public/avatar/' . $obj->img3) : base_url('assets/public/avatar/no-avatar.png');
$image_04 = isset($obj) && $obj->img4 != "" ? base_url('assets/public/avatar/' . $obj->img4) : base_url('assets/public/avatar/no-avatar.png');
$image_05 = isset($obj) && $obj->img5 != "" ? base_url('assets/public/avatar/' . $obj->img5) : base_url('assets/public/avatar/no-avatar.png');
$image_06 = isset($obj) && $obj->img6 != "" ? base_url('assets/public/avatar/' . $obj->img6) : base_url('assets/public/avatar/no-avatar.png');
$image_07 = isset($obj) && $obj->img7 != "" ? base_url('assets/public/avatar/' . $obj->img7) : base_url('assets/public/avatar/no-avatar.png');
$image_08 = isset($obj) && $obj->img8 != "" ? base_url('assets/public/avatar/' . $obj->img8) : base_url('assets/public/avatar/no-avatar.png');


?>
<!-- begin .app-main -->
<div class="app-main">
	<?php echo form_open_multipart($action, array('autocomplete' => "off", 'id' => "userform")); ?>

		<!-- begin .container-fluid -->
		<div class="container-fluid p-t-15">
			<?php if (isset($_SESSION['system_msg'])) {
				echo $_SESSION['system_msg'];
				unset($_SESSION['system_msg']);
			} ?>
			<div class="row">
				<input type="hidden" id="id" name="id" value="<?php echo isset($obj) ? $id : "" ?>">
				<h1 class="col-md-12" style="margin-bottom:20px">Đăng tin/Post</h1>
				<div class="col-md-6">
					<div class="form-group required">
						<label class="control-label">Địa chỉ/Address</label>
						<input type="text" class="form-control" name="address" id="address" required value="<?php echo isset($obj) ? $obj->address : ""; ?>" />
					</div>
				</div>
				<div class="col-md-3">
					<div class="form-group">
						<label>Vị Trí/Location</label>
						<select name="location" class="form-control">
							<?php foreach ($locations as $item) : ?>
								<option value="<?php echo $item->id ?>" <?php echo isset($obj) && $obj->location == $item->id ? "selected" : "" ?>><?php echo $item->province?> - <?php echo $item->district?></option>
							<?php endforeach; ?>
						</select>
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
						<label>Loại bất động sản/Property type</label>
						<select name="category_id" class="form-control">
							<?php
							$arr = array();
							foreach ($cates as $key) {
								if ($key->level == 0) {
									$arr[] = $key;
									foreach ($cates as $key2) {
										if ($key2->level == 1 && $key->id == $key2->parent) {
											$arr[] = $key2;
											foreach ($cates as $key3) {
												if ($key3->level == 2 && $key2->id == $key3->parent) {
													$arr[] = $key3;
												}
											}
										}
									}
								}
							}
							?>
							<?php foreach ($arr as $key) : ?>
								<option value="<?php echo $key->id ?>" <?php echo  !empty($obj) && $obj->category_id == $key->id ? "selected" : "" ?>><?php if ($key->level == 1) echo '&nbsp;&nbsp;&nbsp;-';
								else if ($key->level == 2) echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;+';
								echo $key->name ?></option>
							<?php endforeach; ?>


						</select>
					</div>
				</div>
				<div class="col-md-2">
					<div class="form-group ">
						<label class="control-label">Phòng ngủ/Bedroom</label>
						<input type="text" class="form-control" name="room" id="price" required value="<?php echo !empty($obj) ?  $obj->room : ""; ?>" />
					</div>
				</div>
				<div class="col-md-2">
					<div class="form-group ">
						<label class="control-label">Phòng tắm/Bathroom</label>
						<input type="text" class="form-control" name="tolet" id="price"  required value="<?php echo !empty($obj) ?  $obj->tolet : ""; ?>" />
					</div>
				</div>
				<div class="col-md-2">
					<div class="form-group">
						<label>Tầng/Floors</label>
						<input type="text" class="form-control" name="floor" id="floor"  value="<?php echo !empty($obj) ?  $obj->floor : ""; ?>" />
					</div>
				</div>
				<div class="col-md-3">
					<div class="form-group ">
						<label class="control-label">Furnished</label>
						<select name="furnished" class="form-control">
							<option value="Fully" <?php echo  !empty($obj) && $obj->furnished == 'Fully' ? "selected" : "" ?>>Đầy đủ</option>
							<option value="Partly" <?php echo  !empty($obj) && $obj->furnished == 'Partly' ? "selected" : "" ?>>Một phần</option>
							<option value="Unfurnshed" <?php echo  !empty($obj) && $obj->furnished == 'Unfurnshed' ? "selected" : "" ?>>Căn bản</option>
						</select>							
					</div>
				</div>
				<div class="col-md-3">
					<div class="form-group ">
						<label class="control-label">Ownership</label>
						<select name="ownership" class="form-control">
							<option value="freehold" <?php echo  !empty($obj) && $obj->ownership == 'freehold' ? "selected" : "" ?>>Chính chủ/Freehold</option>
							<option value="leasehold" <?php echo  !empty($obj) && $obj->ownership == 'leasehold' ? "selected" : "" ?>>Cho thuê/Leasehold</option>
						</select>							
					</div>
				</div>
				<div class="col-md-2">
					<div class="form-group ">
						<label class="control-label">Diện tích/Area</label>
						<input type="text" class="form-control" name="area" id="price"  value="<?php echo !empty($obj) ?  $obj->area : ""; ?>" />
					</div>
				</div>
				
				<div class="col-md-3">
					<div class="form-group ">
						<label class="control-label">Giá bán-thuê/Price sale-rent VNĐ</label>
						<input type="text" class="form-control" name="price" id="price"  value="<?php echo !empty($obj) ?  $obj->price : ""; ?>" />
					</div>
				</div>
				<div class="col-md-3">
					<div class="form-group ">
						<label class="control-label">Giá bán-thuê/Price sale-rent USD</label>
						<input type="text" class="form-control" name="price" id="price"  value="<?php echo !empty($obj) ?  $obj->price : ""; ?>" />
					</div>
				</div>
				<div class="col-md-2">
					<div class="form-group">
						<label>Hiện trạng/Status</label>
						<select name="status_product" class="form-control">
							<option value="ban.png" <?php echo  !empty($obj) && $obj->status_product == "chothue.png" ? "selected" : "" ?>>Bán</option>
							<!-- <option value="daban.png" <?php echo  !empty($obj) && $obj->status_product == "dangban.png" ? "selected" : "" ?>>Đã Bán</option> -->
							<option value="chothue.png" <?php echo  !empty($obj) && $obj->status_product == "chothue.png" ? "selected" : "" ?>>Cho Thuê</option>
							<!-- <option value="dathue.png" <?php echo  !empty($obj) && $obj->status_product == "dathue.png" ? "selected" : "" ?>>Đã Thuê</option> -->
							<!-- <option value="moidang.png" <?php echo  !empty($obj) && $obj->status_product == "moidang.png" ? "selected" : "" ?>>Mới Đăng</option> -->
							<!-- <option value="sapra.png" <?php echo  !empty($obj) && $obj->status_product == "sapra.png" ? "selected" : "" ?>>Sắp Ra</option> -->
						</select>
					</div>
				</div>

				<div class="col-md-2">
					<div class="form-group">
						<label>Latitude</label>
						<input type="text" class="form-control" name="lating" id="lating" required value="<?php echo isset($obj) ? $obj->lating : ""; ?>" />
					</div>
				</div>
				<div class="col-md-2">
					<div class="form-group">
						<label>Longitude</label>
						<input type="text" class="form-control" name="longing" id="longing" required value="<?php echo isset($obj) ? $obj->longing : ""; ?>" />
					</div>
				</div>
				<div class="col-md-12"></div>
				<div class="col-md-12">
					<div class="checkbox" style="background-color:#E6E6E6;padding:8px; border-radius:5px; width:fit-content; margin:5px">
						<label style="color:red"><input type="checkbox" onclick="toggle(this);">Chọn tất cả</label>
					</div>
				</div>
				<div class="col-md-2">
					<div class="checkbox" style="background-color:#E6E6E6;padding:8px; border-radius:5px; width:fit-content; margin:5px">
						<label><input type="checkbox"  <?php if(!empty($obj) && $obj->ac_heating=='on') echo 'checked'?> id="expend" name="ac_heating">Máy lạnh</label>
					</div>
				</div>
				<div class="col-md-2">
					<div class="checkbox" style="background-color:#E6E6E6;padding:8px; border-radius:5px; width:fit-content; margin:5px">
						<label><input type="checkbox" <?php if(!empty($obj) && $obj->balcony=='on') echo 'checked'?> id="expend" name="balcony">Khu vực BBQ</label>
					</div>
				</div>
				<div class="col-md-2">
					<div class="checkbox" style="background-color:#E6E6E6;padding:8px; border-radius:5px; width:fit-content; margin:5px">
						<label><input type="checkbox" <?php if(!empty($obj) && $obj->clubhouse=='on') echo 'checked'?> id="expend" name="clubhouse">CCTV</label>
					</div>
				</div>
				<div class="col-md-2">
					<div class="checkbox" style="background-color:#E6E6E6;padding:8px; border-radius:5px; width:fit-content; margin:5px">
						<label><input type="checkbox" <?php if(!empty($obj) && $obj->dishwasher=='on') echo 'checked'?> id="expend" name="dishwasher">Nhân viên lễ tân</label>
					</div>
				</div>
				<div class="col-md-2">
					<div class="checkbox" style="background-color:#E6E6E6;padding:8px; border-radius:5px; width:fit-content; margin:5px">
						<label><input type="checkbox" <?php if(!empty($obj) && $obj->elevator=='on') echo 'checked'?> id="expend" name="elevator">Phòng tập</label>
					</div>
				</div>
				<div class="col-md-2">
					<div class="checkbox" style="background-color:#E6E6E6;padding:8px; border-radius:5px; width:fit-content; margin:5px">
						<label><input type="checkbox" <?php if(!empty($obj) && $obj->fitness_center=='on') echo 'checked'?> id="expend" name="fitness_center">Sân vườn</label>
					</div>
				</div>
				<div class="col-md-2">
					<div class="checkbox" style="background-color:#E6E6E6;padding:8px; border-radius:5px; width:fit-content; margin:5px">
						<label><input type="checkbox" <?php if(!empty($obj) && $obj->granite_countertops=='on') echo 'checked'?> id="expend" name="granite_countertops">Thư viện</label>
					</div>
				</div>
				<div class="col-md-2">
					<div class="checkbox" style="background-color:#E6E6E6;padding:8px; border-radius:5px; width:fit-content; margin:5px">
						<label><input type="checkbox" <?php if(!empty($obj) && $obj->laundry_facilities=='on') echo 'checked'?> id="expend" name="laundry_facilities">Hướng núi</label>
					</div>
				</div>
				<div class="col-md-2">
					<div class="checkbox" style="background-color:#E6E6E6;padding:8px; border-radius:5px; width:fit-content; margin:5px">
						<label><input type="checkbox" <?php if(!empty($obj) && $obj->modern_kitchen=='on') echo 'checked'?> id="expend" name="modern_kitchen">Bãi đậu xe</label>
					</div>
				</div>
				<div class="col-md-2">
					<div class="checkbox" style="background-color:#E6E6E6;padding:8px; border-radius:5px; width:fit-content; margin:5px">
						<label><input type="checkbox" <?php if(!empty($obj) && $obj->pet_friendly=='on') echo 'checked'?> id="expend" name="pet_friendly">Sân chơi</label>
					</div>
				</div>
				<div class="col-md-2">
					<div class="checkbox" style="background-color:#E6E6E6;padding:8px; border-radius:5px; width:fit-content; margin:5px">
						<label><input type="checkbox" <?php if(!empty($obj) && $obj->pool=='on') echo 'checked'?> id="expend" name="pool">Hướng sông/biển</label>
					</div>
				</div>
				<div class="col-md-2">
					<div class="checkbox" style="background-color:#E6E6E6;padding:8px; border-radius:5px; width:fit-content; margin:5px">
						<label><input type="checkbox" <?php if(!empty($obj) && $obj->spa=='on') echo 'checked'?> id="expend" name="spa">Bảo vệ</label>
					</div>
				</div>
				<div class="col-md-2">
					<div class="checkbox" style="background-color:#E6E6E6;padding:8px; border-radius:5px; width:fit-content; margin:5px">
						<label><input type="checkbox" <?php if(!empty($obj) && $obj->storage_units=='on') echo 'checked'?> id="expend" name="storage_units">Tầng trệt</label>
					</div>
				</div>
				<div class="col-md-2">
					<div class="checkbox" style="background-color:#E6E6E6;padding:8px; border-radius:5px; width:fit-content; margin:5px">
						<label><input type="checkbox" <?php if(!empty($obj) && $obj->tennis_court=='on') echo 'checked'?> id="expend" name="tennis_court">Hồ bơi</label>
					</div>
				</div>
				<div class="col-md-2">
					<div class="checkbox" style="background-color:#E6E6E6;padding:8px; border-radius:5px; width:fit-content; margin:5px">
						<label><input type="checkbox" <?php if(!empty($obj) && $obj->valet_parking=='on') echo 'checked'?> id="expend" name="valet_parking">Sân tennis</label>
					</div>
				</div>
				<div class="col-md-2">
					<div class="checkbox" style="background-color:#E6E6E6;padding:8px; border-radius:5px; width:fit-content; margin:5px">
						<label><input type="checkbox" <?php if(!empty($obj) && $obj->wifi_in_common_areas=='on') echo 'checked'?> id="expend" value="" name="wifi_in_common_areas">WiFi</label>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group ">
						<label class="control-label">Tiêu đề tiếng việt</label>
						<input type="text" class="form-control" name="name" id="name" required  value="<?php echo !empty($obj) ? $obj->name : ""; ?>" />
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group ">
						<label class="control-label">Tiêu đề tiếng anh</label>
						<input type="text" class="form-control" name="name_en" id="name" required  value="<?php echo !empty($obj) ? $obj->name_en : ""; ?>" />
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group ">
						<label class="control-label">Mô tả ngắn tiếng việt</label>
						<textarea  id="content" class="form-control" name="short_des" id="short_des"><?php echo !empty($obj) ?  $obj->short_des : ""; ?></textarea>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group ">
						<label class="control-label">Mô tả ngắn tiếng anh</label>
						<textarea  id="content" class="form-control" name="short_des_en" id="short_des"><?php echo !empty($obj) ?  $obj->short_des_en : ""; ?></textarea>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group ">
						<label class="control-label">Mô tả chi tiết tiếng việt</label>
						<textarea  id="content" class="form-control" name="detail_des" id="short_des"><?php echo !empty($obj) ?  $obj->detail_des : ""; ?></textarea>
					</div>
					<script>
						var editor = CKEDITOR.replace('detail_des', {
							language: 'vi',
							filebrowserBrowseUrl: '<?php echo base_url() . "filemanager/ckfinder/ckfinder.html" ?>',

							filebrowserImageBrowseUrl: '<?php echo base_url() . "filemanager/ckfinder/ckfinder.html?type=Images" ?>',

							filebrowserFlashBrowseUrl: '<?php echo base_url() . "filemanager/ckfinder/ckfinder.html?type=Flash" ?>',

							filebrowserUploadUrl: '<?php echo base_url() . "filemanager/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files" ?>',

							filebrowserImageUploadUrl: '<?php echo base_url() . "filemanager/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images" ?>',

							filebrowserFlashUploadUrl: '<?php echo base_url() . "filemanager/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash" ?>',

						});
					</script>
						</div>
						<div class="col-md-6">
							<div class="form-group ">
								<label class="control-label">Mô tả chi tiết tiếng anh</label>
								<textarea  id="content" class="form-control" name="detail_des_en" id="short_des"><?php echo !empty($obj) ?  $obj->detail_des_en : ""; ?></textarea>
							</div>
							<script>
								var editor = CKEDITOR.replace('detail_des_en', {
									language: 'vi',
									filebrowserBrowseUrl: '<?php echo base_url() . "filemanager/ckfinder/ckfinder.html" ?>',

									filebrowserImageBrowseUrl: '<?php echo base_url() . "filemanager/ckfinder/ckfinder.html?type=Images" ?>',

									filebrowserFlashBrowseUrl: '<?php echo base_url() . "filemanager/ckfinder/ckfinder.html?type=Flash" ?>',

									filebrowserUploadUrl: '<?php echo base_url() . "filemanager/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files" ?>',

									filebrowserImageUploadUrl: '<?php echo base_url() . "filemanager/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images" ?>',

									filebrowserFlashUploadUrl: '<?php echo base_url() . "filemanager/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash" ?>',

								});
							</script>
						</div>
				<div class="col-md-3">
					<div class="form-group">
						<label>Hình đại diện</label>
						<div>
							<img style="width:100%" id="imgFile_01" class="imgFile" alt="Avatar" src="<?php echo $image_01 ?>" />
							<input type="file" name="image_01" id="chooseImgFile" onchange="document.getElementById('imgFile_01').src = window.URL.createObjectURL(this.files[0])">
						</div>
					</div>
				</div>
				<div class="col-md-3">
					<div class="form-group">
						<label>Hình 2</label>
						<div>
							<img style="width:100%" id="imgFile_02" class="imgFile" alt="Avatar" src="<?php echo $image_02 ?>" />
							<input type="file" name="image_02" id="chooseImgFile" onchange="document.getElementById('imgFile_02').src = window.URL.createObjectURL(this.files[0])">
						</div>
					</div>
				</div>
				<div class="col-md-3">
					<div class="form-group">
						<label>Hình 3</label>
						<div>
							<img style="width:100%" id="imgFile_03" class="imgFile" alt="Avatar" src="<?php echo $image_03 ?>" />
							<input type="file" name="image_03" id="chooseImgFile" onchange="document.getElementById('imgFile_03').src = window.URL.createObjectURL(this.files[0])">
						</div>
					</div>
				</div>
				<div class="col-md-3">
					<div class="form-group">
						<label>Hình 4</label>
						<div>
							<img style="width:100%" id="imgFile_04" class="imgFile" alt="Avatar" src="<?php echo $image_04 ?>" />
							<input type="file" name="image_04" id="chooseImgFile" onchange="document.getElementById('imgFile_04').src = window.URL.createObjectURL(this.files[0])">
						</div>
					</div>
				</div>
				<div class="col-md-3">
					<div class="form-group">
						<label>Hình 5</label>
						<div>
							<img style="width:100%" id="imgFile_05" class="imgFile" alt="Avatar" src="<?php echo $image_05 ?>" />
							<input type="file" name="image_05" id="chooseImgFile" onchange="document.getElementById('imgFile_05').src = window.URL.createObjectURL(this.files[0])">
						</div>
					</div>
				</div>
				<div class="col-md-3">
					<div class="form-group">
						<label>Hình 6</label>
						<div>
							<img style="width:100%" id="imgFile_06" class="imgFile" alt="Avatar" src="<?php echo $image_06 ?>" />
							<input type="file" name="image_06" id="chooseImgFile" onchange="document.getElementById('imgFile_06').src = window.URL.createObjectURL(this.files[0])">
						</div>
					</div>
				</div>
				<div class="col-md-3">
					<div class="form-group">
						<label>Hình 7</label>
						<div>
							<img style="width:100%" id="imgFile_07" class="imgFile" alt="Avatar" src="<?php echo $image_07 ?>" />
							<input type="file" name="image_07" id="chooseImgFile" onchange="document.getElementById('imgFile_07').src = window.URL.createObjectURL(this.files[0])">
						</div>
					</div>
				</div>
				<div class="col-md-3">
					<div class="form-group">
						<label>Hình 8</label>
						<div>
							<img style="width:100%" id="imgFile_08" class="imgFile" alt="Avatar" src="<?php echo $image_08 ?>" />
							<input type="file" name="image_08" id="chooseImgFile" onchange="document.getElementById('imgFile_08').src = window.URL.createObjectURL(this.files[0])">
						</div>
					</div>
				</div>
				<div class="clearfix"></div>
				<div class="col-md-3">
					<button type="reset" class="btn btn-warning">Huỷ</button>
					<button type="submit" id="formSubmit" class="btn btn-primary">Gửi</button>
				</div>
			</div>

		</div>
		<!-- END: .container-fluid -->

		<?php echo form_close(); ?>


</div>
<!-- END: .app-main -->
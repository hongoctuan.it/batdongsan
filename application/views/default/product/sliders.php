<style>
.carousel-item img {
    object-fit: cover; /* Do not scale the image */
    object-position: center; /* Center the image within the element */
    width: 100% !important;
    max-height: 500px;
    margin-bottom: 1rem;
}
</style>
<div id="myCarousel" class="carousel slide border" data-ride="carousel" style="width:100%">
    <ol class="carousel-indicators">
        <?php if(!empty($product->img1)):?>
            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
        <?php endif;?>
        <?php if(!empty($product->img2)):?>
            <li data-target="#myCarousel" data-slide-to="1"></li>
        <?php endif;?>
        <?php if(!empty($product->img3)):?>
            <li data-target="#myCarousel" data-slide-to="2"></li>
        <?php endif;?>
        <?php if(!empty($product->img4)):?>
            <li data-target="#myCarousel" data-slide-to="3"></li>
        <?php endif;?>
        <?php if(!empty($product->img5)):?>
            <li data-target="#myCarousel" data-slide-to="4"></li>
        <?php endif;?>
        <?php if(!empty($product->img6)):?>
            <li data-target="#myCarousel" data-slide-to="5"></li>
        <?php endif;?>
        <?php if(!empty($product->img7)):?>
            <li data-target="#myCarousel" data-slide-to="6"></li>
        <?php endif;?>
        <?php if(!empty($product->img8)):?>
            <li data-target="#myCarousel" data-slide-to="7"></li>
        <?php endif;?>
   </ol>
   <div class="carousel-inner">
        <?php if(!empty($product->img1)):?>
            <div class="carousel-item active">
                <img class="d-block w-100 img-fluid" src="<?php echo site_url("assets/public/avatar/".$product->img1) ?>" style="width:100%;">
            </div>
        <?php endif;?>
        <?php if(!empty($product->img2)):?>
            <div class="carousel-item">
                <img class="d-block w-100" src="<?php echo site_url("assets/public/avatar/".$product->img2) ?>" style="width:100%;">
            </div>
        <?php endif;?>
        <?php if(!empty($product->img3)):?>
            <div class="carousel-item">
                <img class="d-block w-100" src="<?php echo site_url("assets/public/avatar/".$product->img3) ?>" style="width:100%;">
            </div>
        <?php endif;?>
        <?php if(!empty($product->img4)):?>
            <div class="carousel-item">
                <img class="d-block w-100" src="<?php echo site_url("assets/public/avatar/".$product->img4) ?>" style="width:100%;">
            </div>
        <?php endif;?>
        <?php if(!empty($product->img5)):?>
            <div class="carousel-item">
                <img class="d-block w-100" src="<?php echo site_url("assets/public/avatar/".$product->img5) ?>" style="width:100%;">
            </div>
        <?php endif;?>
        <?php if(!empty($product->img6)):?>
            <div class="carousel-item">
                <img class="d-block w-100" src="<?php echo site_url("assets/public/avatar/".$product->img6) ?>" style="width:100%;">
            </div>
        <?php endif;?>
        <?php if(!empty($product->img7)):?>
            <div class="carousel-item">
                <img class="d-block w-100" src="<?php echo site_url("assets/public/avatar/".$product->img7) ?>" style="width:100%;">
            </div>
        <?php endif;?>
        <?php if(!empty($product->img8)):?>
            <div class="carousel-item">
                <img class="d-block w-100" src="<?php echo site_url("assets/public/avatar/".$product->img8) ?>" style="width:100%;">
            </div>
        <?php endif;?>
    </div>    
   <!-- Controls -->
   <a class="carousel-control-prev" href="#myCarousel" role="button" data-slide="prev">
     <span class="carousel-control-prev-icon" aria-hidden="true"></span>
     <span class="sr-only">Previous</span>
   </a>
   <a class="carousel-control-next" href="#myCarousel" role="button" data-slide="next">
     <span class="carousel-control-next-icon" aria-hidden="true"></span>
     <span class="sr-only">Next</span>
   </a>
</div>
      <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
      <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script>
function myFunction(id,image,price,location,room,tolet,area) {
    document.getElementById("contact_image").innerHTML = 
    '<img src="'+image+'" width="100%" height="80px" style="float:left"/>';
    document.getElementById("productcontact").innerHTML = '<input type="hidden" name="productcontact" class="productcontact" value="'+id+'">';
    document.getElementById("contact_content").innerHTML = 
    '<div style="font-size:16px">đ '+price+'</div><div class="contact-address"><i class="fa fa-map-marker"></i> &nbsp;'+location+'</div><div style="font-size:16px"><i class="fa fa fa-bed"></i> '+room+'  &nbsp;&nbsp;<i class="fa fa-bath"></i>'+tolet+' &nbsp;&nbsp;<i class="fa fa-crosshairs"></i> '+area+' </div>';
    document.getElementById("contact_image").style.display = "block";
    document.getElementById("contact_content").style.display = "block"

}
function search() {
    var keyword = document.getElementById("keyword").value;
    var location = document.getElementById("location").value;
    var properties = document.getElementById("properties").value;
    var minprice = document.getElementById("minprice").value;
    var maxprice = document.getElementById("maxprice").value;
    var beds = document.getElementById("beds").value;
    window.location.href = "<?php echo site_url("danh-muc-san-pham")?>"+"?search=true&keyword="+keyword+"&location="+location+"&properties="+properties+"&minprice="+minprice+"&maxprice="+maxprice+"&beds="+beds;
}
function getval(sel)
{
    window.location.href = sel.value;
}
</script>
<div class='container-fluid content'>
<div class="category-search">
    <div class="form-group row">
        <input <?php echo isset($_GET['keyword'])&&$_GET['keyword']!=""?"value='".$_GET['keyword']."'":""; ?> id="keyword" class="col-md-2" type="text" class="form-control" id="searchname" placeholder="Từ khoá tìm kiếm...">
        <select id="location" style="background-color: transparent;" class="form-control searchtype col-md-1">
            <option style="padding-left:20px" value="all" <?php echo isset($_GET['location'])&&$_GET['location']=="all"?"selected":""; ?>>Địa Điểm</option>
            <?php foreach($locationsearch as $item): ?>
                <option style="padding-left:20px" value="<?php echo $item->province?>" <?php echo isset($_GET['location'])&&$_GET['location']==$item->province?"selected":""; ?>><?php echo $item->province?></option>
            <?php endforeach;?>
        </select>
        <select id="properties" style="background-color: transparent;" class="form-control searchtype col-md-1">
            <option style="padding-left:20px" value="all" <?php echo isset($_GET['properties'])&&$_GET['properties']=="all"?"selected":""; ?>>Trạng Thái</option>
            <option style="padding-left:20px" value="ban.png" <?php echo isset($_GET['properties'])&&$_GET['properties']=="ban.png"?"selected":""; ?>>Bán</option>
            <option style="padding-left:20px" value="chothue.png" <?php echo isset($_GET['properties'])&&$_GET['properties']=="chothue.png"?"selected":""; ?>>Cho Thuê</option>
        </select>
       <select id="minprice" style="background-color: transparent;" class="form-control searchtype col-md-1">
            <option style="padding-left:20px" value="0" <?php echo isset($_GET['minprice'])&&$_GET['minprice']=='0'?"selected":""; ?>>Giá Từ</option>
            <option style="padding-left:20px" value="250000000" <?php echo isset($_GET['minprice'])&&$_GET['minprice']=='250000000'?"selected":""; ?>>250 tr</option>
            <option style="padding-left:20px" value="500000000" <?php echo isset($_GET['minprice'])&&$_GET['minprice']=='500000000'?"selected":""; ?>>500 tr</option>
            <option style="padding-left:20px" value="1000000000" <?php echo isset($_GET['minprice'])&&$_GET['minprice']=='1000000000'?"selected":""; ?>>1 tỷ</option>
            <option style="padding-left:20px" value="1250000000" <?php echo isset($_GET['minprice'])&&$_GET['minprice']=='1250000000'?"selected":""; ?>>1 tỷ 250tr</option>
            <option style="padding-left:20px" value="1500000000" <?php echo isset($_GET['minprice'])&&$_GET['minprice']=='1500000000'?"selected":""; ?>>1 tỷ 500tr</option> 
        </select>
        <select id="maxprice" style="background-color: transparent;" class="form-control searchtype col-md-1">
            <option style="padding-left:20px" value="0" <?php echo isset($_GET['maxprice'])&&$_GET['maxprice']=='0'?"selected":""; ?>>Giá đến</option>
            <option style="padding-left:20px" value="250000000" <?php echo isset($_GET['maxprice'])&&$_GET['maxprice']=='250000000'?"selected":""; ?>>250 tr</option>
            <option style="padding-left:20px" value="500000000" <?php echo isset($_GET['maxprice'])&&$_GET['maxprice']=='500000000'?"selected":""; ?>>500 tr</option>
            <option style="padding-left:20px" value="1000000000" <?php echo isset($_GET['maxprice'])&&$_GET['maxprice']=='1000000000'?"selected":""; ?>>1 tỷ</option>
            <option style="padding-left:20px" value="1250000000" <?php echo isset($_GET['maxprice'])&&$_GET['maxprice']=='1250000000'?"selected":""; ?>>1 tỷ 250tr</option>
            <option style="padding-left:20px" value="1500000000" <?php echo isset($_GET['maxprice'])&&$_GET['maxprice']=='1500000000'?"selected":""; ?>>1 tỷ 500tr</option> 
        </select>
        <select id="beds" style="background-color: transparent;" class="form-control searchtype col-md-1">
            <option style="padding-left:20px" value="0" <?php echo isset($_GET['beds'])&&$_GET['beds']=='0'?"selected":""; ?>>Phòng Ngủ</option>
            <option style="padding-left:20px" value="1" <?php echo isset($_GET['beds'])&&$_GET['beds']=='1'?"selected":""; ?>>01</option>
            <option style="padding-left:20px" value="2" <?php echo isset($_GET['beds'])&&$_GET['beds']=='2'?"selected":""; ?>>02</option>
            <option style="padding-left:20px" value="3" <?php echo isset($_GET['beds'])&&$_GET['beds']=='3'?"selected":""; ?>>03</option>
            <option style="padding-left:20px" value="4" <?php echo isset($_GET['beds'])&&$_GET['beds']=='4'?"selected":""; ?>>04</option>
            <option style="padding-left:20px" value="5" <?php echo isset($_GET['beds'])&&$_GET['beds']=='5'?"selected":""; ?>>05</option>
        </select>
        <button type="button" onclick="search()" class="searchtype btn btn-default col-md-1" style="background-color:#fc0; width:20%">Tìm Kiếm</button>
    </div>
<div>

<div class="row">
<div class="col-md-9">
        <div  class="row" style="padding-bottom:20px">
            <?php if(isset($partner)):?>
                <div class="col-md-6">
                    <img src="<?php echo !empty($partner->img1)?site_url("assets/public/avatar/".$partner->img1):site_url("assets/logo.png")?>" width="100%" />
                </div>
                <div class="col-md-6">
                    <div style="font-weight:bold; font-size:24px"><?php echo $partner->name?></div>
                    <div style="font-style: italic;"><?php echo $partner->constructor?></div>
                    <div><i class="fa fa-building">&nbsp;</i><?php echo $partner->activelisting?></div>
                    <?php if(!empty($partner->rentbedroom01)||!empty($partner->rentbedroom02)||!empty($partner->rentbedroom03)||!empty($partner->rentbedroom04)||!empty($partner->rentbedroom05)||!empty($partner->rentpenthouse)):?>
                    <div><span style="font-style: italic;font-weight:bold;">Giá thuê:</span>
                    <?php if($partner->rentbedroom01>0):?><div>1 phòng: <?php echo number_format ($partner->rentbedroom01, 2, '.', ',')?> VNĐ</div><?php endif;?>
                    <?php if($partner->rentbedroom02>0):?><div>2 phòng: <?php echo number_format ($partner->rentbedroom02, 2, '.', ',')?> VNĐ</div><?php endif;?>
                    <?php if($partner->rentbedroom03>0):?><div>3 phòng: <?php echo number_format ($partner->rentbedroom03, 2, '.', ',')?> VNĐ</div><?php endif;?>
                    <?php if($partner->rentbedroom04>0):?><div>4 phòng: <?php echo number_format ($partner->rentbedroom04, 2, '.', ',')?> VNĐ</div><?php endif;?>
                    <?php if($partner->rentbedroom05>0):?><div>5 phòng: <?php echo number_format ($partner->rentbedroom05, 2, '.', ',')?> VNĐ</div><?php endif;?>
                    <?php if($partner->rentpenthouse>0):?><div>Penthouse/Duplex: <?php echo number_format ($partner->rentpenthouse, 2, '.', ',')?> VNĐ</div><?php endif;?>
                    </div>
                    <?php endif?>
                    <?php if(!empty($partner->salebedroom01)||!empty($partner->salebedroom02)||!empty($partner->salebedroom03)||!empty($partner->salebedroom04)||!empty($partner->salebedroom05)||!empty($partner->salepenthouse)):?>
                    <div><span style="font-style: italic;font-weight:bold;">Giá bán:</span>
                    <?php if($partner->rentbedroom01>0):?><div>1 phòng: <?php echo number_format ($partner->rentbedroom01, 2, '.', ',')?> VNĐ</div><?php endif;?>
                    <?php if($partner->rentbedroom02>0):?><div>2 phòng: <?php echo number_format ($partner->rentbedroom02, 2, '.', ',')?> VNĐ</div><?php endif;?>
                    <?php if($partner->rentbedroom03>0):?><div>3 phòng: <?php echo number_format ($partner->rentbedroom03, 2, '.', ',')?> VNĐ</div><?php endif;?>
                    <?php if($partner->rentbedroom04>0):?><div>4 phòng: <?php echo number_format ($partner->rentbedroom04, 2, '.', ',')?> VNĐ</div><?php endif;?>
                    <?php if($partner->rentbedroom05>0):?><div>5 phòng: <?php echo number_format ($partner->rentbedroom05, 2, '.', ',')?> VNĐ</div><?php endif;?>
                    <?php if($partner->rentpenthouse>0):?><div>Penthouse/Duplex: <?php echo number_format ($partner->rentpenthouse, 2, '.', ',')?> VNĐ</div><?php endif;?>
                    </div>
                    <?php endif;?>
                    <div><?php echo substr($partner->description, 0, 200)?>...</div>
                </div>
            <?php endif;?>
        </div>
        <div class="row">
            <?php if(isset($partner)):?>
                <div class="col-md-9 content-title">Dự án <?php echo $partner->name?></div>
            <?php else:?>
                <div class="col-md-9 content-title">Sản phẩm</div>
            <?php endif;?>

            <div class="dropdown col-md-3" style="text-align: right;" >
            <?php
                if(!empty($_SERVER['QUERY_STRING'])){
                    $urlsortnotsearch=explode("?sort",current_url().'?'.$_SERVER['QUERY_STRING']);
                    if(empty($urlsortnotsearch[1])){
                        $urlsort = explode("&sort",current_url().'?'.$_SERVER['QUERY_STRING']);
                        $urlsort[0]= $urlsort[0].'&';
                    }
                    else{
                        $urlsortnotsearch[0] = $urlsortnotsearch[0].'?';
                        $urlsort = $urlsortnotsearch;
                    }
                }
                else
                    $urlsort = explode("sort",current_url().'?'.$_SERVER['QUERY_STRING'])
            ?>
                <select style="background-color: transparent;" class="form-control" onchange="getval(this);">
                    <option value="<?php echo !empty($_SERVER['QUERY_STRING'])?$urlsort[0]."sort=newest":$urlsort[0].'sort=newest';?>">Mới nhất</option>
                    <option value="<?php echo !empty($_SERVER['QUERY_STRING'])?$urlsort[0]."sort=low":$urlsort[0].'sort=low';?>">Giá: Thấp đến Cao</option>
                    <option value="<?php echo !empty($_SERVER['QUERY_STRING'])?$urlsort[0]."sort=hight":$urlsort[0].'sort=hight';?>">Giá: Cao đến Thấp</option>
                </select>
            </div>
        </div>
    </div>
    <div class="col-md-9">
        <?php if(!empty($products)):?>
            <?php foreach($products as $item): ?>
                <div class="row categoryitem">            
                        <div class="col-md-4" style="padding:8px">      
                        <a href="<?php echo site_url("san-pham/".$item->slug)?>">      
                            <img class="image-item" src="<?php echo !empty($item->img1)?site_url("assets/public/avatar/".$item->img1):site_url("assets/logo.png")?>" width="270" height="180" />
                            <?php 
                                if($item->hot!=0)
                                    echo '<div style="background: #fc0;
                                    width: fit-content;
                                    padding: 5px;
                                    position: absolute;
                                    top: 8px;
                                    border-bottom-right-radius: 5px;
                                    color: black;
                                    font-weight: bold;">Nổi Bật</div>';
                            ?>
                        </a>
                        </div>
                        <div class="dropdown col-md-6" style="padding:0px">
                        <a href="<?php echo site_url("san-pham/".$item->slug)?>">      
                            <div class="categoryprice">$<?php echo number_format($item->price,0,",",".")?></div>
                            <div class="categoryname"><?php echo $item->name ?></div>
                            <div class="categorylocation"><i class="fa fa-map-marker"></i> &nbsp;<?php echo !empty($item->partner_id !=1)?$item->partner_id:$item->address; ?></div>
                            <div class="categorylocation"><i class="fa fa fa-bed"></i> <?php echo $item->room ?>  &nbsp;&nbsp;<i class="fa fa-bath"></i><?php echo $item->tolet ?> &nbsp;&nbsp;<i class="fa fa-crosshairs"></i> <?php echo $item->area ?> </div>
                            <div class="categorydescription"><?php echo $item->short_des ?>...</div>                                                 
                        </a>
                        </div>
                    
                    <div class="col-md-2" style="padding:0px">
                        <img class="image-logo" src="<?php echo site_url("assets/public/avatar/".$item->partner_logo) ?>" width="50%" style="float:right"/>
                        <button onclick="myFunction('<?php echo $item->slug?>','<?php echo site_url('assets/public/avatar/'.$item->img1)?>','<?php echo number_format($item->price,0,',','.') ?>','<?php echo $item->location_province ?> - <?php echo $item->location_district ?>','<?php echo $item->room ?>','<?php echo $item->tolet ?>','<?php echo $item->area ?>')" type="button" class="btn btn-default contact_button">Liên hệ</button>
                    </div>
                </div>
            <?php endforeach;?>
        <?php endif;?>
    </div>
    <div class="col-md-3 contact">
        <div class="row">
            <div class="col-md-4" id="contact_image" style="padding:0px !important; display:none">
            </div>
            <div class="col-md-8" id="contact_content" style="display:none;">
                <div style="font-size:16px">đ <?php echo $item->price?></div>
                <div class="contact-address"><i class="fa fa-map-marker"></i> &nbsp;<?php echo $item->location_province ?> - <?php echo $item->location_district ?></div>
                <div style="font-size:16px"><i class="fa fa fa-bed"></i> <?php echo $item->room ?>  &nbsp;&nbsp;<i class="fa fa-bath"></i><?php echo $item->tolet ?> &nbsp;&nbsp;<i class="fa fa-crosshairs"></i> <?php echo $item->area ?> </div>
            </div>
            <div class="col-md-12">
                <div id="productcontact" style="margin-bottom:20px"></div>
                <a type="button" class="btn btn-info" href="tel:<?php echo $phone?>" style="width:100%; margin-bottom:10px"><i class="fa fa-phone-square" aria-hidden="true" style="font-size:25px; margin-right:10px;top:2px; position:relative"></i><?php echo $phone?></a>
                <a type="button" class="btn btn-info" href="viber://chat?number=<?php echo $viber?>" style="width:100%; margin-bottom:10px; background-color:#665cac">Viber</a>
                <a type="button" class="btn btn-info" href="https://api.whatsapp.com/send?phone=<?php echo $whatsapp?>" style="width:100%; margin-bottom:10px; background-color:#01e675">Whatsapp</a>
                <a type="button" class="btn btn-info" href="https://chat.zalo.me/?phone=<?php echo $zalo?>" style="width:100%; margin-bottom:10px; background-color:#0cb3ff"><img src="<?php echo site_url('assets/zalo.png')?>" width="25px" style="margin-right:10px"/>Liên hệ với zalo</a>
                <a type="button" class="btn btn-info" href="<?php echo $facebook?>" style="width:100%; margin-bottom:10px; background-color:#2d88ff"><img src="<?php echo site_url('assets/facebook.png')?>" width="25px" style="margin-right:10px"/>Liên hệ với Facebook</a>

                <input type="text" name="representator" required class="partner-representator form-control contactinput" id="usr" placeholder="Nhập họ tên">
                <input type="email" class="partner-email form-control contactinput" name="email" aria-describedby="emailHelp" placeholder="Nhập email">
                <input type="text" class="partner-phone form-control contactinput" name="phone" placeholder="Nhập số điện thoại">
                <input type="text" class="partner-request form-control contactinput" name="request" placeholder="Nhập yêu cầu">
                <button type="button" class="partner-registry-btn btn btn-default contactinput" style="background-color:#fc0; width:100% !important; margin-bottom:20px"><i class="fa fa-envelope" aria-hidden="true"></i> Gửi liên hệ</button>
                <div id="error-message"></div>
            </div>
        </div>
    </div>
</div>

</div>
</div>
</div>
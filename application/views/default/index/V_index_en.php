<style>
.parallax {
  /* The image used */
  background-image: url("https://zuhaus.qodeinteractive.com/wp-content/uploads/2017/10/h1-title-img.jpg");

  /* Set a specific height */
  min-height: 700px; 

  /* Create the parallax scrolling effect */
  background-attachment: fixed;
  background-position: center;
  background-repeat: no-repeat;
  background-size: cover;
}
.activea p{
  color: #cb8204;
}
</style>
<script>
    function search() {
        var cate = $( "li.activea" ).find('p.category-name').text()
        var properties = document.getElementById("properties").value;
        var location = document.getElementById("location").value;
        if(cate!="")
            window.location.href = "<?php echo site_url("category")?>"+"?properties="+properties+"&location="+location+"&cate="+cate;
        else
            window.location.href = "<?php echo site_url("category")?>"+"?properties="+properties+"&location="+location;
    }
</script>
<div class="parallax">
</div>
<section class="home-fillter pt-4 mt-4">
<div class="container">
        <h1 class="text-center m-auto px-3" style="font-family:Josefin Sans,sans-serif; color:white; font-size:55px">Find the Best Places to Be</h1>
    <div>
    <ul class="category" id="category">  
        <li class="btn activea" style="display:none"></li>
        <?php foreach($categories as $item): ?>
            <li class="category-item">
                <p><img src="<?php echo site_url("assets/public/avatar/".$item->img) ?>" alt="Taxonomy Custom Icon"></p>
                <p class="category-name"><?php echo $item->name_en?></p>
            </li>
        <?php endforeach;?>
    </ul>
        <div class="form-group category-search-item" >
            <select id="location" style="background-color: transparent; color:white" class="form-control" id="exampleFormControlSelect1">
                <option style="padding-left:20px; color:black" value="all">All locations</option>
                <?php foreach($locationsearch as $item): ?>
                    <option style="padding-left:20px; color:black" value="<?php echo $item->province?>"><?php echo $item->province_en?></option>
                <?php endforeach;?>
            </select>
        </div>
        <div class="form-group category-search-item" >
            <select id="properties" style="background-color: transparent; color:white; margin-left:5px" class="form-control" id="exampleFormControlSelect1">
                <option style="padding-left:20px; color:black" value="all">All Properties</option>
                <option style="padding-left:20px; color:black" value="ban.png" >Sale</option>
                <option style="padding-left:20px; color:black" value="chothue.png" >Leasing</option>
            </select>
        </div>
        <button type="button" class="btn btn-default category-search-button" onclick="search()" >Search</button>
        

    </div>
</div>
</section>

<section class="home-featured-products pt-4 mt-4">
<div class="container">
    <div>
        <?php echo $com_info[11]->value ?>
    </div>
</div>
<div class="container">
    <?php if(isset($hotproducts_01)): ?>
    <div class="hotproduct_01">
        <a href="<?php echo site_url("product/".$hotproducts_01->slug)?>">
            <img class="pro-img" style="padding:4px" src="<?php echo site_url("assets/public/avatar/".$hotproducts_01->img1) ?>" width="100%" height="100%"/>
            <img class="comming" style="padding:4px" src="<?php echo site_url("assets/public/avatar/".$hotproducts_01->status_product) ?>" width="100%" height="100%"/>
            <div class="product">
                <p class="product_title"><?php echo $hotproducts_01->address ?></p>
                <p class="product_content"><?php echo $hotproducts_01->location_district ?> - <?php echo $hotproducts_01->location_province ?></p>
                <div class="product_price_hot1">$<?php echo number_format($hotproducts_01->price,0,",","."); ?></div>
            </div>
        </a>
    </div>
    <?php endif; ?>
    <div  class="hotproduct_02">
    <a href="<?php echo site_url("product/".$hotproducts_02->slug)?>">
        <img class="pro-img" style="padding:4px" src="<?php echo site_url("assets/public/avatar/".$hotproducts_02->img1) ?>" width="100%" height="100%"/>
        <img class="sale" style="padding:4px" src="<?php echo site_url("assets/public/avatar/".$hotproducts_02->status_product) ?>" width="100%" height="100%"/>
        <div class="product">
            <p class="product_title"><?php echo $hotproducts_02->address ?></p>
            <p class="product_content"><?php echo $hotproducts_02->location_district ?> - <?php echo $hotproducts_02->location_province ?></p>
            <div class="product_price_hot2">$<?php echo number_format($hotproducts_02->price,0,",",".") ?></div>
        </div>
    </a>
    </div>
    <div class="hotproduct_03">
    <a href="<?php echo site_url("product/".$hotproducts_03->slug)?>">
        <img class="pro-img" style="padding:4px" src="<?php echo site_url("assets/public/avatar/".$hotproducts_03->img1) ?>" width="100%" height="100%"/>
        <img class="saled" style="padding:4px" src="<?php echo site_url("assets/public/avatar/".$hotproducts_03->status_product) ?>" width="100%" height="100%"/>
        <div class="product">
            <p class="product_title"><?php echo $hotproducts_03->address ?></p>
            <p class="product_content"><?php echo $hotproducts_03->location_district ?> - <?php echo $hotproducts_03->location_province ?></p>
            <div class="product_price_hot3">$<?php echo number_format($hotproducts_03->price,0,",",".") ?></div>
        </div>
    </a>
    </div>
    <div class="hotproduct_04">
    <a href="<?php echo site_url("product/".$hotproducts_04->slug)?>">
        <img class="pro-img" style="padding:4px" src="<?php echo site_url("assets/public/avatar/".$hotproducts_04->img1) ?>" width="100%" height="100%"/>
        <img class="new" style="padding:4px" src="<?php echo site_url("assets/public/avatar/".$hotproducts_04->status_product) ?>" width="100%" height="100%"/>
        <div class="product">
            <p class="product_title"><?php echo $hotproducts_04->address ?></p>
            <p class="product_content"><?php echo $hotproducts_04->location_district ?> - <?php echo $hotproducts_04->location_province ?></p>
            <div class="product_price_hot4">$<?php echo number_format($hotproducts_04->price,0,",",".") ?></div>
        </div>
    </a>
    </div>
    <div class="hotproduct_05">
    <a href="<?php echo site_url("product/".$hotproducts_05->slug)?>">
        <img class="pro-img" style="padding:4px" src="<?php echo site_url("assets/public/avatar/".$hotproducts_05->img1) ?>" width="100%" height="100%"/>
        <img class="rent" style="padding:4px" src="<?php echo site_url("assets/public/avatar/".$hotproducts_05->status_product) ?>" width="100%" height="100%"/>
        <div class="product">
            <p class="product_title"><?php echo $hotproducts_05->address ?></p>
            <p class="product_content"><?php echo $hotproducts_05->location_district ?> - <?php echo $hotproducts_05->location_province ?></p>
            <div class="product_price_hot5">$<?php echo number_format($hotproducts_05->price,0,",",".") ?></div>
        </div>
    </a>
    </div>
</div>

</section>
<div style="clear:both"></div>
<section class="home-featured-products pt-4 mt-4">
    <div class="container">
        <div>
            <?php echo $com_info[12]->value ?>
        </div>
    </div>
    <div class="container p-5">
        <div class="owl-carousel featured-products-slide">
            <?php foreach($location as $item): ?>
                <div class="justify-content-center">
                    <figure class="figure featured-products-item-figure">
                        <a href="<?php echo site_url("location/").$item->slug?>" class="featured-products-link">
                            <div class="featured-products-item-wrap" style="background-color:black">
                                <div class="featured-products-img-wrap">
                                <img src="<?php echo site_url("assets/public/avatar/".$item->img) ?>" alt="Taxonomy Featured Image">                                    </div>
                                <div class="locationproduct"><?php echo $item->count?></div>
                                <div class="locationstatus">Real Estate</div>
                                <div class="locationprovince"><?php echo $item->province_en?></div>
                                <div class="locationdistrict"><?php echo $item->district_en?></div>
                            </div>
                        </a>
                    </figure>
                </div>
            <?php endforeach;?>
        </div>
    </div>


</section>
<script>
// Add active class to the current button (highlight it)
var header = document.getElementById("category");
var btns = header.getElementsByClassName("category-item");
for (var i = 0; i < btns.length; i++) {
  btns[i].addEventListener("click", function() {
  var current = document.getElementsByClassName("activea");
  current[0].className = current[0].className.replace(" activea", "");
  this.className += " activea";
  });
}
</script>
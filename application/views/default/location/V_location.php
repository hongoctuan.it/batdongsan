<style>
    /* body { min-height: 100vh; font-family: 'Roboto'; background-color:#fafafa} */
    .activea p{
  color: #cb8204;
}
</style>
    
    
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script>
    $(document).ready(function(){
        $("#btnFilter").click(function(){
            var properties = $("#properties"). val();
            var cate = $( "li.activea" ).find('p.category-name').text()
            var temp="<?php echo site_url('filter/')?>";
            var bedrooms = $("#bedrooms"). val();
            var bathrooms = $("#bathrooms"). val();
            var min_price = $("#min_price"). val();
            var max_price = $("#max_price"). val();
            var location = "<?php echo $this->uri->segment(2);?>";
            var _url="";
            if(cate=='')
                _url=temp+properties+"_all"+"_"+bedrooms+"_"+bathrooms+"_"+min_price+"_"+max_price+"_"+location;
            else
                _url=temp+properties+"_"+cate+"_"+bedrooms+"_"+bathrooms+"_"+min_price+"_"+max_price+"_"+location;
            $.ajax({url: _url, success: function(result){
                $("#locationfilter").html(result);
            }});
        });
    });
</script>
<script src="http://maps.google.com/maps/api/js?sensor=true" type="text/javascript"></script>
<div class="container content">
    <div class="row">
        <div class="col-md-8 col-sm-12">
            <div class="container">
                <div class="row">
                    <div class="form-group col-md-3 verticalline">
                        <select id="properties" class="form-control">
                            <option style="padding-left:20px" value="all" >Trạng Thái</option>
                            <option style="padding-left:20px" value="ban.png" >Bán</option>
                            <option style="padding-left:20px" value="daban.png" >Đã Bán</option>
                            <option style="padding-left:20px" value="chothue.png" >Cho Thuê</option>
                            <option style="padding-left:20px" value="dathue.png" >Đã Thuê</option>
                            <option style="padding-left:20px" value="moidang.png" >Mới Đăng</option>
                            <option style="padding-left:20px" value="sapra.png" >Sắp Ra</option>
                        </select>
                    </div>
                    <div class="col-md-9">
                        <ul class="category" id="category">  
                            <li class="btn activea"></li>
                            <?php foreach($categories as $item): ?>
                                <li class="category-item">
                                    <p class="category-image"><img src="<?php echo site_url("assets/public/avatar/".$item->img) ?>" alt="Taxonomy Custom Icon"></p>
                                    <p class="category-name"><?php echo $item->name?></p>
                                </li>
                            <?php endforeach;?>
                        </ul>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2">
                        <div>Phòng tắm</div>
                        <div class="number-input">
                            <button onclick="this.parentNode.querySelector('input[type=number]').stepDown()" class="minus btn-default"><i class="fa fa-minus" aria-hidden="true"></i></button>
                            <input class="quantity" min="0" name="quantity" value="1" id="bathrooms" type="number">
                            <button onclick="this.parentNode.querySelector('input[type=number]').stepUp()" class="plus btn-default"><i class="fa fa-plus" aria-hidden="true"></i></button>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div>Phòng ngủ</div>
                        <div class="number-input">
                            <button onclick="this.parentNode.querySelector('input[type=number]').stepDown()" class="minus btn-default"><i class="fa fa-minus" aria-hidden="true"></i></button>
                            <input class="quantity" min="0" name="quantity" value="1" id="bedrooms" type="number">
                            <button onclick="this.parentNode.querySelector('input[type=number]').stepUp()" class="plus btn-default"><i class="fa fa-plus" aria-hidden="true"></i></button>
                        </div>
                    </div>
                    <div class="col-md-8">
                            <span>Price range: From</span>
                            <input type="text" min=250000000 max="3000000000" oninput="validity.valid||(value='250000000');" id="min_price" class="price-range-field" />
                            <span>To</span>
                            <input type="text" min=250000000 max="3000000000" oninput="validity.valid||(value='3000000000');" id="max_price" class="price-range-field" />
                        <div class="price-range-block">
                            <div id="slider-range" class="price-filter-range" name="rangeInput"></div>   
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <button type="button" id="btnFilter" class="btn btn-warning" style="width:80%; margin-top:20px">Filter</button>
                    </div>  
                </div>
                <div class="row" id="locationfilter">
                        <?php foreach($products as $item):?>
                            <a class="col-md-4 col-sm-12" style="color:black" href="<?php echo site_url("san-pham/".$item->slug)?>">
                                <div class="related-item" style="background-image:url('<?php echo site_url("assets/public/avatar/".$item->img1) ?>'); height:240px; background-position:center; padding:0px">
                                    <div class="related-content">
                                        <p class="related-content-title"><?php echo $item->productname;?> <?php echo $item->category_name;?> For Rent & Sale</p>
                                        <p class="related-content-address"><?php echo $item->address;?></p>
                                        <p class="related-content-info"><i class="fa fa-bed"></i> <?php echo $item->room;?> <i class="fa fa-bath"></i> <?php echo $item->tolet;?> <i class="fa fa-map"> <?php echo $item->area;?></i></p>
                                    </div>
                                </div>
                            </a>
                        <?php endforeach;?>
                </div>
            </div>
        </div>
        <div class="col-md-4 col-sm-12">
            <div id="map"></div>
        </div>
    </div>
<div>

<!-- close footer -->
</div>
</div>
<script type="text/javascript">
    var locations = [
        <?php foreach($location as $item):?>
            <?php echo "['".$item->name."', ".$item->lating.", ".$item->longing.", ".$item->id."],";?>
        <?php endforeach;?>
    ];

    var map = new google.maps.Map(document.getElementById('map'), {
      zoom: 10,
      center: new google.maps.LatLng(10.7, 106.7),
      mapTypeId: google.maps.MapTypeId.ROADMAP
    });

    var infowindow = new google.maps.InfoWindow();

    var marker, i;

    for (i = 0; i < locations.length; i++) { 
      marker = new google.maps.Marker({
        position: new google.maps.LatLng(locations[i][1], locations[i][2]),
        map: map
      });

      google.maps.event.addListener(marker, 'click', (function(marker, i) {
        return function() {
          infowindow.setContent(locations[i][0]);
          infowindow.open(map, marker);
        }
      })(marker, i));
    }
  </script>
  <script>
// Add active class to the current button (highlight it)
var header = document.getElementById("category");
var btns = header.getElementsByClassName("category-item");
for (var i = 0; i < btns.length; i++) {
  btns[i].addEventListener("click", function() {
  var current = document.getElementsByClassName("activea");
  current[0].className = current[0].className.replace(" activea", "");
  this.className += " activea";
  });
}
</script>
<!DOCTYPE html>
<html lang="en">

<head>
    <link rel="shortcut icon" type="ico" href="assets/olivia.ico" />
    <meta http-equiv=”content-language” content=”vi” />
    <meta http-equiv=”Content-Type” content=”text/html; charset=utf-8″ />
    <link href=”favicon.ico” rel=”shortcut icon” type=”image/x-icon” />
    <meta name='revisit-after' content='1 days' />
    <meta name=”robots” content=”all” />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <?php if ($this->uri->segment(1) == "san-pham") : ?>
    <title><?php echo isset($product) ? $product->name : ""; ?></title>
    <meta name=”description” content="<?php isset($product) ? $product->short_des : "" ?>" />
    <meta property="og:url" content="<?php echo site_url() . $this->uri->uri_string(); ?>" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="<?php echo isset($product->name) ? $product->name : ""; ?>" />
    <meta property="og:description" content="<?php echo isset($product) ? $product->short_des : ""; ?>" />
    <meta property="og:image" content="<?php echo isset($product) ? base_url('assets/public/avatar/') . $product->img1 : ""; ?>" />
    <meta name="image" content="<?php echo isset($product) ? base_url('assets/public/avatar/') . $product->img1 : ""; ?>" />
    <meta property="og:image:width" content="700">
    <meta property="og:image:height" content="630">
    <?php else: ?>
    <title><?php echo isset($seo) ? $seo['title'] : ""; ?></title>
    <meta name="description" content="<?php echo isset($seo) ? $seo['meta'] : "" ?>" />
    <meta property="og:url" content="<?php echo site_url() . $this->uri->uri_string(); ?>" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="<?php echo isset($seo) ? $seo['title'] : ""; ?>" />
    <meta property="og:description" content="<?php echo isset($seo) ? $seo['meta'] : ""; ?>" />
    <?php endif; ?>
    
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>

    <link rel="stylesheet" href="<?php echo base_url();?>statics/default/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url();?>statics/default/fontawesome-free/css/all.css">
    <?php
    switch ($this->uri->segment(1)) {
        case 'danh-muc-san-pham':
            echo '<link rel="stylesheet" href="' . site_url("statics/default/css/category.css") . '">';
            break;
        case 'san-pham':
            echo '<link rel="stylesheet" href="' . site_url("statics/default/css/product.css") . '">';
            echo '<link rel="stylesheet" href="' . site_url("statics/default/css/partner.css") . '">';
            break;
        case 'dang-san-pham':
                echo '<script src="'.base_url("filemanager").'/ckeditor/ckeditor.js"></script>';
                echo '<link rel="stylesheet" href="' . site_url("statics/default/css/product.css") . '">';
                break;
        case 'tin-tuc':
                echo '<link rel="stylesheet" href="' . site_url("statics/default/css/news.css") . '">';
                echo '<link rel="stylesheet" href="' . site_url("statics/default/css/news_detail.css") . '">';

                break;
        case 've-chung-toi':
            echo '<link rel="stylesheet" href="' . site_url("statics/default/css/about.css") . '">';
            break;
        case 'location':
            echo '<link rel="stylesheet" href="' . site_url("statics/default/css/location.css") . '">';
            break;
        default:
            echo '<link rel="stylesheet" href="' . site_url("statics/default/css/index.css") . '">';
            echo '<link rel="stylesheet" href="' . site_url("statics/default/css/category.css") . '">';
            // echo '<link rel="stylesheet" href="' . site_url("statics/default/css/product.css") . '">';
            echo '<link rel="stylesheet" href="' . site_url("statics/default/fancybox-master/dist/jquery.fancybox.min.css") . '">';
    }
    ?>
    <link rel="stylesheet" href="<?php echo site_url('statics/default/css/header.css'); ?>">
    <link rel="stylesheet" href="<?php echo site_url('statics/default/swiper/dist/css/swiper.min.css'); ?>">
    <link rel="stylesheet" href="<?php echo site_url('statics/default/perfect-scrollbar/css/perfect-scrollbar.css'); ?>">
    <link rel="stylesheet" href="<?php echo site_url('statics/default/owl/dist/assets/owl.carousel.min.css'); ?>">
    <link rel="stylesheet" href="<?php echo site_url('statics/default/owl/dist/assets/owl.theme.default.min.css'); ?>">
    <link href="https://fonts.googleapis.com/css2?family=Josefin+Sans:wght@700&display=swap" rel="stylesheet">
    <style>
        body {
            font-family: 'Roboto', sans-serif;
        }
    </style>

</head>
<script>
/* When the user clicks on the button, 
toggle between hiding and showing the dropdown content */
function catedropdown() {
  document.getElementById("myDropdown").classList.toggle("show");
  
  if($("#myDropdown").hasClass("dropdown-content-show"))
    $("div").removeClass("dropdown-content-show");
  else
    $(".dropdown-content").addClass("dropdown-content-show");
}
function catedropdownmobile() {
//   document.getElementById("myDropdownmobile").classList.toggle("show");
  
  if($("#myDropdownmobile").hasClass("dropdown-content-show"))
    $("div").removeClass("dropdown-content-show");
  else
    $(".dropdown-content-mobile").addClass("dropdown-content-show");
}


// Close the dropdown if the user clicks outside of it
window.onclick = function(event) {
  if (!event.target.matches('.dropbtn')) {
    var dropdowns = document.getElementsByClassName("dropdown-content");
    var i;
    for (i = 0; i < dropdowns.length; i++) {
      var openDropdown = dropdowns[i];
      if (openDropdown.classList.contains('show')) {
        openDropdown.classList.remove('show');
      }
    }
  }
}
</script>
<body>
    <header>
        <nav class="navbar-expand-lg navbar-dark navbar-lg" id="navbartop">
            <div class="container menu-bar">
                <ul class="navbar-nav navbar-nav-lg">
                    <li id="logo">
                        <img itemprop="image" class="mkdf-light-logo" src="<?php echo site_url("assets/logo.png") ?>" width="100%" alt="light logo">
                    </li>
                    <li style="margin-left:100px" class="nav-item <?php echo $this->uri->segment(1) == '' ? "active" : ""; ?>">
                        <a class="nav-link mx-2 text-white menu-text"  href="<?php echo site_url(); ?>">TRANG CHỦ
                        </a>
                    </li>
                    <li class="nav-item <?php echo $this->uri->segment(1) == 'danh-muc-san-pham' || $this->uri->segment(1) == 'san-pham' ? "active" : ""; ?>">
                        <div class="dropdown">
                            <a class="nav-link mx-2 text-white menu-text dropbtn"  onclick="catedropdown()">DỰ ÁN</a>
                            <div id="myDropdown" class="dropdown-content">
                                <?php if(!empty($project)): ?>
                                    <?php foreach($project as $item):?>
                                        <a href="<?php echo site_url('danh-muc-san-pham?project='.$item->slug)?>"><?php echo $item->name?></a>
                                    <?php endforeach;?>
                                <?php endif?>
                            </div>
                        </div>
                    </li>
                    <li class="nav-item <?php echo $this->uri->segment(1) == 'tin-tuc' ? "active" : ""; ?>">
                        <a class="nav-link mx-2 text-white menu-text"  href="<?php echo site_url(); ?>tin-tuc">BLOGS</a>
                    </li>
                    <li class="nav-item <?php echo $this->uri->segment(1) == 've-chung-toi' ? "active" : ""; ?>">
                        <a class="nav-link mx-2 text-white menu-text"  href="<?php echo site_url(); ?>ve-chung-toi">GIỚI THIỆU</a>
                    </li>
                    <li class="nav-item vi">
                        <a class="nav-link mx-2 text-white menu-text language" style="background-color:#fc0"  href="<?php echo site_url(); ?>">VI</a>
                    </li>
                    <li class="nav-item en">
                        <a class="nav-link mx-2 text-white menu-text language"  href="<?php echo site_url('en'); ?>">EN</a>
                    </li>
                    <li class="nav-item postproduct">
                        <a class="nav-link mx-2 text-white menu-text language"  href="<?php echo site_url('dang-san-pham'); ?>">Đăng Bài</a>
                    </li>
                </ul>
            </div>
        </nav>
        <nav class="navbar navbar-expand-lg navbar-dark navbar-mobile">
            <div class="container">
                <div id="logo">
                    <img itemprop="image" class="mkdf-light-logo" src="<?php echo site_url("assets/logo.png") ?>" width="100%" alt="light logo">
                </div>                
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation" style="position: fixed;right: 5px;background-color: #fc0;top: 5px;">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarSupportedContent" style="margin-left:100px;margin-right:50px">
                    <ul class="navbar-nav" style="background:gray">
                        <li class="nav-item <?php echo $this->uri->segment(1) == '' ? "active" : ""; ?>">
                            <a class="nav-link mx-2 text-white menu-text"  href="<?php echo site_url(); ?>">TRANG CHỦ</a>
                        </li>
                        <li class="nav-item <?php echo $this->uri->segment(1) == 'danh-muc-san-pham' || $this->uri->segment(1) == 'san-pham' ? "active" : ""; ?>">
                        <div class="dropdown">
                            <a class="nav-link mx-2 text-white menu-text dropbtn"  onclick="catedropdownmobile()">TÒA NHÀ</a>
                            <div id="myDropdownmobile" class="dropdown-content-mobile">
                                <?php if(!empty($project)): ?>
                                    <?php foreach($project as $item):?>
                                        <a href="<?php echo site_url('danh-muc-san-pham?project='.$item->slug)?>"><?php echo $item->name?></a>
                                    <?php endforeach;?>
                                <?php endif?>
                            </div>
                        </div>
                    </li>
                        <li class="nav-item <?php echo $this->uri->segment(1) == 'blogs' ? "active" : ""; ?>">
                            <a class="nav-link mx-2 text-white menu-text"  href="<?php echo site_url(); ?>blogs">BLOGS</a>
                        </li>
                        <li class="nav-item <?php echo $this->uri->segment(1) == 've-chung-toi' ? "active" : ""; ?>">
                            <a class="nav-link mx-2 text-white menu-text"  href="<?php echo site_url(); ?>ve-chung-toi">GIỚI THIỆU</a>
                        </li>
                        <li class="nav-item vi">
                        <a class="nav-link mx-2 text-white menu-text language"   href="<?php echo site_url(); ?>">VI</a>
                    </li>
                    <li class="nav-item en">
                        <a class="nav-link mx-2 text-white menu-text language"  href="<?php echo site_url('en'); ?>">EN</a>
                    </li>
                    <li class="nav-item postproduct">
                        <a class="nav-link mx-2 text-white menu-text language"  href="<?php echo site_url('dang-san-pham'); ?>">ĐĂNG BÀI</a>
                    </li>
                    </ul>
                </div>
            </div>
        </nav>
    </header>
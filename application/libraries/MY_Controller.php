<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class MY_Controller extends CI_Controller {

    public $data = array();
	public function __construct() {
		parent::__construct();
		$this->load->helper('obisys');
        $this->load->model('M_myweb');
        $this->load->model('default/m_seo');
        $this->data['project'] = $this->M_myweb->set_table('partner')->set('deleted',0)->gets();
        $this->data['seo']  = $this->m_seo->getSEO(2);
		$this->data['com_info'] = $this->M_myweb->set_table('company_info')->gets();
    }
    

}
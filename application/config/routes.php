<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'main';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

//default

$route['tim-kiem'] = 'main/search';

$route['danh-muc-san-pham'] = 'default/category';
$route['danh-muc-ajax/ajax/getpage'] = 'default/category/getpageajax';
$route['danh-muc-san-pham/(:any)'] = 'default/category/category/$1';
$route['location/(:any)'] = 'default/location/index/$1';
$route['filter/(:any)'] = 'default/location/filter/$1';

$route['san-pham'] = 'default/product';
$route['san-pham/(:any)'] = 'default/product/index/$1';
$route['ve-chung-toi'] = 'default/about';
$route['doi-tac'] = 'default/partner';
$route['thu-vien'] = 'default/gallery';
$route['gallery/ajax/getpage'] = 'default/gallery/getpageajax';
$route['thu-vien/(:any)'] = 'default/gallery/album/$1';

$route['tin-tuc'] = 'default/news';
$route['tin-tuc/(:any)'] = 'default/news/details/$1';

$route['dang-san-pham'] = 'default/product/edit';



//english language
$route['en'] = 'main_en';
$route['buildings'] = 'default/category_en/index';
$route['blogs'] = 'default/news_en';
$route['blogs/danh-muc/(:any)'] = 'default/news_en/category/$1';
$route['blogs/(:any)'] = 'default/news_en/details/$1';
$route['about-us'] = 'default/about_en';
$route['product'] = 'default/product_en';
$route['product/(:any)'] = 'default/product_en/index/$1';
$route['category'] = 'default/category_en';
$route['post-product'] = 'default/product_en/edit';

//admin
$route['admin'] = 'cms/product';
$route['admin/login'] = 'auth';
$route['admin/logout'] = 'auth/logout';
$route['admin/(:any)'] = 'cms/$1';

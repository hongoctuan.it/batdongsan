<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Main_en extends MY_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->search = isset($_GET['search']) ? $_GET['search'] : FALSE;
		$this->load->model('default/m_seo');
		$this->load->model('default/m_category_en');
		$this->data['seo']  = $this->m_seo->getSEO(1);
		$this->data['com_info'] = $this->M_myweb->set_table('company_info')->gets();
	}
	public function index()
	{
		$this->data['categories'] = $this->M_myweb->set_table('category')->sets(array('deleted' => 0, 'active' => 1, 'level' => 0))->set_orderby('id')->gets();
		$this->data['products'] = $this->M_myweb->set_table('product')->sets(array('deleted' => 0, 'active' => 1, 'hot' => 1))->gets();
		$this->data['hotproducts_01'] = $this->m_category_en->loadHotProduct(1)[0];
		$this->data['hotproducts_02'] = $this->m_category_en->loadHotProduct(2)[0];
		$this->data['hotproducts_03'] = $this->m_category_en->loadHotProduct(3)[0];
		$this->data['hotproducts_04'] = $this->m_category_en->loadHotProduct(4)[0];
		$this->data['hotproducts_05'] = $this->m_category_en->loadHotProduct(5)[0];
		$this->data['locationsearch'] = $this->m_category_en->loadSearchLocation();
		$this->data['propertiessearch'] = $this->m_category_en->loadSearchLocation();
		$this->data['location'] = $this->m_category_en->loadLocation();
		$this->data['subview'] 	= 'default/index/V_index_en';
		$this->load->view('default/_main_page_en', $this->data);
	}
	public function partner_registry()
	{
		$data = $this->input->post();
		if ($data) {
			$this->M_myweb->set_table('partner_registry')->sets($data)->save();
			echo 1;
		} else {
			echo 0;
		}
	}
}

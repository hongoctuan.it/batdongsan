<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product extends CMS_Controller {

	public function __construct(){
		parent::__construct();
		$this->data['product'] = false;
		$this->data['cates'] = $this->M_myweb->set('deleted',0)->set_table('category')->gets();
		$this->Model = $this->M_myweb->set_table('product');
		$this->load->model('cms/m_product');
		$this->load->library('pagination');
		$this->load->helper('download');

	}
	
	public function index()
	{
		switch($this->act){
			case "upd":
				if($this->input->post())
					$this->save();
				$this->edit();
				break;
			case "hot":
				$this->hot();
				break;
			case "unhot":
				$this->unHot();
				break;
			case "lock":
				$this->lock();
				break;
			case "unlock":
				$this->unLock();
				break;
			case "del":
				$this->delete();
				break;
			case "exp":
				$this->export();
				break;
			default:
				$this->home();
				break;
		}
	}

	private function home(){
		$this->data["product"] = $this->m_product->get_current_page_records();
		$this->data['subview'] = 'cms/product/home';
		$this->load->view('cms/_main_page',$this->data);
	}

	private function createLinks($start) {
		
		$page_curent = ceil( $start / $this->_limit )+1;
		$page = ceil($this->_total/$this->_limit);
		$last = ceil( $this->_total / $this->_limit );	
		$html = '<ul>';
		if($page_curent < $page-1){
			$html .= '<li style="list-style:none; float:left"><a class="btn btn-info" style="text-decoration: none;" href="'.site_url('admin/product?paging='.($page_curent)*$this->_limit).'">Sau</a></li>'; 
		}
		for($i=1; $i<=$page; $i++){
			if($page_curent == $i){
				$html .= '<li class="btn" style="color:red;list-style:none; float:left; font-weight: bold;">'.$i.'</li>'; 
			}else{
				$html .= '<li style="list-style:none; float:left"><a class="btn" style="text-decoration: none; font-weight: bold;" href="'.site_url('admin/product?paging='.($i-1)*$this->_limit).'">'.$i.'</a></li>'; 
			}
		}
		if($page_curent != 1){
			$html .= '<li style="list-style:none; float:left"><a class="btn btn-info" style="text-decoration: none;" href="'.site_url('admin/product?paging='.($page_curent-2)*$this->_limit).'">Trước</a></li>'; 
		}
		$html .= '</ul>'; 
		return $html;
	}

	private function edit(){

		if(isset($_GET['id'])){
			$this->data['id'] = $_GET['id'];
			$this->data['obj'] = $this->Model->set('id',$this->data['id'])->get();
			$this->data['obj']->image_01 = "";
			$this->data['product_partner']=$this->M_myweb->set_table('product_partner')->set("product_id",$_GET['id'])->gets();
		}
		$this->data['locations'] = $this->M_myweb->set_table('location')->set('deleted',0)->set_orderby('province')->gets();
		$this->data['partner']=$this->M_myweb->set_table('partner')->sets(array('active'=>1,'deleted'=>0))->gets();
		$this->data['subview'] = 'cms/product/edit';
		$this->load->view('cms/_main_page',$this->data);
	}
	
	private function hot(){
		if(isset($_GET['id'])){
			$data['id'] = $_GET['id'];
			$data['hot'] = $_GET['val'];
			if($data['hot']!=0){
				$this->unhot($data['hot']);
			}
			$this->Model->sets($data)->primary_key('id')->setPrimary($this->id)->save();
		}
		
	}

	private function unHot($hot){
		$data['hot'] = 0;
		$this->Model->sets($data)->primary_key('hot')->setPrimary($hot)->save();
	}

	private function lock(){
		if(isset($_GET['id'])){
			$this->data['id'] = $_GET['id'];
			$data['active'] = 0;
			$this->Model->sets($data)->setPrimary($this->id)->save();
		}
		$this->data['subview'] = 'cms/product/home';
		return redirect(site_url('admin/product'));
	}

	private function unLock(){
		if(isset($_GET['id'])){
			$this->data['id'] = $_GET['id'];
			$data['active'] = 1;
			$this->Model->sets($data)->setPrimary($this->id)->save();
		}
		$this->data['subview'] = 'cms/product/home';
		return redirect(site_url('admin/product'));
	}

	private function save(){
		$data = $this->input->post();
		$partner=$data['partner_id'];
		$image_01 = "";
		$image_02 = "";
		$image_03 = "";
		$image_04 = "";
		$countfiles = count($_FILES['images']['name']);
	// Looping all files
		for($i=0;$i<$countfiles;$i++){
			if(!empty($_FILES['images']['name'][$i]) && $i <=8){
				var_dump($_FILES['images']['name'][$i]);
				// Define new $_FILES array - $_FILES['file']
				$_FILES['image']['name'] = $_FILES['images']['name'][$i];
				$_FILES['image']['type'] = $_FILES['images']['type'][$i];
				$_FILES['image']['tmp_name'] = $_FILES['images']['tmp_name'][$i];
				$_FILES['image']['error'] = $_FILES['images']['error'][$i];
				$_FILES['image']['size'] = $_FILES['images']['size'][$i];
				$image = do_upload('avatar','image');	
				if($i==0)
					$data['img1'] = $image;
				else if($i==1)
					$data['img2'] = $image;
				else if($i==2)
					$data['img3'] = $image;
				else if($i==3)
					$data['img4'] = $image;
				else if($i==4)
					$data['img5'] = $image;
				else if($i==5)
					$data['img6'] = $image;
				else if($i==6)
					$data['img7'] = $image;
				else if($i==7)
					$data['img8'] = $image;
			}else{
				break;
			}
		}
		if(!isset($data['slug'])||trim($data['slug'])==""){
			$data['slug'] = str_replace(" ","-",stripUnicode($data['name']));
		}else{
			$data['slug'] = str_replace(" ","-",stripUnicode($data['slug']));
		}
		if($this->id){
			$this->Model->sets($data)->setPrimary($this->id)->save();
			$_SESSION['system_msg'] = messageDialog("div","success","Cập nhật sản phẩm thành công");
		}else{
			$id=$this->Model->sets($data)->save();
			$_SESSION['system_msg'] = messageDialog("div","success","Thêm sản phẩm thành công");
		}
		return redirect(site_url('admin/product'));
	}

	private function delete(){
		if($this->id){
			$getPro = $this->Model->set('id',$this->id)->get();
			if($getPro){
				$this->Model->sets(array('deleted'=>1))->setPrimary($this->id)->save();
				$_SESSION['system_msg'] = messageDialog("div","success","Xoá sản phẩm thành công");
			}else{
				$_SESSION['system_msg'] = messageDialog("div","error","Không thể xoá sản phẩm");
			}
		}
		redirect(site_url('admin/product'));
	}

	private function export(){
		$check = $this->input->post('checkall');
		if(!isset($check)){
			$check = $this->input->post('check');
		}
		$writer = new XMLWriter();
		$writer->openURI('oliviaproperty.xml');
		$writer->setIndent(true);
		$writer->startDocument('1.0', 'utf-8');
		$writer->startElement("data");
		if($check!=0){
			foreach($check as $item){
				$obj = $this->m_product->getProduct($item);
				$writer->startElement('post');
					//id
					$writer->startElement('ID');
					$writer->text($obj->id);
					$writer->endElement();
					//title
					$writer->startElement('Title');
					$writer->text($obj->name);
					$writer->endElement();
					//content
					$writer->startElement('Content');
					$writer->text($obj->detail_des);
					$writer->endElement();
					//Permalink
					$writer->startElement('Permalink');
					$writer->text(site_url('san-pham/'.$obj->slug));
					$writer->endElement();
					//URL
					$image="";
					if(!empty($obj->img1)){
						$image=$image.site_url('assets/public/avatar/'.$obj->img1);
					}
					if(!empty($obj->img2)){
						$image=$image."|".site_url('assets/public/avatar/'.$obj->img2);
					}
					if(!empty($obj->img3)){
						$image=$image."|".site_url('assets/public/avatar/'.$obj->img3);
					}
					if(!empty($obj->img4)){
						$image=$image."|".site_url('assets/public/avatar/'.$obj->img4);
					}
					if(!empty($obj->img5)){
						$image=$image."|".site_url('assets/public/avatar/'.$obj->img5);
					}
					if(!empty($obj->img6)){
						$image=$image."|".site_url('assets/public/avatar/'.$obj->img6);
					}
					if(!empty($obj->img7)){
						$image=$image."|".site_url('assets/public/avatar/'.$obj->img7);
					}
					if(!empty($obj->img8)){
						$image=$image."|".site_url('assets/public/avatar/'.$obj->img8);
					}
					$writer->startElement('URL');
					$writer->text($image);
					$writer->endElement();
					//Featured
					$writer->startElement('Featured');
					$writer->text(site_url('assets/public/avatar/'.$obj->img1));
					$writer->endElement();
					//PropertyType
					$writer->startElement('PropertyType');
					$writer->text($obj->category);
					$writer->endElement();
					//project
					$writer->startElement('project');
					$writer->text($obj->partner);
					$writer->endElement();
					//PropertyStatus
					$status_product="";
					if($obj->status_product=='ban.png'){
						$status_product='Bán';
					}else if($obj->status_product=='chothue.png'){
						$status_product='Cho Thuê';
					}else if($obj->status_product=='daban.png'){
						$status_product='Đã Bán';
					}else if($obj->status_product=='dathue.png'){
						$status_product='Đã Thuê';
					}else if($obj->status_product=='moidang.png'){
						$status_product='Mới Đăng';
					}else{
						$status_product='Sắp Ra';
					}
					$writer->startElement('PropertyStatus');
					$writer->text($status_product);//
					$writer->endElement();
					//REAL_HOMES_property_address
					$writer->startElement('REAL_HOMES_property_address');
					$writer->text($obj->address);
					$writer->endElement();
					//REAL_HOMES_property_location
					$writer->startElement('REAL_HOMES_property_location');
					$writer->text($obj->lating.','.$obj->longing);
					$writer->endElement();
					//REAL_HOMES_property_location
					$writer->startElement('REAL_HOMES_property_price');
					$writer->text($obj->price);
					$writer->endElement();
					//REAL_HOMES_property_price_postfix
					$writer->startElement('REAL_HOMES_property_price_postfix');
					$writer->text($obj->payment_period);
					$writer->endElement();
					//REAL_HOMES_property_size
					$writer->startElement('REAL_HOMES_property_size');
					$writer->text($obj->property_size);
					$writer->endElement();
					//REAL_HOMES_property_size_postfix
					$writer->startElement('REAL_HOMES_property_size_postfix');
					$writer->text('m2');
					$writer->endElement();
					//REAL_HOMES_property_bedrooms
					$writer->startElement('REAL_HOMES_property_bedrooms');
					$writer->text($obj->room);
					$writer->endElement();
					//REAL_HOMES_property_bathrooms
					$writer->startElement('REAL_HOMES_property_bathrooms');
					$writer->text($obj->tolet);
					$writer->endElement();
					//REAL_HOMES_property_garage
					$writer->startElement('REAL_HOMES_property_garage');
					$writer->text($obj->garages);
					$writer->endElement();
					//Language
					$writer->startElement('Language');
					$writer->text('VI');
					$writer->endElement();
					//Sub-type
					$writer->startElement('Sub-type');
					$writer->text($obj->category);
					$writer->endElement();
				$writer->endElement();
			}
		}else{
			$data = $this->m_product->getAllProduct();
			foreach($data as $obj){
				$writer->startElement('post');
					//id
					$writer->startElement('ID');
					$writer->text($obj->id);
					$writer->endElement();
					//title
					$writer->startElement('Title');
					$writer->text($obj->name);
					$writer->endElement();
					//content
					$writer->startElement('Content');
					$writer->text($obj->detail_des);
					$writer->endElement();
					//Permalink
					$writer->startElement('Permalink');
					$writer->text(site_url('san-pham/'.$obj->slug));
					$writer->endElement();
					//URL
					$image="";
					if(!empty($obj->img1)){
						$image=$image.site_url('assets/public/avatar/'.$obj->img1);
					}
					if(!empty($obj->img2)){
						$image=$image."|".site_url('assets/public/avatar/'.$obj->img2);
					}
					if(!empty($obj->img3)){
						$image=$image."|".site_url('assets/public/avatar/'.$obj->img3);
					}
					if(!empty($obj->img4)){
						$image=$image."|".site_url('assets/public/avatar/'.$obj->img4);
					}
					if(!empty($obj->img5)){
						$image=$image."|".site_url('assets/public/avatar/'.$obj->img5);
					}
					if(!empty($obj->img6)){
						$image=$image."|".site_url('assets/public/avatar/'.$obj->img6);
					}
					if(!empty($obj->img7)){
						$image=$image."|".site_url('assets/public/avatar/'.$obj->img7);
					}
					if(!empty($obj->img8)){
						$image=$image."|".site_url('assets/public/avatar/'.$obj->img8);
					}
					$writer->startElement('URL');
					$writer->text($image);
					$writer->endElement();
					//Featured
					$writer->startElement('Featured');
					$writer->text(site_url('assets/public/avatar/'.$obj->img1));
					$writer->endElement();
					//PropertyType
					$writer->startElement('PropertyType');
					$writer->text($obj->category);
					$writer->endElement();
					//project
					$writer->startElement('project');
					$writer->text($obj->partner);
					$writer->endElement();
					//PropertyStatus
					$status_product="";
					if($obj->status_product=='ban.png'){
						$status_product='Bán';
					}else if($obj->status_product=='chothue.png'){
						$status_product='Cho Thuê';
					}else if($obj->status_product=='daban.png'){
						$status_product='Đã Bán';
					}else if($obj->status_product=='dathue.png'){
						$status_product='Đã Thuê';
					}else if($obj->status_product=='moidang.png'){
						$status_product='Mới Đăng';
					}else{
						$status_product='Sắp Ra';
					}
					$writer->startElement('PropertyStatus');
					$writer->text($status_product);//
					$writer->endElement();
					//REAL_HOMES_property_address
					$writer->startElement('REAL_HOMES_property_address');
					$writer->text($obj->address);
					$writer->endElement();
					//REAL_HOMES_property_location
					$writer->startElement('REAL_HOMES_property_location');
					$writer->text($obj->lating.','.$obj->longing);
					$writer->endElement();
					//REAL_HOMES_property_location
					$writer->startElement('REAL_HOMES_property_price');
					$writer->text($obj->price);
					$writer->endElement();
					//REAL_HOMES_property_price_postfix
					$writer->startElement('REAL_HOMES_property_price_postfix');
					$writer->text($obj->payment_period);
					$writer->endElement();
					//REAL_HOMES_property_size
					$writer->startElement('REAL_HOMES_property_size');
					$writer->text($obj->property_size);
					$writer->endElement();
					//REAL_HOMES_property_size_postfix
					$writer->startElement('REAL_HOMES_property_size_postfix');
					$writer->text('m2');
					$writer->endElement();
					//REAL_HOMES_property_bedrooms
					$writer->startElement('REAL_HOMES_property_bedrooms');
					$writer->text($obj->room);
					$writer->endElement();
					//REAL_HOMES_property_bathrooms
					$writer->startElement('REAL_HOMES_property_bathrooms');
					$writer->text($obj->tolet);
					$writer->endElement();
					//REAL_HOMES_property_garage
					$writer->startElement('REAL_HOMES_property_garage');
					$writer->text($obj->garages);
					$writer->endElement();
					//Language
					$writer->startElement('Language');
					$writer->text('VI');
					$writer->endElement();
					//Sub-type
					$writer->startElement('Sub-type');
					$writer->text($obj->category);
					$writer->endElement();
				$writer->endElement();
			}
		}
		$writer->endDocument();
		$writer->flush();
		force_download('oliviaproperty.xml', NULL);
	}
	
}
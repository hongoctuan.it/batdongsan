<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Partner extends CMS_Controller {

	public function __construct(){
		parent::__construct();
		$this->Model = $this->M_myweb->set_table('partner');
	}
	
	public function index()
	{	
		switch($this->act){
			case "new":
				$this->save();
				break;
			case "upd":
				$this->updatePartner();
				break;
			case "del":
				$this->delPartner();
				break;
			case "lock":
				$this->lock();
				break;
			case "unlock":
				$this->unLock();
				break;
			case "registry-list":
				$this->partner_registry();
				break;
			case "registry-news-list":
					$this->partner_registry_news();
					break;
			default:
				$this->home();
				break;
		}
	}

	private function home(){
		$this->data['partner'] = $this->Model->set('deleted',0)->set_orderby('name')->gets();
		$this->data['subview'] = 'cms/partner/home';
		$this->load->view('cms/_main_page',$this->data);
  }
    
  private function save(){
		$data = $this->input->post('Partner');
		if(!empty($data)){
			if(!isset($data['slug'])||trim($data['slug'])==""){
				$data['slug'] = str_replace(" ","-",stripUnicode($data['name']));
			}else{
				$data['slug'] = str_replace(" ","-",stripUnicode($data['slug']));
			}
			if($_FILES['logo']['name']!=""){
				$logo = do_upload('avatar','logo');	
				$data['logo'] = $logo;			
			}
			if($_FILES['image01']['name']!=""){
				$image01 = do_upload('avatar','image01');	
				$data['img1'] = $image01;			
			}
			if($_FILES['image02']['name']!=""){
				$image02 = do_upload('avatar','image02');	
				$data['img2'] = $image02;			
			}
			if($_FILES['image03']['name']!=""){
				$image03 = do_upload('avatar','image03');	
				$data['img3'] = $image03;			
			}
			if($_FILES['image04']['name']!=""){
				$image04 = do_upload('avatar','image04');	
				$data['img4'] = $image04;			
			}
			if($_FILES['image05']['name']!=""){
				$image05 = do_upload('avatar','image05');	
				$data['img5'] = $image05;			
			}
			$data['slug'] = str_replace(" ","_",$data['slug']);
			$this->Model->sets($data)->save();
			$_SESSION['system_msg'] = messageDialog("div","success","Thêm thư viện thành công");
			return redirect(site_url('admin/partner'));
		}else{
			$this->data['locations'] = $this->M_myweb->set_table('location')->set('deleted',0)->set_orderby('province')->gets();
			$this->data['subview'] = 'cms/partner/edit';
			$this->load->view('cms/_main_page',$this->data);
		}
	}

	private function updatePartner(){
		$data = $this->input->post('Partner');
		$id = $_GET['id'];
		if(!empty($data)){
			if(!isset($data['slug'])||trim($data['slug'])==""){
				$data['slug'] = str_replace(" ","-",stripUnicode($data['name']));
			}else{
				$data['slug'] = str_replace(" ","-",stripUnicode($data['slug']));
			}
			if($_FILES['logo']['name']!=""){
				$logo = do_upload('avatar','logo');	
				$data['logo'] = $logo;			
			}
			if($_FILES['image01']['name']!=""){
				$image01 = do_upload('avatar','image01');	
				$data['img1'] = $image01;			
			}
			if($_FILES['image02']['name']!=""){
				$image02 = do_upload('avatar','image02');	
				$data['img2'] = $image02;			
			}
			if($_FILES['image03']['name']!=""){
				$image03 = do_upload('avatar','image03');	
				$data['img3'] = $image03;			
			}
			if($_FILES['image04']['name']!=""){
				$image04 = do_upload('avatar','image04');	
				$data['img4'] = $image04;			
			}
			if($_FILES['image05']['name']!=""){
				$image05 = do_upload('avatar','image05');	
				$data['img5'] = $image05;			
			}
			$data['slug'] = str_replace(" ","_",$data['slug']);
			$this->Model->sets($data)->setPrimary($this->id)->save();
			$_SESSION['system_msg'] = messageDialog("div","success","Cập nhật thư viện thành công");
			return redirect(site_url('admin/partner'));
		}else{
			if(isset($_GET['id'])){
				$this->data['id'] = $_GET['id'];
				$this->data['obj'] = $this->Model->set('id',$this->data['id'])->get();
			}
			$this->data['locations'] = $this->M_myweb->set_table('location')->set('deleted',0)->set_orderby('province')->gets();
			$this->data['subview'] = 'cms/partner/edit';
			$this->load->view('cms/_main_page',$this->data);
		}
	}
	private function delPartner(){
		$id = $_GET['id'];
		if($this->id){
			$getPro = $this->Model->set('id',$this->id)->get();
			if($getPro){
				$this->Model->sets(array('deleted'=>1))->setPrimary($this->id)->save();
				$_SESSION['system_msg'] = messageDialog("div","success","Xoá hình đối tác công");
			}else{
				$_SESSION['system_msg'] = messageDialog("div","error","Không thể xoá đối tác");
			}
		}
		return redirect(site_url('admin/partner'));
	}

	private function lock(){
		if(isset($_GET['id'])){
			$this->data['id'] = $_GET['id'];
			$data['active'] = 0;
			$this->Model->sets($data)->setPrimary($this->id)->save();
		}
		$this->data['subview'] = 'cms/partner/home';
		return redirect(site_url('admin/partner?id='.$_GET['id'].'&token='.$this->data['infoLog']->token));
	}

	private function unLock(){
		if(isset($_GET['id'])){
			$this->data['id'] = $_GET['id'];
			$data['active'] = 1;
			$this->Model->sets($data)->setPrimary($this->id)->save();
		}
		$this->data['subview'] = 'cms/gallery/home';
		return redirect(site_url('admin/partner?id='.$_GET['id'].'&token='.$this->data['infoLog']->token));
			
	}
	private function partner_registry(){
		
		$this->data['partners'] = $this->M_myweb->set_table('partner_registry')->sets(array('representator <>'=>'registernew','phone <>'=>'0'))->gets();
		$this->data['subview'] = 'cms/partner/registry';
		$this->load->view('cms/_main_page',$this->data);
	}
	private function partner_registry_news(){
		$this->data['partners'] = $this->M_myweb->set_table('partner_registry')->sets(array('representator'=>'registernew','phone'=>'0'))->gets();
		$this->data['subview'] = 'cms/partner/registry_news';
		$this->load->view('cms/_main_page',$this->data);
	}
}
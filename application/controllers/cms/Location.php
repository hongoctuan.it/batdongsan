<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class location extends CMS_Controller {

	public function __construct(){
		parent::__construct();
		$this->Model = $this->M_myweb->set_table('location');
	}
	
	public function index()
	{
		switch($this->act){
			case "upd":
				if($this->input->post())
					$this->save();
				$this->edit();
				break;
			case "del":
				$this->delete();
				break;
			case "lock":
				$this->lock();
				break;
			case "unlock":
				$this->unLock();
				break;
			default:
				$this->home();
				break;
		}
	}

	private function home(){
		$this->data['location'] = $this->Model->set('deleted',0)->set_orderby('province')->gets();
		$this->data['subview'] = 'cms/location/home';
		$this->load->view('cms/_main_page',$this->data);
	}

	private function edit(){
		if(isset($_GET['id'])){
			$this->data['id'] = $_GET['id'];
			$this->data['obj'] = $this->Model->set('id',$this->data['id'])->get();
		}
		$this->data['subview'] = 'cms/location/edit';
		$this->load->view('cms/_main_page',$this->data);
	}

	private function save(){
		$data = $this->input->post();
		if(!isset($data['slug'])||trim($data['slug'])==""){
			$data['slug'] = str_replace(" ","-",stripUnicode($data['province']."-".$data['district']));
		}else{
			$data['slug'] = str_replace(" ","-",stripUnicode($data['slug']));
		}
		if($this->id){
			$image_04 = "";
			if($_FILES['image_01']['name']!=""){
				$image_01 = do_upload('avatar','image_01');	
				$data['img'] = $image_01;				
			}
			$this->Model->sets($data)->setPrimary($this->id)->save();
			$_SESSION['system_msg'] = messageDialog("div","success","Cập nhật thư viện thành công");
		}else{
			$image_04 = "";
			if($_FILES['image_01']['name']!=""){
				$image_01 = do_upload('avatar','image_01');	
				$data['img'] = $image_01;				
			}
			if(isset($_GET['parent'])){
				$data['parent'] = $_GET['parent'];
			}
			$data['id'] = $_GET['location'];
			$this->Model->sets($data)->save();
			$_SESSION['system_msg'] = messageDialog("div","success","Thêm thư viện thành công");
		}
		return redirect(site_url('admin/location'));
	}

	private function delete(){
		if($this->id){
			$getPro = $this->Model->set('id',$this->id)->get();
			if($getPro){
				$this->Model->sets(array('deleted'=>1))->setPrimary($this->id)->save();
				$_SESSION['system_msg'] = messageDialog("div","success","Xoá category thành công");
			}else{
				$_SESSION['system_msg'] = messageDialog("div","error","Không thể xoá category");
			}
		}
		return redirect(site_url('admin/location'));
	}

	private function lock(){
		if(isset($_GET['id'])){
			$this->data['id'] = $_GET['id'];
			$data['active'] = 0;
			$this->Model->sets($data)->setPrimary($this->id)->save();
		}
		$this->data['subview'] = 'cms/location/home';
		return redirect(site_url('admin/location'));
	}

	private function unLock(){
		if(isset($_GET['id'])){
			$this->data['id'] = $_GET['id'];
			$data['active'] = 1;
			$this->Model->sets($data)->setPrimary($this->id)->save();
		}
		$this->data['subview'] = 'cms/location/home';
		return redirect(site_url('admin/location'));
			
	}

}
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Hotproductinfo extends CMS_Controller {

	public function __construct(){
		parent::__construct();
		$this->Model = $this->M_myweb->set_table('news');
	}
	
	public function index()
	{
		switch($this->act){
			case "upd":

				$this->save();
				break;
			default:
				$this->home();
				break;
		}
	}

	private function home(){
		$this->Model = $this->M_myweb->set_table('home_hot_product');
		$this->data['obj_01'] = $this->Model->set('id',1)->get();
		$this->data['obj_02'] = $this->Model->set('id',2)->get();
		$this->data['obj_03'] = $this->Model->set('id',3)->get();
		$this->data['obj_04'] = $this->Model->set('id',4)->get();
		$this->data['obj_05'] = $this->Model->set('id',5)->get();

		$this->data['subview'] = 'cms/hotproductinfo/desHotProduct';
		$this->load->view('cms/_main_page',$this->data);
	}

	

	private function save(){
		$this->Model = $this->M_myweb->set_table('home_hot_product');
		$temp = $this->input->post();
		$image_01 = "";
		$image_02 = "";
		$image_03 = "";
		$image_04 = "";
		$image_05 = "";
		$arr= array();

		$data['product_id'] = $temp['product_id_01'];
		$this->Model->sets($data)->setPrimary(1)->save();
		$data = null;
		$data['product_id'] = $temp['product_id_02'];
		$this->Model->sets($data)->setPrimary(2)->save();
		$data = null;
		$data['product_id'] = $temp['product_id_03'];
		$this->Model->sets($data)->setPrimary(3)->save();
		$data = null;
		$data['product_id'] = $temp['product_id_04'];
		$this->Model->sets($data)->setPrimary(4)->save();
		$data = null;
		$data['product_id'] = $temp['product_id_05'];
		$this->Model->sets($data)->setPrimary(5)->save();
		$_SESSION['system_msg'] = messageDialog("div","success","Cập nhật thành công");
		return redirect(site_url('admin/hotproductinfo'));
	}

	

	
}
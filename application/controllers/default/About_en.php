<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class About_en extends MY_Controller {
	public function __construct(){
		parent::__construct();
		$this->Model = $this->M_myweb->set_table('about_en');
		$this->load->model('default/m_seo');
		$this->data['seo']  = $this->m_seo->getSEO(4);

	}
	
	public function index()
	{
		//general
		$introduce_info = $this->Model->get();
		$this->data['introduce_info'] = $introduce_info;
		//history
		$this->Model = $this->M_myweb->set('deleted',0)->set_table('history');
		$this->data['history'] 	= $this->Model->set_orderby('title')->gets();
		$this->data['subview'] 	= 'default/about/V_about_en';
		$this->load->view('default/_main_page_en',$this->data);
	}
}
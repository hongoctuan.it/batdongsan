<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Category extends MY_Controller {

	public function __construct(){
		parent::__construct();
		$this->data['style']=isset($_GET['style'])?$_GET['style']:false;
		$this->data['by']=isset($_GET['by'])?$_GET['by']:false;
		$this->data['order']=isset($_GET['order'])?$_GET['order']:false;
		$this->cate=isset($_GET['cate'])?$this->input->get('cate'):false;
		$this->load->model('default/m_category');
		$this->load->model('default/m_product');
		$this->data['seo']  = $this->m_seo->getSEO(2);
	}
	
	public function index()
	{
		$this->data['facebook'] = $this->M_myweb->set_table('company_info')->set('info','facebook')->get()->value;
		$this->data['viber'] = $this->M_myweb->set_table('company_info')->set('info','viber')->get()->value;
		$this->data['whatsapp'] = $this->M_myweb->set_table('company_info')->set('info','whatsapp')->get()->value;
		$this->data['zalo'] = $this->M_myweb->set_table('company_info')->set('info','zalo')->get()->value;
		$this->data['phone'] = $this->M_myweb->set_table('company_info')->set('info','phone')->get()->value;
		$this->data['email'] = $this->M_myweb->set_table('company_info')->set('info','email')->get()->value;

		$properties=isset($_GET['properties'])?$_GET['properties']:false;
		$location=isset($_GET['location'])?$_GET['location']:false;
		$keyword=isset($_GET['keyword'])?$_GET['keyword']:false;
		$minprice=isset($_GET['minprice'])?$_GET['minprice']:false;
		$maxprice=isset($_GET['maxprice'])?$_GET['maxprice']:false;
		$this->data['locationsearch'] = $this->m_category->loadSearchLocation();
		$this->catslug = $this->m_category->getcatslug(isset($_GET['cate'])?$_GET['cate']:false);
		$beds=isset($_GET['beds'])?$_GET['beds']:false;
		$search=isset($_GET['search'])?$_GET['search']:false;
		$sort=isset($_GET['sort'])?$_GET['sort']:false;
		$project=isset($_GET['project'])?$_GET['project']:false;
		if($search){
			if($properties=='ban.png'){
				$arr_ban=$this->m_category->loadCatSearchProduct($keyword,$location,$properties,$minprice,$maxprice,$beds,$sort);
				$properties = 'sapra.png';
				$arr_sapra=$this->m_category->loadCatSearchProduct($keyword,$location,$properties,$minprice,$maxprice,$beds,$sort);
				$properties = 'daban.png';
				$arr_daban=$this->m_category->loadCatSearchProduct($keyword,$location,$properties,$minprice,$maxprice,$beds,$sort);
				$this->data['products'] = array_merge($arr_ban,$arr_sapra,$arr_daban);
			}else if($properties=='chothue.png'){
				$arr_chothue=$this->m_category->loadCatSearchProduct($keyword,$location,$properties,$minprice,$maxprice,$beds,$sort);
				$properties = 'moidang.png';
				$arr_moidang=$this->m_category->loadCatSearchProduct($keyword,$location,$properties,$minprice,$maxprice,$beds,$sort);
				$properties = 'dathue.png';
				$arr_dathue=$this->m_category->loadCatSearchProduct($keyword,$location,$properties,$minprice,$maxprice,$beds,$sort);
				$this->data['products'] = array_merge($arr_chothue,$arr_moidang,$arr_dathue);
			}else{
				$this->data['products']=$this->m_category->loadCatSearchProduct($keyword,$location,$properties,$minprice,$maxprice,$beds,$sort);
			}
			$this->data['subview'] 	= 'default/category/V_category';
			$this->load->view('default/_main_page_vi',$this->data);
		}else if($location){
			if($properties=='ban.png'){
				$arr_ban=$this->m_category->loadSearchProduct($properties,$location,$sort);
				$properties = 'sapra.png';
				$arr_sapra=$this->m_category->loadSearchProduct($properties,$location,$sort);
				$properties = 'daban.png';
				$arr_daban=$this->m_category->loadSearchProduct($properties,$location,$sort);
				$this->data['products'] = array_merge($arr_ban,$arr_sapra,$arr_daban);
			}else if($properties=='chothue.png'){
				$arr_chothue=$this->m_category->loadSearchProduct($properties,$location,$sort);
				$properties = 'moidang.png';
				$arr_moidang=$this->m_category->loadSearchProduct($properties,$location,$sort);
				$properties = 'dathue.png';
				$arr_dathue=$this->m_category->loadSearchProduct($properties,$location,$sort);
				$this->data['products'] = array_merge($arr_chothue,$arr_moidang,$arr_dathue);
			}else{
				$this->data['products']=$this->m_category->loadSearchProduct($properties,$location,$sort);
			}
			$this->data['subview'] 	= 'default/category/V_category';
			$this->load->view('default/_main_page_vi',$this->data);
		}else if($project){
			$this->data['products']=$this->m_category->loadSearchProject($project);
			$this->data['partner']=$this->m_category->getPartner($project);
			$this->data['subview'] 	= 'default/category/V_category';
			$this->load->view('default/_main_page_vi',$this->data);
		}
		else{
			$this->data['products'] = $this->m_category->loadProductPage($this->catslug,false,$sort,$properties);
			$this->data['propertiessearch'] = $this->m_category->loadSearchLocation();
			$this->data['subview'] 	= 'default/category/V_category';
			$this->load->view('default/_main_page_vi',$this->data);
		}
	}
	public function category($slug)
	{
		$cates=$this->m_category->getCategoryWhere(array('slug'=>$slug));
		if(!empty($cates))
		{
			$this->data['products']=$this->m_category->loadProductPage($slug,false);
			$this->data['subview'] 	= 'default/category/V_category';
			$this->load->view('default/_main_page_vi',$this->data);
		}else{
			$arr = explode('-',$slug);
			if($arr[2]!=''){
				$this->data['products']=$this->m_category->loadProductPage(false,$arr[2]);
				$this->data['subview'] 	= 'default/category/V_category';
				$this->load->view('default/_main_page_vi',$this->data);
			}else{
				$arrslug =  (explode("-",$slug));
				$this->data['products']=$this->m_category->loadLocationProduct($arrslug[0],$arrslug[1]);
				$this->data['subview'] 	= 'default/category/V_category';
				$this->load->view('default/_main_page_vi',$this->data);
			}
		}
	}

	public function index_en()
	{
		$properties=isset($_GET['properties'])?$_GET['properties']:false;
		$location=isset($_GET['location'])?$_GET['location']:false;
		$keyword=isset($_GET['keyword'])?$_GET['keyword']:false;
		$minprice=isset($_GET['minprice'])?$_GET['minprice']:false;
		$maxprice=isset($_GET['maxprice'])?$_GET['maxprice']:false;
		$this->data['locationsearch'] = $this->m_category->loadSearchLocation();
		$this->catslug = $this->m_category->getcatslug(isset($_GET['cate'])?$_GET['cate']:false);
		$beds=isset($_GET['beds'])?$_GET['beds']:false;
		$search=isset($_GET['search'])?$_GET['search']:false;
		$sort=isset($_GET['sort'])?$_GET['sort']:false;
		$project=isset($_GET['project'])?$_GET['project']:false;
		if($search){
			$this->data['products']=$this->m_category->loadCatSearchProduct($keyword,$location,$properties,$minprice,$maxprice,$beds,$sort);
			$this->data['subview'] 	= 'default/category/V_category_en';
			$this->load->view('default/_main_page_en',$this->data);
		}else if($location){
			$this->data['products']=$this->m_category->loadSearchProduct($properties,$location,$sort);
			$this->data['subview'] 	= 'default/category/V_category_en';
			$this->load->view('default/_main_page_en',$this->data);
		}else if($project){
			$this->data['products']=$this->m_category->loadSearchProject($project);
			$this->data['subview'] 	= 'default/category/V_category_en';
			$this->load->view('default/_main_page_en',$this->data);
		}
		else{
			$this->data['products'] = $this->m_category->loadProductPage($this->catslug,false,$sort,$properties);
			$this->data['propertiessearch'] = $this->m_category->loadSearchLocation();
			$this->data['subview'] 	= 'default/category/V_category_en';
			$this->load->view('default/_main_page_en',$this->data);
		}
	}

	public function category_en($slug)
	{
		$cates=$this->m_category->getCategoryWhere(array('slug'=>$slug));
		if(!empty($cates))
		{
			$this->data['products']=$this->m_category->loadProductPage($slug,false);
			$this->data['subview'] 	= 'default/category/V_category';
			$this->load->view('default/_main_page_vi',$this->data);
		}else{
			$arr = explode('-',$slug);
			if($arr[2]!=''){
				$this->data['products']=$this->m_category->loadProductPage(false,$arr[2]);
				$this->data['subview'] 	= 'default/category/V_category';
				$this->load->view('default/_main_page_vi',$this->data);
			}else{
				$arrslug =  (explode("-",$slug));
				$this->data['products']=$this->m_category->loadLocationProduct($arrslug[0],$arrslug[1]);
				$this->data['subview'] 	= 'default/category/V_category';
				$this->load->view('default/_main_page_vi',$this->data);
			}
		}
	}
}
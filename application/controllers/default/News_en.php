<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class News_en extends MY_Controller {

	public function __construct(){
        parent::__construct();
        $this->data['news_categories']=$this->M_myweb->set_table('news')->sets(array('deleted'=>0,'active'=>1,'parent'=>0))->gets();
        $this->load->model('default/m_news_en');
        $this->page=isset($_GET['page'])?$this->input->get('page'):false;
        $this->cate=isset($_GET['cate'])?$this->input->get('cate'):false;
        $this->load->model('default/m_seo');
        $this->data['seo']  = $this->m_seo->getSEO(3);
    }	
	public function index()
	{

        if($this->page){
            $this->m_news_en->setPage($this->page);
        }else{
            $this->page=1;
        }
        // $this->m_news->setWhere('parent >',0);
        $this->data['current_page']=$this->page;
        $this->data['total_pages']=$this->m_news_en->totalPages();
        $this->data['news_datas']=$this->m_news_en->getNews();
        $this->data['subview'] 	= 'default/news/V_news';
        $this->load->view('default/_main_page_en',$this->data);
    }

    public function category($category){
        $this->data['slug']=$category;
        if($this->page){
            $this->m_news_en->setPage($this->page);
        }else{
            $this->page=1;
        }
        $this->data['cate']=$this->M_myweb->set_table('news')->sets(array('deleted'=>0,'active'=>1,'slug'=>$category))->get();
        $this->m_news_en->setWhere('parent',$this->data['cate']->id);
        $this->m_news_en->setWhere('parent >',0);
        $this->data['current_page']=$this->page;
        $this->data['total_pages']=$this->m_news_en->totalPages();
        $this->data['news_datas']=$this->m_news_en->getNews();
        if(isset($this->data['news_datas']))
        {
            $this->data['title']	= "Tin Tức";
            $this->data['subview'] 	= 'default/news/V_news';
            $this->load->view('default/_main_page_en',$this->data);
        }else{
            redirect(site_url("tin-tuc"));
        }

    }
    public function details($news_slug)
    {
        $this->m_news_en->setWhere("slug",$news_slug);
        $this->data['news_data']=$this->m_news_en->getNews();
        if(isset($this->data['news_data']))
        {
            $this->data['cate']=$this->M_myweb->set_table('news')->sets(array('deleted'=>0,'active'=>1,'id'=>$this->data['news_data']->parent))->get();
            $this->data['title']	= "Tin Tức";
            $this->data['subview'] 	= 'default/news/V_news_detail';
            $this->load->view('default/_main_page_en',$this->data);
        }else{
            redirect(site_url("tin-tuc"));
        }

    }
}
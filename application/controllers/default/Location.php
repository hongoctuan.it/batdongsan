<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Location extends MY_Controller {

	public function __construct(){
		parent::__construct();
		$this->data['categories'] = $this->M_myweb->set_table('category')->sets(array('deleted' => 0, 'active' => 1, 'level' => 0))->gets();
		$this->load->model('default/m_location');
		$this->data['seo']  = $this->m_seo->getSEO(2);

	}
	
	public function index($slug)
	{

		$locationId = $this->m_location->getlocationId($slug)->id;
		$this->data['products'] = $this->m_location->loadProductPage($locationId);
		$this->data['location'] = $this->m_location->loadLocation($locationId);

		$this->data['subview'] 	= 'default/location/V_location';
		$this->load->view('default/_main_page_vi',$this->data);
	}

	public function filter($slug)
	{
		$arr = explode('_',$slug);
		$locationId = $this->m_location->getlocationId($arr[6])->id;
		$catId = $this->m_location->getcatid($arr[1]);
		$minPrice = str_replace('.', '', $arr[4]);
		$maxPrice = str_replace('.', '', $arr[5]);
		$this->data['products'] = $this->m_location->filter($arr[0],$catId->id,$arr[2],$arr[3],$minPrice,$maxPrice,$locationId);
		$result="";
		foreach($this->data['products'] as $item){
			$result = $result.'<a class="col-md-4" style="color:black" href="'.site_url("san-pham/".$item->slug).'">';
			$result = $result.'<div class="related-item" style="background-image:url('."'".site_url("assets/public/avatar/".$item->img1)."'".'); width:100%; height:240px; background-position:center; padding:0px">';
			$result = $result.'<div class="related-content">';
			$result = $result.'<p class="related-content-title">'.$item->productname.' '.$item->category_name. 'For Rent & Sale</p>';
			$result = $result.'<p class="related-content-address">'.$item->address.'</p>';
			$result = $result.'<p class="related-content-info"><i class="fa fa-bed"></i> '.$item->room.' <i class="fa fa-bath"></i> '.$item->tolet.' <i class="fa fa-map"> '.$item->area.'</i></p>';
			$result = $result.'</div>';
			$result = $result.'</div>';
			$result = $result.'</a>';
		}
		print_r($result);
		// echo $slug;
	}

}
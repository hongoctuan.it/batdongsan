<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product extends MY_Controller {

	public function __construct(){
		parent::__construct();
		$this->id = $this->input->get('id');
		$this->load->model('default/m_product');
		$this->load->model('default/m_seo');
		$this->data['com_info'] = $this->M_myweb->set_table('company_info')->gets();
		$this->Model = $this->M_myweb->set_table('product');

	}
	// function _remap($slug) {
    //     $this->index($slug);
    // }
	public function index($slug)
	{
		$this->data['facebook'] = $this->M_myweb->set_table('company_info')->set('info','facebook')->get()->value;
		$this->data['viber'] = $this->M_myweb->set_table('company_info')->set('info','viber')->get()->value;
		$this->data['whatsapp'] = $this->M_myweb->set_table('company_info')->set('info','whatsapp')->get()->value;
		$this->data['zalo'] = $this->M_myweb->set_table('company_info')->set('info','zalo')->get()->value;
		$this->data['phone'] = $this->M_myweb->set_table('company_info')->set('info','phone')->get()->value;
		$this->data['email'] = $this->M_myweb->set_table('company_info')->set('info','email')->get()->value;
		$this->m_product->setWhere("product.slug",$slug);
		$this->data['product'] = $this->m_product->getProductWhere();
		$this->data['seo']['title']  = $this->data['product']->name;
		$this->data['seo']['meta']  = $this->data['product']->short_des;
		$this->data['relatedproduct'] = $this->m_product->getRelatedProductWhere($this->data['product']->category_id,$this->data['product']->id);
		$this->data['partner'] = $this->M_myweb->set_table('partner')->set('id',$this->data['product']->partner_id)->get();
		if($this->data['product'])
		{
			$this->data['subview'] 	= 'default/product/V_productDetail';
			$this->load->view('default/_main_page_vi',$this->data);
		}else{
			$this->data['title']	= "Sản Phẩm";
			$this->data['subview'] 	= 'default/product/V_noProduct';
			$this->load->view('default/_main_page_vi',$this->data);
		}
	}
	public function edit(){
		$data =$this->input->post();
		if(isset($_GET['id'])){
			$this->data['id'] = $_GET['id'];
			$this->data['obj'] = $this->Model->set('id',$this->data['id'])->get();
			$this->data['obj']->image_01 = "";
			$this->data['product_partner']=$this->M_myweb->set_table('product_partner')->set("product_id",$_GET['id'])->gets();
		}else if(!empty($data)){
			$this->save();
		}
		$this->data['cates'] = $this->M_myweb->set('deleted',0)->set_table('category')->gets();
		$this->data['locations'] = $this->M_myweb->set_table('location')->set('deleted',0)->set_orderby('province')->gets();

		$this->data['subview'] = 'default/product/edit';
		$this->load->view('default/_main_page_vi',$this->data);

	}

	private function save(){
		$data = $this->input->post();
		$image_01 = "";
		$image_02 = "";
		$image_03 = "";
		$image_04 = "";
		if($_FILES['image_01']['name']!=""){
			$image_01 = do_upload('avatar','image_01');	
			$data['img1'] = $image_01;				
		}
		if($_FILES['image_02']['name']!=""){
			$image_02 = do_upload('avatar','image_02');		
			$data['img2'] = $image_02;		
		}
		if($_FILES['image_03']['name']!=""){
			$image_03 = do_upload('avatar','image_03');	
			$data['img3'] = $image_03;			
		}
		if($_FILES['image_04']['name']!=""){
			$image_04 = do_upload('avatar','image_04');	
			$data['img4'] = $image_04;			
		}
		if($_FILES['image_05']['name']!=""){
			$image_05 = do_upload('avatar','image_05');	
			$data['img5'] = $image_05;			
		}
		if($_FILES['image_06']['name']!=""){
			$image_06 = do_upload('avatar','image_06');	
			$data['img6'] = $image_06;			
		}
		if($_FILES['image_07']['name']!=""){
			$image_07 = do_upload('avatar','image_07');	
			$data['img7'] = $image_04;			
		}
		if($_FILES['image_08']['name']!=""){
			$image_08 = do_upload('avatar','image_08');	
			$data['img8'] = $image_08;			
		}
		if(!isset($data['slug'])||trim($data['slug'])==""){
			$data['slug'] = str_replace(" ","-",stripUnicode($data['name']));
		}else{
			$data['slug'] = str_replace(" ","-",stripUnicode($data['slug']));
		}
		$data['type']=1;
		$data['active']=0;
		if($this->id){
			$this->Model->sets($data)->setPrimary($this->id)->save();
			$_SESSION['system_msg'] = messageDialog("div","success","Cập nhật sản phẩm thành công");
		}else{
			$id=$this->Model->sets($data)->save();
			$_SESSION['system_msg'] = messageDialog("div","success","Thêm sản phẩm thành công");
		}
		return redirect(site_url('dang-san-pham'));
	}
	
}
$(document).ready(function(){
	
	$('#price-range-submit').hide();

	// $("#min_price,#max_price").on('change', function () {

	//   $('#price-range-submit').show();

	//   var min_price_range = parseInt($("#min_price").val());

	//   var max_price_range = parseInt($("#max_price").val());

	//   if (min_price_range > max_price_range) {
	// 	$('#max_price').val(min_price_range);
	//   }

	//   $("#slider-range").slider({
	// 	values: [min_price_range, max_price_range]
	//   });
	  
	// });


	// $("#min_price,#max_price").on("paste keyup", function () {                                        

	//   $('#price-range-submit').show();

	//   var min_price_range = parseInt($("#min_price").val());

	//   var max_price_range = parseInt($("#max_price").val());
	  
	//   if(min_price_range == max_price_range){

	// 		max_price_range = min_price_range + 100;
			
	// 		$("#min_price").val(min_price_range);		
	// 		$("#max_price").val(max_price_range);
	//   }

	//   $("#slider-range").slider({
	// 	values: [min_price_range, max_price_range]
	//   });

	// });


	$(function () {
	  $("#slider-range").slider({
		range: true,
		orientation: "horizontal",
		min: 250000000,
		max: 3000000000,
		values: [250000000, 3000000000],
		step: 100,

		slide: function (event, ui) {
			if (ui.values[0] == ui.values[1]) {
				return false;
			}
			var minPrice= (ui.values[0]+"");
			var temp ='';
			var count= minPrice.length-1;
			for(i = 0;i<minPrice.length;i++){
				if(i%3==2&&i!=0){
					temp=temp+minPrice[count]+".";
				}else{
					temp=temp+minPrice[count];
				}
				count--;
			}
		  	minPrice='';
		  	for(i = (temp.length-1);i>=0;i--){
			  	minPrice=minPrice+temp[i];
			}
			if(minPrice[0]=='.'){
				minPrice = minPrice.substr(1);
			}

			// format max price
			var maxPrice= (ui.values[1]+"");
			temp ='';
			count= maxPrice.length-1;
			for(i = 0;i<maxPrice.length;i++){
				if(i%3==2&&i!=0){
					temp=temp+maxPrice[count]+".";
				}else{
					temp=temp+maxPrice[count];
				}
				count--;
			}
			maxPrice='';
		  	for(i = (temp.length-1);i>=0;i--){
				maxPrice=maxPrice+temp[i];
			}
			if(maxPrice[0]=='.'){
				maxPrice = maxPrice.substr(1);
			}

		  $("#min_price").val(minPrice);
		  $("#max_price").val(maxPrice);
		}
	  });

	  $("#min_price").val($("#slider-range").slider("values", 0));
	  $("#max_price").val($("#slider-range").slider("values", 1));

	});

	// $("#slider-range,#price-range-submit").click(function () {

	//   var min_price = $('#min_price').val();
	//   var max_price = $('#max_price').val();

	//   $("#searchResults").text("Here List of products will be shown which are cost between " + min_price  +" "+ "and" + " "+ max_price + ".");
	// });

});